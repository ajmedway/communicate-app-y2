<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SignnowApiToken */

$this->title = 'Create Signnow Api Token';
$this->params['breadcrumbs'][] = ['label' => 'Signnow Api Tokens', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="signnow-api-token-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

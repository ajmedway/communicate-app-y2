<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
$userModule = Yii::$app->getModule('user');
$bsContainerClass = !empty($this->params['bsContainerClass']) ? $this->params['bsContainerClass'] : 'container';
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandImage' => '@web/img/logo.png',
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'innerContainerOptions' => [
            'class' => $bsContainerClass,
        ],
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            [
                'label' => '<i class="fa fa-home"></i> Home',
                'url' => ['/site/index'],
            ],
            [
                'label' => '<i class="fa fa-user"></i> About',
                'url' => ['/site/about'],
            ],
            [
                'label' => '<i class="fa fa-envelope"></i> Contact',
                'url' => ['/site/contact'],
                'visible' => !Yii::$app->user->can('admin'),
            ],
            [
                'label' => '<i class="fa fa-book"></i> My Profile',
                'items' => [
                    [
                        'label' => 'View Profile',
                        'url' => ['/user/profile/show'],
                    ],
                    '<li class="divider"></li>',
                    '<li class="dropdown-header">Manage My Profile</li>',
                    [
                        'label' => 'Edit Profile',
                        'url' => ['/user/settings/profile'],
                    ],
                    [
                        'label' => 'Edit Company',
                        'url' => ['/company/manage'],
                    ],
                    [
                        'label' => 'Edit Account',
                        'url' => ['/user/settings/account'],
                    ],
                ],
                'visible' => !Yii::$app->user->isGuest,
            ],
            [
                'label' => '<i class="fa fa-clipboard-list"></i> My Quote',
                'items' => [
                    [
                        'label' => 'Create & Manage Your Quote',
                        'url' => ['/quote/manage'],
                    ],
                ],
                'visible' => (Yii::$app->user->can('user') && !Yii::$app->user->can('admin')),
            ],
            [
                'label' => '<i class="fa fa-clipboard-list"></i> Quotes',
                'items' => [
                    [
                        'label' => 'View All Quotes',
                        'url' => ['/quote/index'],
                    ],
                    [
                        'label' => 'Manage Quotes',
                        'url' => ['/quote/manage-admin'],
                    ],
                ],
                'visible' => Yii::$app->user->can('admin'),
            ],
            [
                'label' => '<i class="fa fa-users"></i> Manage Users',
                'url' => ['/user/admin/index'],
                'active' => in_array(Yii::$app->controller->id, ['admin', 'role', 'permission', 'rule']),
                'visible' => Yii::$app->user->can('admin'),
            ],
            [
                'label' => '<i class="fa fa-boxes"></i> Manage Products',
                'items' => [
                    '<li class="dropdown-header">Products</li>',
                    [
                        'label' => 'View Products',
                        'url' => ['/product/product/index'],
                    ],
                    [
                        'label' => 'Create Product',
                        'url' => ['/product/product/create'],
                    ],
                    '<li class="divider"></li>',
                    '<li class="dropdown-header">Product Categories</li>',
                    [
                        'label' => 'View Categories',
                        'url' => ['/product/product-category/index'],
                    ],
                    '<li class="divider"></li>',
                    '<li class="dropdown-header">Suppliers</li>',
                    [
                        'label' => 'View Suppliers',
                        'url' => ['/product/supplier/index'],
                    ],
                ],
                'active' => in_array(Yii::$app->controller->module->id, ['product']),
                'visible' => Yii::$app->user->can('admin'),
            ],
            Yii::$app->user->isGuest ? (
                [
                    'label' => '<i class="fa fa-sign-in-alt"></i> Login',
                    'url' => ['/user/security/login'],
                ]
            ) : (
                '<li>'
                . Html::beginForm(
                    ['/user/security/logout'],
                    'post'
                )
                . Html::submitButton(
                    '<i class="fa fa-sign-out-alt"></i> Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            ),
            Yii::$app->session->has($userModule->switchIdentitySessionKey) ? (
                '<li>'
                . Html::a(
                    'Switch to Admin',
                    ['/user/admin/switch-identity'],
                    ['data-method' => 'post']
                )
                . '</li>'
            ) : (''),
        ],
        'encodeLabels' => false,
    ]);
    NavBar::end();
    ?>

    <div class="<?= $bsContainerClass ?>">
        <div id="breadcrumbs-wrapper">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
        </div>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="<?= $bsContainerClass ?>">
        <p class="pull-left">&copy; r//evolution Marketing <?= date('Y') ?></p>
        <?php /*<p class="pull-right"><?= Yii::powered() ?></p>*/ ?>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

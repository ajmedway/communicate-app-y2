<?php
/*
 * This file is part of the 2amigos/yii2-usuario project.
 *
 * (c) 2amigOS! <http://2amigos.us/>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use app\models\Profile;

/**
 * @var yii\web\View           $this
 * @var \Da\User\Model\User    $user
 * @var \Da\User\Model\Profile $profile
 */
?>

<?php $this->beginContent('@Da/User/resources/views/admin/update.php', ['user' => $user]) ?>

<?php
$form = ActiveForm::begin([
    'id' => $profile->formName(),
    'layout' => 'horizontal',
    'fieldConfig' => [
        'horizontalCssClasses' => [
            'offset' => 'col-sm-offset-3',
            'label' => 'col-sm-3',
            'wrapper' => 'col-sm-9',
            'hint' => '',
        ],
    ],
    'enableAjaxValidation' => true,
    'enableClientValidation' => true,
    'validateOnBlur' => false,
]);
?>

<?php // Abandoned 2amigos/yii2-usuario core module `profile` fields ?>
<?php //= $form->field($profile, 'name') ?>
<?php //= $form->field($profile, 'website') ?>
<?php //= $form->field($profile, 'location') ?>
<?php /* =
  $form
  ->field($profile, 'timezone')
  ->dropDownList(ArrayHelper::map($timezoneHelper->getAll(), 'timezone', 'name')); */
?>

<h4>Profile Details</h4>
<?= $form->field($profile, 'public_email')->textInput(['readOnly' => true, 'disabled' => true]) ?>
<?= $form->field($profile, 'gravatar_email') ?>
<?= $form->field($profile, 'hs_salutation') ?>
<?= $form->field($profile, 'hs_firstname') ?>
<?= $form->field($profile, 'hs_lastname') ?>
<?= $form->field($profile, 'hs_jobtitle') ?>
<?= $form->field($profile, 'hs_mobile_phone') ?>
<?= $form->field($profile, 'hs_phone') ?>
<?= $form->field($profile, 'hs_fax') ?>
<?= $form->field($profile, 'website') ?>
<?php //= $form->field($profile, 'bio')->textarea() ?>

<hr>

<h4>Company &amp; Address Details</h4>
<?= $form->field($profile, 'hs_company') ?>
<?= $form->field($profile, 'hs_address_1') ?>
<?= $form->field($profile, 'hs_address_2') ?>
<?= $form->field($profile, 'hs_address_3') ?>
<?= $form->field($profile, 'hs_city') ?>
<?= $form->field($profile, 'hs_state') ?>
<?= $form->field($profile, 'hs_region')->dropDownList(Profile::addressRegionOptions(true), ['prompt' => 'Select Region']) ?>
<?= $form->field($profile, 'hs_zip') ?>
<?= $form->field($profile, 'hs_country') ?>

<hr>

<h4>Billing Contact</h4>
<?= $form->field($profile, 'hs_billing_contact')->checkbox() ?>
<div id="billing_contact" <?= $profile->hs_billing_contact ? 'style="display: none;"' : '' ?>>
    <?= $form->field($profile, 'billing_contact_firstname') ?>
    <?= $form->field($profile, 'billing_contact_lastname') ?>
    <?= $form->field($profile, 'billing_contact_email') ?>
</div>

<hr>

<h4>Billing Address Details</h4>
<?= $form->field($profile, 'billing_address_same')->checkbox() ?>
<div id="billing_address" <?= $profile->billing_address_same ? 'style="display: none;"' : '' ?>>
    <?= $form->field($profile, 'billing_address_1') ?>
    <?= $form->field($profile, 'billing_address_2') ?>
    <?= $form->field($profile, 'billing_address_3') ?>
    <?= $form->field($profile, 'billing_city') ?>
    <?= $form->field($profile, 'billing_state') ?>
    <?= $form->field($profile, 'billing_region')->dropDownList(Profile::addressRegionOptions(true), ['prompt' => 'Select Region']) ?>
    <?= $form->field($profile, 'billing_zip') ?>
    <?= $form->field($profile, 'billing_country') ?>
</div>

<hr>

<h4>Marketing Preferences</h4>
<div class="form-group field-profile-gdpr_version">
    <label class="control-label col-sm-3" for="profile-gdpr_version">GDPR Text Version</label>
    <div class="col-sm-9 gdpr_text_admin">
        <span>&quot;<?= $profile->getGdprText($profile->gdpr_version) ?>&quot;</span>
    </div>
</div>
<?= $form->field($profile, 'gdpr_email')->checkbox(['disabled' => true]) ?>
<?= $form->field($profile, 'gdpr_sms')->checkbox(['disabled' => true]) ?>
<?= $form->field($profile, 'gdpr_phone')->checkbox(['disabled' => true]) ?>
<?= $form->field($profile, 'gdpr_post')->checkbox(['disabled' => true]) ?>

<div class="form-group">
    <div class="col-sm-offset-3 col-sm-9">
        <?= Html::submitButton(Yii::t('usuario', 'Update'), ['class' => 'btn btn-block btn-success']) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

<?php $this->endContent() ?>

<?php
/*
 * This file is part of the 2amigos/yii2-usuario project.
 *
 * (c) 2amigOS! <http://2amigos.us/>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

use Da\User\Helper\TimezoneHelper;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\Profile;

/**
 * @var yii\web\View                $this
 * @var yii\bootstrap\ActiveForm    $form
 * @var app\models\Profile          $model
 * @var TimezoneHelper              $timezoneHelper
 */
$this->title = Yii::t('usuario', 'Profile settings');
$this->params['breadcrumbs'][] = $this->title;
$timezoneHelper = $model->make(TimezoneHelper::class);
?>

<div class="clearfix"></div>

<?= $this->render('/shared/_alert', ['module' => Yii::$app->getModule('user')]) ?>

<div class="row">
    <div class="col-md-3">
        <?= $this->render('/settings/_menu') ?>
    </div>
    <div class="col-md-9">
        <div class="panel panel-default">
            <div class="panel-heading">
                <?= Html::encode($this->title) ?>
            </div>
            <div class="panel-body">
                <?php
                $form = ActiveForm::begin([
                    'id' => $model->formName(),
                    'layout' => 'horizontal',
                    'fieldConfig' => [
                        'horizontalCssClasses' => [
                            'offset' => 'col-sm-offset-3',
                            'label' => 'col-sm-3',
                            'wrapper' => 'col-sm-9',
                            'hint' => '',
                        ],
                    ],
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => true,
                    'validateOnBlur' => false,
                ]);
                ?>

                <?php // Abandoned 2amigos/yii2-usuario core module `profile` fields ?>
                <?php //= $form->field($model, 'name') ?>
                <?php //= $form->field($model, 'website') ?>
                <?php //= $form->field($model, 'location') ?>
                <?php /* =
                  $form
                  ->field($model, 'timezone')
                  ->dropDownList(ArrayHelper::map($timezoneHelper->getAll(), 'timezone', 'name')); */
                ?>

                <h4>Profile Details</h4>
                <?= $form->field($model, 'public_email')->textInput(['readOnly' => true, 'disabled' => true]) ?>
                <?=
                $form->field($model, 'gravatar_email', [
                    'template' => "{label}\n<div class=\"col-sm-9\">{input}</div>\n<div class=\"col-sm-offset-3 col-sm-9\">{error}\n{hint}</div>"
                ])->hint(Html::a(Yii::t('usuario', 'Change your avatar at Gravatar.com'), 'http://gravatar.com', ['target' => '_blank']))
                ?>
                <?= $form->field($model, 'hs_salutation') ?>
                <?= $form->field($model, 'hs_firstname') ?>
                <?= $form->field($model, 'hs_lastname') ?>
                <?= $form->field($model, 'hs_jobtitle') ?>
                <?= $form->field($model, 'hs_mobile_phone') ?>
                <?= $form->field($model, 'hs_phone') ?>
                <?= $form->field($model, 'hs_fax') ?>
                <?= $form->field($model, 'website') ?>
                <?php //= $form->field($model, 'bio')->textarea() ?>

                <hr>
                
                <h4>Company &amp; Address Details</h4>
                <?= $form->field($model, 'hs_company') ?>
                <?= $form->field($model, 'hs_address_1') ?>
                <?= $form->field($model, 'hs_address_2') ?>
                <?= $form->field($model, 'hs_address_3') ?>
                <?= $form->field($model, 'hs_city') ?>
                <?= $form->field($model, 'hs_state') ?>
                <?= $form->field($model, 'hs_region')->dropDownList(Profile::addressRegionOptions(true), ['prompt' => 'Select Region']) ?>
                <?= $form->field($model, 'hs_zip') ?>
                <?= $form->field($model, 'hs_country') ?>

                <hr>
                
                <h4>Billing Contact</h4>
                <?= $form->field($model, 'hs_billing_contact')->checkbox() ?>
                <div id="billing_contact" <?= $model->hs_billing_contact ? 'style="display: none;"' : '' ?>>
                    <?= $form->field($model, 'billing_contact_firstname') ?>
                    <?= $form->field($model, 'billing_contact_lastname') ?>
                    <?= $form->field($model, 'billing_contact_email') ?>
                </div>
                
                <hr>
                
                <h4>Billing Address Details</h4>
                <?= $form->field($model, 'billing_address_same')->checkbox() ?>
                <div id="billing_address" <?= $model->billing_address_same ? 'style="display: none;"' : '' ?>>
                    <?= $form->field($model, 'billing_address_1') ?>
                    <?= $form->field($model, 'billing_address_2') ?>
                    <?= $form->field($model, 'billing_address_3') ?>
                    <?= $form->field($model, 'billing_city') ?>
                    <?= $form->field($model, 'billing_state') ?>
                    <?= $form->field($model, 'billing_region')->dropDownList(Profile::addressRegionOptions(true), ['prompt' => 'Select Region']) ?>
                    <?= $form->field($model, 'billing_zip') ?>
                    <?= $form->field($model, 'billing_country') ?>
                </div>

                <hr>

                <h4>Marketing Preferences</h4>
                <blockquote>
                    <p>&quot;<?= $model->getGdprText() ?>&quot;</p>
                </blockquote>
                <?= $form->field($model, 'gdpr_email')->checkbox() ?>
                <?= $form->field($model, 'gdpr_sms')->checkbox() ?>
                <?= $form->field($model, 'gdpr_phone')->checkbox() ?>
                <?= $form->field($model, 'gdpr_post')->checkbox() ?>
                <?= $form->field($model, 'gdpr_version', ['template' => '{input}', 'options' => ['tag' => false]])->hiddenInput(['value' => $model->getGdprTextVersion(), 'readonly' => true])->label(false) ?>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                        <span id="helpGDPR" class="help-block">Please tick all boxes where you give consent.</span>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                        <?= Html::submitButton(Yii::t('usuario', 'Save'), ['class' => 'btn btn-block btn-success']) ?>
                        <br>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

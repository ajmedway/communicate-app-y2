<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Company */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Companies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'created_date',
            'modified_date',
            'anv_customer_id',
            'hs_company_id',
            'hs_name',
            'hs_phone',
            'hs_address_1',
            'hs_address_2',
            'hs_city',
            'hs_state',
            'hs_region',
            'hs_zip',
            'hs_country',
            'hs_park_building_name',
            'hs_park_building_type',
            'hs_website',
            'hs_domain',
            'hs_industry',
            'hs_description',
            'hs_parent_company_id',
            'hs_hubspot_owner_id',
            'hs_last_modified_date',
            'hs_created_date',
            'CustomerTypeID',
            'CompanyNumber',
            'VATref',
            'PrimaryContactID',
            'PrimaryAddressID',
        ],
    ]) ?>

</div>

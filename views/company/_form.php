<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Company */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="company-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'created_date')->textInput() ?>

    <?= $form->field($model, 'modified_date')->textInput() ?>

    <?= $form->field($model, 'anv_customer_id')->textInput() ?>

    <?= $form->field($model, 'hs_company_id')->textInput() ?>

    <?= $form->field($model, 'hs_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hs_phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hs_address_1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hs_address_2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hs_city')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hs_state')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hs_region')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hs_zip')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hs_country')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hs_park_building_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hs_park_building_type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hs_website')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hs_domain')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hs_industry')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hs_description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hs_parent_company_id')->textInput() ?>

    <?= $form->field($model, 'hs_hubspot_owner_id')->textInput() ?>

    <?= $form->field($model, 'hs_last_modified_date')->textInput() ?>

    <?= $form->field($model, 'hs_created_date')->textInput() ?>

    <?= $form->field($model, 'CustomerTypeID')->textInput() ?>

    <?= $form->field($model, 'CompanyNumber')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'VATref')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'PrimaryContactID')->textInput() ?>

    <?= $form->field($model, 'PrimaryAddressID')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

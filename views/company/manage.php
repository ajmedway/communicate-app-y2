<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\Company;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\Company */


$this->title = Yii::t('usuario', 'Company settings');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="clearfix"></div>

<?php //= $this->render('/shared/_alert', ['module' => Yii::$app->getModule('user')])  ?>

<div class="row">
    <div class="col-md-3">
        <?= $this->render('@app/views/user/settings/_menu') ?>
    </div>
    <div class="col-md-9">
        <div class="panel panel-default">
            <div class="panel-heading">
                <?= Html::encode($this->title) ?>
            </div>
            <div class="panel-body">
                <?php
                $form = ActiveForm::begin([
                    'id' => $model->formName(),
                    'layout' => 'horizontal',
                    'fieldConfig' => [
                        'horizontalCssClasses' => [
                            'offset' => 'col-sm-offset-3',
                            'label' => 'col-sm-3',
                            'wrapper' => 'col-sm-9',
                            'hint' => '',
                        ],
                    ],
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => true,
                    'validateOnBlur' => false,
                ]);
                ?>

                <?= $form->field($model, 'hs_name') ?>
                <?= $form->field($model, 'hs_phone') ?>
                <?= $form->field($model, 'hs_website') ?>
                <?php //= $form->field($model, 'hs_domain') ?>
                <?php //= $form->field($model, 'hs_industry') ?>
                <?= $form->field($model, 'hs_description') ?>
                <?= $form->field($model, 'CustomerTypeID')->dropDownList(Company::customerTypeOptions(), ['prompt' => 'Select Customer Type']) ?>
                <?= $form->field($model, 'CompanyNumber') ?>
                <?= $form->field($model, 'VATref') ?>

                <?php //= $form->field($model, 'hs_address_1') ?>
                <?php //= $form->field($model, 'hs_address_2') ?>
                <?php //= $form->field($model, 'hs_city') ?>
                <?php //= $form->field($model, 'hs_state') ?>
                <?php //= $form->field($model, 'hs_region') ?>
                <?php //= $form->field($model, 'hs_zip') ?>
                <?php //= $form->field($model, 'hs_country') ?>

                <?php //= $form->field($model, 'hs_park_building_name')  ?>
                <?php //= $form->field($model, 'hs_park_building_type') ?>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                        <?= Html::submitButton(Yii::t('usuario', 'Save'), ['class' => 'btn btn-block btn-success']) ?>
                        <br>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\CompanySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Companies';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Company', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'created_date',
            'modified_date',
            'anv_customer_id',
            'hs_company_id',
            //'hs_name',
            //'hs_phone',
            //'hs_address_1',
            //'hs_address_2',
            //'hs_city',
            //'hs_state',
            //'hs_region',
            //'hs_zip',
            //'hs_country',
            //'hs_park_building_name',
            //'hs_park_building_type',
            //'hs_website',
            //'hs_domain',
            //'hs_industry',
            //'hs_description',
            //'hs_parent_company_id',
            //'hs_hubspot_owner_id',
            //'hs_last_modified_date',
            //'hs_created_date',
            //'CustomerTypeID',
            //'CompanyNumber',
            //'VATref',
            //'PrimaryContactID',
            //'PrimaryAddressID',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CompanySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="company-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'created_date') ?>

    <?= $form->field($model, 'modified_date') ?>

    <?= $form->field($model, 'anv_customer_id') ?>

    <?= $form->field($model, 'hs_company_id') ?>

    <?php // echo $form->field($model, 'hs_name') ?>

    <?php // echo $form->field($model, 'hs_phone') ?>

    <?php // echo $form->field($model, 'hs_address_1') ?>

    <?php // echo $form->field($model, 'hs_address_2') ?>

    <?php // echo $form->field($model, 'hs_city') ?>

    <?php // echo $form->field($model, 'hs_state') ?>

    <?php // echo $form->field($model, 'hs_region') ?>

    <?php // echo $form->field($model, 'hs_zip') ?>

    <?php // echo $form->field($model, 'hs_country') ?>

    <?php // echo $form->field($model, 'hs_park_building_name') ?>

    <?php // echo $form->field($model, 'hs_park_building_type') ?>

    <?php // echo $form->field($model, 'hs_website') ?>

    <?php // echo $form->field($model, 'hs_domain') ?>

    <?php // echo $form->field($model, 'hs_industry') ?>

    <?php // echo $form->field($model, 'hs_description') ?>

    <?php // echo $form->field($model, 'hs_parent_company_id') ?>

    <?php // echo $form->field($model, 'hs_hubspot_owner_id') ?>

    <?php // echo $form->field($model, 'hs_last_modified_date') ?>

    <?php // echo $form->field($model, 'hs_created_date') ?>

    <?php // echo $form->field($model, 'CustomerTypeID') ?>

    <?php // echo $form->field($model, 'CompanyNumber') ?>

    <?php // echo $form->field($model, 'VATref') ?>

    <?php // echo $form->field($model, 'PrimaryContactID') ?>

    <?php // echo $form->field($model, 'PrimaryAddressID') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

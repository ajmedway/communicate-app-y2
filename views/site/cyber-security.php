<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Cyber Security Products Enquiry Form';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        If you have enquiries relating to cyber security products, please fill out the following form to contact us.
        Thank you.
    </p>
    
    <div class="row">
        <div class="col-lg-12">

            <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/shell.js"></script>
            <script>
                hbspt.forms.create({
                    portalId: "3838015",
                    formId: "89b21dab-cf5a-4b93-abb3-9e915d6d9d38"
                });
            </script>

        </div>
    </div>
</div>

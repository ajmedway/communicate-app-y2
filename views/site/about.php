<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="about-us-text col-md-6">
            <h2>What We Do</h2>
            <p>We allow our clients to rent telephone handsets via our IP Telephone Systems, servers in our onsite data centres, PC's on desks, laptops, Backup and Disaster Recovery services, shared printers, remote monitoring services and software supply and support.</p>
            <p>All services are primarily based on a simple per user per month basis (PUPM) making it easy for clients to add and remove users as the business requirements change. Certain services are contracted over 1, 3, 5 or 10 year terms, whilst others provide easy in easy out, low cost monthly contracts making it simple for clients to forecast and budget.</p>
        </div>
        <div class="about-us-text col-md-6">
            <h2>Why Choose Us</h2>
            <p>Clients need the confidence of a long term, reputable and stable business partner when it comes to IT support and services as effectively we become part of their business. Communicate PLC provides the client with this confidence by investing in onsite, high quality offices, personnel, equipment and technology.</p>
            <p>Most businesses wish to have one provider for all of their IT services, from telephones and broadband, to IT support and hardware maintenance. At Communicate PLC, we pride ourselves on being able to provide a wide range of IT services that are all managed and supported in-house by our very own dedicated team of engineers.</p>
        </div>
    </div>

    <div class="row" style="margin-top: 20px;">
        <div class="about-us-text col-md-6">
            <h4>Where We Are</h4>
            <p>Communicate PLC currently support 3 Business Parks spread across the UK with over 200 clients and approximately 5,000 end users.</p>
            <img class="about-img" src="https://communicateplc.com/assets/img/locations/wynyard-park.jpg" alt="Wynyard Business Park Image">
        </div>
        <div class="about-us-text col-md-6">
            <h4>Wynyard Business Park</h4>
            <p><a href="http://wynyardbusinesspark.com/" target="_blank">Wynyard Business Park</a> is the UK headquarters of Communicate Technology PLC and was our first managed business park where we proved the concept of delivering IT and Telecoms services over a converged infrastructure. We have tenants based both on the park and throughout the North all served by direct links to our network. After Initial trials were proved successful we rolled out the services to all tenants and now provide connections to over 80% of the parks occupiers.</p>
            <p>We have gigabit internet connections in our dedicated server rooms with telecoms, networking and audio visual services all ready to have clients connected instantly.</p>
            <p>Wynyard is also home to our remote support and help desk facilities for all of our locations where client's equipment and incoming services are monitored and reported 24x7. It is also home to R&amp;D where proof of concept and customer trials are carried out before roll out of new services to clients.
                <br>
                Our onsite engineers work with clients everyday helping to support and improve their IT and telecoms delivery and provide friendly, no nonsense advice to help grow their business.</p>
        </div>
    </div>
</div>

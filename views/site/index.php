<?php

use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'Communicate Hub';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1><?= $this->title ?></h1>

        <p class="lead">Welcome to the Communicate Quote Management Hub, <br>a convenient place to create a new quote or manage an existing one.</p>

        <p><a class="btn btn-lg btn-success" href="<?= Url::toRoute('/quote/manage') ?>">Get started with your Quote</a></p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Don't have an account?</h2>

                <p>Go ahead and sign up here!</p>

                <p><a class="btn btn-default" href="<?= Url::toRoute('/user/register') ?>">Create Account</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Already signed up?</h2>

                <p>Go ahead and login here!</p>

                <p><a class="btn btn-default" href="<?= Url::toRoute('/user/login') ?>">Account Login</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Forgot you password?</h2>

                <p>Click below to get some help.</p>

                <p><a class="btn btn-default" href="<?= Url::toRoute('/user/forgot') ?>">Recover Your Password</a></p>
            </div>
        </div>

    </div>
</div>

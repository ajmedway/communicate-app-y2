/* Communicate App Custom Javascript */

/**
 * Copyright © 2018 r//evolution Marketing. All rights reserved.
 * 
 * @author Alec Pritchard <alec@r-evolution.co.uk>
 */

// Ensure to avoid conflicts with other libraries using the $ variable
jQuery.noConflict();

jQuery(document).ready(function ($) {
    
    // Bootstrap Tooltips initialiser
    $(function () {
        $("[data-toggle='tooltip']").tooltip();
    });

    // Bootstrap Popovers initialiser
    $(function () {
        $("[data-toggle='popover']").popover();
    });
    
    // Handler function to filter out non-numeric float keys on keydown
    $(".numericFloatKeysOnly").keydown(function (e) {
        
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl/cmd+A
            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: Ctrl/cmd+C
            (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: Ctrl/cmd+X
            (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
        
    });
    
    // Quote form scripts
    if ($("#quote-form").length) {
        // Listen for changes on the quote form
        $(document).on("change", "#quote-form", function () {
            // Reload quote totals pjax container
            $.pjax.reload({
                type: "post",
                url: "/quote/get-totals",
                container: "#quote-totals",
                push: false,
                replace: false,
                scrollTo: false,
                timeout: 1000,
                data: $("#quote-form").serialize()
            });
        });
        
        // Calculate height of the totals bar, modify wrap css to accommodate it
        function wrapFooterHeightAdjust() {
            let footerHeight = $("footer").outerHeight();
            let totalsHeight = $("#quote-totals").outerHeight();
            let wrapFooterHeightAdjustment = footerHeight + totalsHeight;
            $(".wrap").css({
                marginBottom: '-' + wrapFooterHeightAdjustment + 'px',
                paddingBottom: wrapFooterHeightAdjustment + 'px'
            });
        }
        wrapFooterHeightAdjust();
        
        // Trigger readjustment on window resize
        $(window).resize(function () {
            wrapFooterHeightAdjust();
        });
    }
    
    if ($("#profile-hs_billing_contact").length) {
        $(document).on("change", "#profile-hs_billing_contact", function () {
            if ($(this).is(":checked")) {
                $("#billing_contact").slideUp(250);
            } else {
                $("#billing_contact").slideDown(250);
            }
        });
    }
    
    if ($("#profile-billing_address_same").length) {
        $(document).on("change", "#profile-billing_address_same", function () {
            if ($(this).is(":checked")) {
                $("#billing_address").slideUp(500);
            } else {
                $("#billing_address").slideDown(500);
            }
        });
    }
    
});

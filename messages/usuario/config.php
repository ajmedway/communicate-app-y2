<?php

/*
 * This file is part of the 2amigos/yii2-usuario project.
 *
 * (c) 2amigOS! <http://2amigos.us/>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

/**
 * After composer update, run this: ./yii message messages/usuario/config.php
 * Will update the custom message sources with new elements in vendor dir
 */

return [
    'sourcePath' => \Yii::getAlias('@vendor/2amigos/yii2-usuario/src/User'),
    'messagePath' => \Yii::getAlias('@app/messages/usuario'),
    'languages' => [
        'en-GB',
    ],
    'translator' => 'Yii::t',
    'sort' => false,
    // overwrite true = merge new messages in, keep customised ones
    'overwrite' => true,
    'removeUnused' => false,
    'only' => ['*.php'],
    'except' => [
        '.svn',
        '.git',
        '.gitignore',
        '.gitkeep',
        '.hgignore',
        '.hgkeep',
    ],
    'format' => 'php',
];

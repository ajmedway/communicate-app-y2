<?php

namespace app\modules\product;

use Yii;
use yii\base\Module;
use yii\base\BootstrapInterface;
use yii\web\Application as WebApplication;

/**
 * product module definition class
 */
class Product extends Module implements BootstrapInterface
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\product\controllers';

    /**
     * @var string the route prefix
     */
    public $prefix = 'product';

    /**
     * @var array the url rules (routes)
     */
    public $routes = [
        '<action:[\w-]+>' => 'product/<action>',
    ];

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

    /**
     * {@inheritdoc}
     *
     * @throws InvalidConfigException
     */
    public function bootstrap($app)
    {
        if ($app->hasModule('product') && $app->getModule('product') instanceof Module) {
            if ($app instanceof WebApplication) {
                $this->initUrlRoutes($app);
            }
        }
    }

    /**
     * Initializes web url routes (rules in Yii2).
     *
     * @param WebApplication $app
     *
     * @throws InvalidConfigException
     */
    protected function initUrlRoutes(WebApplication $app)
    {
        /** @var $module Module */
        $module = $app->getModule('product');
        $config = [
            'class' => 'yii\web\GroupUrlRule',
            'prefix' => $module->prefix,
            'rules' => $module->routes,
        ];

        if ($module->prefix !== 'product') {
            $config['routePrefix'] = 'product';
        }

        $rule = Yii::createObject($config);

        $app->getUrlManager()->addRules([$rule], false);
    }

}

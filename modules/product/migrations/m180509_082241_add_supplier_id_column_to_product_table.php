<?php

namespace app\modules\product\migrations;

use yii\db\Migration;

/**
 * Handles adding supplier_id to table `product`.
 * Has foreign keys to the tables:
 *
 * - `supplier`
 */
class m180509_082241_add_supplier_id_column_to_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('product', 'supplier_id', $this->integer()->after('id')->defaultValue(NULL));

        // creates index for column `supplier_id`
        $this->createIndex(
            'idx-product-supplier_id',
            'product',
            'supplier_id'
        );

        // add foreign key for table `supplier`
        $this->addForeignKey(
            'fk-product-supplier_id',
            'product',
            'supplier_id',
            'supplier',
            'supplier_id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `supplier`
        $this->dropForeignKey(
            'fk-product-supplier_id',
            'product'
        );

        // drops index for column `supplier_id`
        $this->dropIndex(
            'idx-product-supplier_id',
            'product'
        );

        $this->dropColumn('product', 'supplier_id');
    }
}

<?php

namespace app\modules\product\migrations;

use yii\db\Migration;

/**
 * Handles adding active to table `product`.
 */
class m180621_094933_add_active_column_to_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('product', 'active', $this->boolean()->after('modified_date')->defaultValue(1));
        
        // creates index for column `active`
        $this->createIndex(
            'idx-product-active',
            'product',
            'active'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops index for column `active`
        $this->dropIndex(
            'idx-product-active',
            'product'
        );
        
        $this->dropColumn('product', 'active');
    }
}

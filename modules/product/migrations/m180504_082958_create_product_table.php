<?php

namespace app\modules\product\migrations;

use yii\db\Migration;

/**
 * Handles the creation of table `product`.
 */
class m180504_082958_create_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('product', [
            'id' => $this->primaryKey(),
            'created_date' => $this->dateTime()->defaultValue(NULL),
            'modified_date' => $this->dateTime()->defaultValue(NULL),
            'ChargeProductID' => $this->integer()->defaultValue(NULL)->comment('Charge ID - Anvil primary key'),
            'CompanyID' => $this->integer()->defaultValue(NULL)->comment('Owner (if any)'),
            'Description' => $this->string()->defaultValue(NULL)->comment('Descriptive text, e.g. Broadband rental'),
            'BillingFrequency' => $this->string()->defaultValue(NULL)->comment('One of: OneOff, Monthly, Quarterly, Annual'),
            'CostInitial' => $this->decimal(15,2)->defaultValue(NULL)->comment('Initial setup cost (supplier) for service'),
            'CostRecurring' => $this->decimal(15,2)->defaultValue(NULL)->comment('Recurring cost (supplier) for service'),
            'ChargeInitial' => $this->decimal(15,2)->defaultValue(NULL)->comment('Initial setup charge to customer'),
            'ChargeRecurring' => $this->decimal(15,2)->defaultValue(NULL)->comment('Recurring periodic charge to customer'),
            'SupplierAccountID' => $this->integer()->defaultValue(NULL)->comment('Supplier account the charges relate to (see /api/V1/SupplierAccounts)'),
            'ProductID' => $this->string()->defaultValue(NULL)->comment('The ProductID if the charge was created from a Product (optional)'),
            'CurrencyID' => $this->string()->defaultValue(NULL)->comment('Currency for the charge (normally GBP)'),
            'CountryID' => $this->string()->defaultValue(NULL)->comment('Country the charge is billed in (normally GB)'),
            'TaxRateID' => $this->integer()->defaultValue(NULL)->comment('The tax rate that applies for this charge (see /api/V1/TaxRates)'),
        ]);
        
        // creates index for column `ChargeProductID`
        $this->createIndex(
            'idx_ChargeProductID',
            'product',
            'ChargeProductID',
            true
        );
        
        // creates index for column `CompanyID`
        $this->createIndex(
            'idx_CompanyID',
            'product',
            'CompanyID'
        );
        
        // creates index for column `ProductID`
        $this->createIndex(
            'idx_ProductID',
            'product',
            'ProductID'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('product');
    }
}

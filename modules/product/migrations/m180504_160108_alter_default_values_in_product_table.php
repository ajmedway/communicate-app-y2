<?php

namespace app\modules\product\migrations;

use yii\db\Migration;

/**
 * Class m180504_160108_alter_default_values_in_product_table
 */
class m180504_160108_alter_default_values_in_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('product', 'CurrencyID', $this->string()->defaultValue('GBP'));
        $this->alterColumn('product', 'CountryID', $this->string()->defaultValue('GB'));
        $this->alterColumn('product', 'TaxRateID', $this->integer()->defaultValue(1));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180504_160108_alter_default_values_in_product_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180504_160108_alter_default_values_in_product_table cannot be reverted.\n";

        return false;
    }
    */
}

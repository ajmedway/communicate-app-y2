<?php

namespace app\modules\product\migrations;

use yii\db\Migration;

/**
 * Handles the creation of table `supplier`.
 */
class m180508_175108_create_supplier_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('supplier', [
            'supplier_id' => $this->primaryKey(),
            'created_date' => $this->dateTime()->defaultValue(NULL),
            'modified_date' => $this->dateTime()->defaultValue(NULL),
            'ID' => $this->integer()->defaultValue(NULL)->comment('Anvil Supplier Account ID'),
            'Name' => $this->string()->defaultValue(NULL),
        ]);
        
        // creates index for column `ID`
        $this->createIndex(
            'idx_ID',
            'supplier',
            'ID',
            true
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('supplier');
    }
}

<?php

namespace app\modules\product\migrations;

use yii\db\Migration;

/**
 * Handles the creation of table `product_category`.
 */
class m180525_083401_create_product_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('product_category', [
            'id' => $this->primaryKey(),
            'ProductID' => $this->string()->defaultValue(NULL),
            'ProductGroupID' => $this->string()->defaultValue(NULL),
            'Notes' => $this->string()->defaultValue(NULL),
        ]);
        
        // creates index for column `ProductID`
        $this->createIndex(
            'idx-product_category-ProductID',
            'product_category',
            'ProductID'
        );
        
        // creates index for column `ProductGroupID`
        $this->createIndex(
            'idx-product_category-ProductGroupID',
            'product_category',
            'ProductGroupID'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('product_category');
    }
}

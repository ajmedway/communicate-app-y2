<?php

namespace app\modules\product\controllers;

use Yii;
use app\modules\product\models\Product;
use app\modules\product\models\ProductSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use Da\User\Filter\AccessRuleFilter;
use app\models\Company;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRuleFilter::class,
                ],
                'rules' => [
                    [
                        // open all product controller actions to admins only
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Product model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
                'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Product();
        $model->loadDefaultValues(true);
        $model->setAnvilCompanyId();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->anvilSyncProduct($id);
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
                'model' => $model,
        ]);
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (empty($model->CompanyID)) {
            // prevent update of product if a global Anvil ChargeProduct
            return $this->redirect(['view', 'id' => $model->id]);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->anvilSyncProduct($id);
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
                'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        
        $disabled = true;
        if ($disabled) {
            // can we support deletion of ChargeProducts? TBC
            Yii::$app->session->setFlash('warning', "<strong>Delete function disabled.</strong> Please contact the r//evolution development team about this issue.");
            return $this->redirect(['view', 'id' => $model->id]);
        }
        
        if (empty($model->CompanyID)) {
            // prevent delete of product if this is a global Anvil ChargeProduct
            return $this->redirect(['view', 'id' => $model->id]);
        }

        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Imports ChargeProduct data from Anvil to create/update Products
     */
    public function actionAnvilImportProducts()
    {
        $anvil = Yii::$app->anvilDataManager;

        if (($CompanyID = Company::getAnvilCompanyId()) === null) {
            return false;
        }

        $totalCreated = $totalUpdated = 0;
        $anvilProducts = $anvil->getChargeProducts($CompanyID);

        if (is_array($anvilProducts)) {

            foreach ($anvilProducts as $anvilProduct) {

                if (empty($anvilProduct['ChargeProductID'])) {
                    continue;
                }

                $properties = $anvil->mapPropertiesAnvilToNative($anvilProduct, $anvil::$ANVTYPE_CHARGEPRODUCT, 'POST', ['ChargeProductID' => $anvilProduct['ChargeProductID']]);

                // load existing or create new model
                if (($model = Product::findOne(['ChargeProductID' => $anvilProduct['ChargeProductID']])) === null) {
                    $model = new Product();
                    $model->loadDefaultValues(true);
                    $model->setAnvilCompanyId();
                }

                if ($model->load($properties, '')) {
                    if ($model->getIsNewRecord() && $model->save()) {
                        $totalCreated++;
                    } else if ($model->save()) {
                        $totalUpdated++;
                    }
                }
            }

            echo '<pre>';
            echo 'total $anvilProducts ' . print_r(count($anvilProducts), true);
            echo '</pre>';
            echo '<pre>';
            echo '$totalCreated ' . print_r($totalCreated, true);
            echo '</pre>';
            echo '<pre>';
            echo '$totalUpdated ' . print_r($totalUpdated, true);
            echo '</pre>';
            exit;
        }
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Sync Anvil ChargeProduct data with native Product data
     * 
     * @param integer $id
     * @return bool success
     */
    protected function anvilSyncProduct($id)
    {
        if (($model = Product::findOne($id)) === null || empty($model->CompanyID) || $model->CompanyID !== $model->getAnvilCompanyId()) {
            return false;
        }

        // check if this charge product exists in Anvil
        $anvilExistingChargeProduct = Yii::$app->anvilDataManager->getChargeProduct($model->CompanyID, $model->ChargeProductID);
        
        if (!empty($model->ChargeProductID) && !empty($anvilExistingChargeProduct)) {
            // update ChargeProduct
            if (($ChargeProduct = Yii::$app->anvilDataManager->updateChargeProduct($model->ChargeProductID, $model)) === false) {
                $errorMsg = "Unable to update ChargeProduct record in Anvil for Product #{$model->id}";
                Yii::error(['message' => $errorMsg, '$model' => $model], __METHOD__);
                Yii::$app->session->setFlash('error', "<strong>Error P-ASP-001.</strong> {$errorMsg}");
            } else {
                Yii::$app->session->setFlash('success', "<strong>Success.</strong> Updated ChargeProduct record in Anvil for ChargeProductID #{$model->ChargeProductID}");
                return true;
            }
        } else {
            // create ChargeProduct
            if (($ChargeProduct = Yii::$app->anvilDataManager->createChargeProduct($model)) === false) {
                $errorMsg = "Unable to create ChargeProduct record in Anvil for Product #{$model->id}";
                Yii::error(['message' => $errorMsg, '$model' => $model], __METHOD__);
                Yii::$app->session->setFlash('error', "<strong>Error P-ASP-002.</strong> {$errorMsg}");
            } else {
                // update model with new ChargeProductID from Anvil system
                $model->ChargeProductID = $ChargeProduct['ChargeProductID'];
                $model->save(false);
                Yii::$app->session->setFlash('success', "<strong>Success.</strong> Created ChargeProduct in Anvil under ChargeProductID #{$model->ChargeProductID}");
                return true;
            }
        }

        return false;
    }

}

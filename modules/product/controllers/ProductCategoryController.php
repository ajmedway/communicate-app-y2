<?php

namespace app\modules\product\controllers;

use Yii;
use app\modules\product\models\ProductCategory;
use app\modules\product\models\ProductCategorySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use Da\User\Filter\AccessRuleFilter;

/**
 * ProductCategoryController implements the CRUD actions for ProductCategory model.
 */
class ProductCategoryController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRuleFilter::class,
                ],
                'rules' => [
                    [
                        // open all product controller actions to admins only
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ProductCategory models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductCategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ProductCategory model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ProductCategory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ProductCategory();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ProductCategory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ProductCategory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Imports Product (Category) data from Anvil to create/update ProductCategories
     */
    public function actionAnvilImportProductCategories()
    {
        $anvil = Yii::$app->anvilDataManager;
        
        $totalCreated = $totalUpdated = 0;
        $anvilProductCategories = $anvil->getProductCategories();

        if (is_array($anvilProductCategories)) {

            foreach ($anvilProductCategories as $anvilProductCategory) {

                $properties = $anvil->mapPropertiesAnvilToNative($anvilProductCategory, $anvil::$ANVTYPE_PRODUCTCATEGORY);

                // load existing or create new model
                if (($model = ProductCategory::findOne(['ProductID' => $properties['ProductID']])) === null) {
                    $model = new ProductCategory();
                }

                if ($model->load($properties, '')) {
                    if ($model->getIsNewRecord() && $model->save()) {
                        $totalCreated++;
                    } else if ($model->save()) {
                        $totalUpdated++;
                    }
                }
            }
            
            echo '<pre>';
            echo 'total $anvilProductCategories ' . print_r(count($anvilProductCategories), true);
            echo '</pre>';
            echo '<pre>';
            echo '$totalCreated ' . print_r($totalCreated, true);
            echo '</pre>';
            echo '<pre>';
            echo '$totalUpdated ' . print_r($totalUpdated, true);
            echo '</pre>';
            exit;
        }
    }

    /**
     * Finds the ProductCategory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProductCategory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProductCategory::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

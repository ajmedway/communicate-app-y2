<?php

namespace app\modules\product\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\product\models\Product;

/**
 * ProductSearch represents the model behind the search form of `app\modules\product\models\Product`.
 */
class ProductSearch extends Product
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['active'], 'integer', 'max' => 1],
            [['id', 'supplier_id', 'active', 'ChargeProductID', 'CompanyID', 'SupplierAccountID', 'TaxRateID'], 'integer'],
            [['created_date', 'modified_date', 'Description', 'BillingFrequency', 'ProductID', 'CurrencyID', 'CountryID'], 'safe'],
            [['CostInitial', 'CostRecurring', 'ChargeInitial', 'ChargeRecurring'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Product::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'supplier_id' => $this->supplier_id,
            'created_date' => $this->created_date,
            'modified_date' => $this->modified_date,
            'active' => $this->active,
            'ChargeProductID' => $this->ChargeProductID,
            'CompanyID' => $this->CompanyID,
            'CostInitial' => $this->CostInitial,
            'CostRecurring' => $this->CostRecurring,
            'ChargeInitial' => $this->ChargeInitial,
            'ChargeRecurring' => $this->ChargeRecurring,
            'SupplierAccountID' => $this->SupplierAccountID,
            'TaxRateID' => $this->TaxRateID,
        ]);

        $query->andFilterWhere(['like', 'Description', $this->Description])
            ->andFilterWhere(['like', 'BillingFrequency', $this->BillingFrequency])
            ->andFilterWhere(['like', 'ProductID', $this->ProductID])
            ->andFilterWhere(['like', 'CurrencyID', $this->CurrencyID])
            ->andFilterWhere(['like', 'CountryID', $this->CountryID]);

        return $dataProvider;
    }
}

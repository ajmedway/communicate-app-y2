<?php

namespace app\modules\product\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\modules\product\models\ProductCategory;
use app\modules\product\models\Supplier;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property int $supplier_id
 * @property string $created_date
 * @property string $modified_date
 * @property int $active
 * @property int $ChargeProductID Charge ID - Anvil primary key
 * @property int $CompanyID Owner (if any)
 * @property string $Description Descriptive text, e.g. Broadband rental
 * @property string $BillingFrequency One of: OneOff, Monthly, Quarterly, Annual
 * @property double $CostInitial Initial setup cost (supplier) for service
 * @property double $CostRecurring Recurring cost (supplier) for service
 * @property double $ChargeInitial Initial setup charge to customer
 * @property double $ChargeRecurring Recurring periodic charge to customer
 * @property int $SupplierAccountID Supplier account the charges relate to (see /api/V1/SupplierAccounts)
 * @property string $ProductID The ProductID if the charge was created from a Product (optional)
 * @property string $CurrencyID Currency for the charge (normally GBP)
 * @property string $CountryID Country the charge is billed in (normally GB)
 * @property int $TaxRateID The tax rate that applies for this charge (see /api/V1/TaxRates)
 * 
 * Defined relations:
 * @property Supplier $supplier
 * @property ProductCategory $productCategory
 */
class Product extends \yii\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_date', 'modified_date'], 'safe'],
            [['ChargeProductID'], 'unique'],
            [['supplier_id', 'TaxRateID', 'Description', 'BillingFrequency', 'ProductID', 'CostInitial', 'CostRecurring', 'ChargeInitial', 'ChargeRecurring'], 'required'],
            [['supplier_id', 'active', 'ChargeProductID', 'CompanyID', 'SupplierAccountID', 'TaxRateID'], 'integer'],
            [['active'], 'integer', 'max' => 1],
            [['CostInitial', 'CostRecurring', 'ChargeInitial', 'ChargeRecurring'], 'number'],
            [['Description', 'BillingFrequency', 'ProductID', 'CurrencyID', 'CountryID'], 'string', 'max' => 255],
            [['supplier_id'], 'exist', 'skipOnError' => true, 'targetClass' => Supplier::className(), 'targetAttribute' => ['supplier_id' => 'supplier_id']],
            [['ProductID'], 'exist', 'skipOnError' => true, 'targetClass' => ProductCategory::className(), 'targetAttribute' => ['ProductID' => 'ProductID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'supplier_id' => 'Supplier',
            'created_date' => 'Created Date',
            'modified_date' => 'Modified Date',
            'active' => 'Active',
            'ChargeProductID' => 'Charge Product ID',
            'CompanyID' => 'Company ID',
            'Description' => 'Description',
            'BillingFrequency' => 'Billing Frequency',
            'CostInitial' => 'Cost Initial',
            'CostRecurring' => 'Cost Recurring',
            'ChargeInitial' => 'Charge Initial',
            'ChargeRecurring' => 'Charge Recurring',
            'SupplierAccountID' => 'Supplier Account ID',
            'ProductID' => 'Category',
            'CurrencyID' => 'Currency ID',
            'CountryID' => 'Country ID',
            'TaxRateID' => 'Tax Rate',
        ];
    }

    /**
     * @param bool return values as keys
     * @return array Anvil Billing Frequency options
     */
    public static function billingFrequencyOptions($valuesAsKeys = false)
    {
        $data = [
            'OneOff',
            'Monthly',
            'Quarterly',
            'Annual',
        ];

        return $valuesAsKeys ? array_combine($data, $data) : $data;
    }

    /**
     * @return array Anvil Contract Duration options
     */
    public static function contractDurationOptions()
    {
        return [
            0 => 'Not Set',
            1 => 'One Month',
            3 => 'Three Months',
            6 => 'Six Months',
            12 => 'One Year',
            24 => 'Two Years',
            36 => 'Three Years',
            48 => 'Four Years',
            60 => 'Five Years',
            72 => 'Six Years',
            84 => 'Seven Years',
        ];
    }

    /**
     * @return array Anvil Tax Rate options
     */
    public static function taxRateOptions()
    {
        return [
            1 => 'S 20.00%',
            2 => 'E 0.00%',
            3 => 'Z 0.00%',
        ];
    }

    /**
     * Get the Anvil CompanyID from app config params
     * 
     * @return int|null
     */
    public function getAnvilCompanyId()
    {
        return isset(Yii::$app->params['anvilCompanyId']) ? Yii::$app->params['anvilCompanyId'] : null;
    }

    /**
     * Set the Anvil CompanyID from app config params
     * 
     * @return void
     */
    public function setAnvilCompanyId()
    {
        $this->CompanyID = $this->getAnvilCompanyId();
    }

    /**
     * Get the instance Tax Rate option name
     * 
     * @return string
     */
    public function getTaxRateName()
    {
        return ArrayHelper::getValue(self::taxRateOptions(), $this->TaxRateID, '');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupplier()
    {
        return $this->hasOne(Supplier::className(), ['supplier_id' => 'supplier_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductCategory()
    {
        return $this->hasOne(ProductCategory::className(), ['ProductID' => 'ProductID']);
    }

    /**
     * {@inheritdoc}
     * @return ProductQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProductQuery(get_called_class());
    }

    /**
     * Handle any nuances of data in the model before we commit to insert/update
     * 
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $supplier = Supplier::findOne($this->supplier_id);
            $this->SupplierAccountID = !empty($supplier->ID) ? $supplier->ID : null;

            $this->modified_date = new \yii\db\Expression('NOW()');
            if (empty($this->created_date)) {
                $this->created_date = new \yii\db\Expression('NOW()');
            }
            return true;
        } else {
            return false;
        }
    }

}

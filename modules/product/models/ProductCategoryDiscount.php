<?php

namespace app\modules\product\models;

use Yii;
use yii\base\Model;

/**
 * ProductCategoryDiscount is the model behind the product category discounts.
 *
 * @property string $ProductID
 * @property int $min_quantity
 * @property int $min_duration_months
 * @property string $type
 * @property double $amount
 */
class ProductCategoryDiscount extends Model
{

    public static $TYPES = [
        'FIXED_SUM' => 'FIXED_SUM',
        'PERCENTAGE' => 'PERCENTAGE',
    ];
    public $ProductID;
    public $min_quantity;
    public $min_duration_months;
    public $type;
    public $amount;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['min_duration_months', 'min_quantity'], 'integer'],
            [['amount'], 'number'],
            [['ProductID'], 'string'],
            ['type', 'in', 'range' => self::$TYPES, 'allowArray' => true],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ProductID' => 'Anvil Product (Category) ID',
            'min_quantity' => 'Minimum Quantity',
            'min_duration_months' => 'Minimum Duration (Months)',
            'type' => 'Type',
            'amount' => 'Amount',
        ];
    }

    /**
     * Use discount instance to calculate it's effect on a given price.
     * 
     * @param float $price
     * @return int Discounted price
     */
    public function applyDiscountToPrice($price)
    {
        if (is_numeric($price)) {
            // calculate discounts
            if ($this->type === ProductCategoryDiscount::$TYPES['FIXED_SUM']) {
                // if not a percentage, subtract from price outright
                $priceDiscounted = $price - $this->amount;
            } else if ($this->type === ProductCategoryDiscount::$TYPES['PERCENTAGE']) {
                // subtract $amount % of $price, from $price
                $priceDiscounted = ($price * ((100 - $this->amount) / 100));
            }
        }

        return !empty($priceDiscounted) ? $priceDiscounted : $price;
    }

}

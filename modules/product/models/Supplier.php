<?php

namespace app\modules\product\models;

use Yii;

/**
 * This is the model class for table "supplier".
 *
 * @property int $supplier_id
 * @property string $created_date
 * @property string $modified_date
 * @property int $ID Anvil Supplier Account ID
 * @property string $Name
 *
 * @property Product[] $products
 */
class Supplier extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'supplier';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_date', 'modified_date'], 'safe'],
            [['ID'], 'unique'],
            [['ID'], 'integer'],
            [['Name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'supplier_id' => 'Supplier ID',
            'created_date' => 'Created Date',
            'modified_date' => 'Modified Date',
            'ID' => 'Supplier Account ID',
            'Name' => 'Supplier Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['supplier_id' => 'supplier_id']);
    }

    /**
     * {@inheritdoc}
     * @return SupplierQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SupplierQuery(get_called_class());
    }
    
    /**
     * Handle any nuances of data in the model before we commit to insert/update
     * 
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->modified_date = new \yii\db\Expression('NOW()');
            if (empty($this->created_date)) {
                $this->created_date = new \yii\db\Expression('NOW()');
            }
            return true;
        } else {
            return false;
        }
    }
}

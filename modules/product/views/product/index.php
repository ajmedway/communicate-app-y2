<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\product\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Product', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'contentOptions' => ['style' => 'width: 80px;'],
            ],
            //'supplier_id',
            'ChargeProductID',
            'Description',
            'ProductID',
            'active:boolean',
            //'created_date',
            'modified_date',
            //'CompanyID',
            //'BillingFrequency',
            //'CostInitial',
            //'CostRecurring',
            //'ChargeInitial',
            //'ChargeRecurring',
            //'SupplierAccountID',
            //'CurrencyID',
            //'CountryID',
            //'TaxRateID',

            [
                'class' => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'view' => function ($model) {
                        return true;
                    },
                    'update' => function ($model) {
                        return !empty($model->CompanyID);
                    },
                    'delete' => function ($model) {
                        return !empty($model->CompanyID);
                    },
                ]
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>

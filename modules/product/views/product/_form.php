<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\components\helpers\Template;
use app\modules\product\models\Product;
use app\modules\product\models\ProductCategory;
use app\modules\product\models\Supplier;

/* @var $this yii\web\View */
/* @var $model app\modules\product\models\Product */
/* @var $form yii\widgets\ActiveForm */

$categories = ProductCategory::find()->orderBy('ProductGroupID ASC', 'ProductID ASC')->all();
$categoryOptions = ArrayHelper::map(
    $categories,
    'ProductID',
    function($model) {
        return $model['ProductGroupID'] . ' > ' . $model['ProductID'];
    }
);

$suppliers = Supplier::find()->orderBy('name')->all();
$supplierOptions = ArrayHelper::map($suppliers, 'supplier_id', 'Name');
?>

<div class="product-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'Description')->textInput(['maxlength' => true, 'placeholder' => 'Enter Product Description']) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'ProductID')->dropDownList($categoryOptions, ['prompt' => 'Select Product Category']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-3">
            <?= $form->field($model, 'CostInitial', [
                'template' => Template::getTextInputAddonTemplate('glyphicon-gbp', true)
            ])->textInput() ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'ChargeInitial', [
                'template' => Template::getTextInputAddonTemplate('glyphicon-gbp', true)
            ])->textInput() ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'CostRecurring', [
                'template' => Template::getTextInputAddonTemplate('glyphicon-gbp', true)
            ])->textInput() ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'ChargeRecurring', [
                'template' => Template::getTextInputAddonTemplate('glyphicon-gbp', true)
            ])->textInput() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-3">
            <?= $form->field($model, 'BillingFrequency')->dropDownList(Product::billingFrequencyOptions(true), ['prompt' => 'Select Billing Frequency']) ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'TaxRateID')->dropDownList(Product::taxRateOptions(), ['prompt' => 'Select Tax Rate']) ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'supplier_id')->dropDownList($supplierOptions, ['prompt' => 'Select Supplier']) ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'SupplierAccountID')->textInput(['readonly' => 'readonly', 'disabled' => 'disabled']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-3">
            <?= $form->field($model, 'ChargeProductID')->textInput(['maxlength' => true, 'readonly' => 'readonly', 'disabled' => 'disabled']) ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'CompanyID')->textInput(['maxlength' => true, 'readonly' => 'readonly', 'disabled' => 'disabled']) ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'CurrencyID')->textInput(['maxlength' => true, 'readonly' => 'readonly', 'disabled' => 'disabled']) ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'CountryID')->textInput(['maxlength' => true, 'readonly' => 'readonly', 'disabled' => 'disabled']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-3">
            <?= $form->field($model, 'created_date')->textInput(['readonly' => 'readonly', 'disabled' => 'disabled']) ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'modified_date')->textInput(['readonly' => 'readonly', 'disabled' => 'disabled']) ?>
        </div>
        <div class="col-sm-3">
            <label>Is Product Active?</label>
            <?= $form->field($model, 'active')->checkbox(['label' => 'Yes']) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

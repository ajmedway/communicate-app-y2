<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\product\models\ProductSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?php // echo $form->field($model, 'supplier_id') ?>

    <?= $form->field($model, 'ChargeProductID') ?>

    <?= $form->field($model, 'ProductID') ?>
    
    <?= $form->field($model, 'active') ?>

    <?= $form->field($model, 'created_date') ?>

    <?= $form->field($model, 'modified_date') ?>

    <?php // echo $form->field($model, 'CompanyID') ?>

    <?php // echo $form->field($model, 'Description') ?>

    <?php // echo $form->field($model, 'BillingFrequency') ?>

    <?php // echo $form->field($model, 'CostInitial') ?>

    <?php // echo $form->field($model, 'CostRecurring') ?>

    <?php // echo $form->field($model, 'ChargeInitial') ?>

    <?php // echo $form->field($model, 'ChargeRecurring') ?>

    <?php // echo $form->field($model, 'SupplierAccountID') ?>

    <?php // echo $form->field($model, 'CurrencyID') ?>

    <?php // echo $form->field($model, 'CountryID') ?>

    <?php // echo $form->field($model, 'TaxRateID') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

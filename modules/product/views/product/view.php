<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\product\models\Product */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (!empty($model->CompanyID)): ?>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?php endif; ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'supplier_id',
            'ChargeProductID',
            'CompanyID',
            'Description',
            'BillingFrequency',
            'CostInitial',
            'CostRecurring',
            'ChargeInitial',
            'ChargeRecurring',
            [
                'attribute' => 'SupplierAccountID',
                'label' => 'Supplier Name',
                'value' => function ($model) {
                        return !empty($model->supplier) ? $model->supplier->Name : '';
                },
            ],
            'ProductID',
            'CurrencyID',
            'CountryID',
            [
                'attribute' => 'TaxRateID',
                'label' => 'Tax Rate',
                'value' => function ($model) {
                        return $model->getTaxRateName();
                },
            ],
            'active:boolean',
            'created_date',
            'modified_date',
        ],
    ]) ?>

</div>

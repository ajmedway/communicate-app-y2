<?php

namespace app\modules\product\components\helpers;

use app\modules\product\models\ProductCategoryDiscount;

/**
 * @author Alec Pritchard <alec@r-evolution.co.uk>
 * 
 * Class ProductCategoryDiscountData
 * 
 * Statically-stored category-level product discounts data.
 * 
 * NOTE: This might need to be moved out to a proper CRUD/ActiveRecord model if
 * the client wishes to take control of these discounts.
 */
class ProductCategoryDiscountData
{

    /**
     * Returns configuration array of category-level discounts.
     * 
     * CLIENT-PROVIDED DISCOUNT SPECIFICATIONS:
     * - 7% discount on telephones and bandwidth only for 2 year contracts.
     * - 12% discount on telephones and bandwidth only for 3 year contracts.
     * 
     * @return array
     */
    public static function productCategoryDiscounts()
    {
        return [
            ProductCategoryDiscount::$TYPES['FIXED_SUM'] => [
                // no fixed-sum discounts
            ],
            ProductCategoryDiscount::$TYPES['PERCENTAGE'] => [
                [
                    'ProductID' => 'Broadband',
                    'min_quantity' => 0,
                    'min_duration_months' => 24,
                    'amount' => 7,
                ],
                [
                    'ProductID' => 'Broadband',
                    'min_quantity' => 0,
                    'min_duration_months' => 36,
                    'amount' => 12,
                ],
            ],
        ];
    }
    
    /**
     * Create/return product category discount objects.
     * 
     * @return array An array of arrays indexed by ProductID, containing
     * app\modules\product\models\ProductCategoryDiscount instances.
     */
    public static function createDiscounts()
    {
        $ProductCategoryDiscounts = [];

        $discounts = self::productCategoryDiscounts();

        foreach ($discounts as $type => $typeDiscounts) {
            foreach ($typeDiscounts as $d) {
                $discount = new ProductCategoryDiscount();
                $discount->attributes = $d;
                $discount->type = $type;
                $ProductCategoryDiscounts[$discount->ProductID][] = $discount;
            }
        }

        return $ProductCategoryDiscounts;
    }

}

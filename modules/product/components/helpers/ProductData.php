<?php

namespace app\modules\product\components\helpers;

use app\modules\product\models\Product;
use app\modules\product\models\ProductCategory;
use app\modules\product\components\helpers\ProductCategoryDiscountData;

/**
 * @author Alec Pritchard <alec@r-evolution.co.uk>
 * 
 * Class ProductData
 * 
 * Toolbox of static helper methods for parsing/managing product-related data.
 */
class ProductData
{

    public static $FIELD_QTY = 'qty';
    public static $FIELD_DUR = 'duration';
    public static $FIELD_STR = 'start';
    public static $FIELD_OCI = 'charge_initial';
    public static $FIELD_OCR = 'charge_recurring';

    /**
     * Get the form name attribute for a given product and var
     * 
     * @param app\modules\product\models\Product $product
     * @return string
     */
    public static function getProductFormName(\app\modules\product\models\Product $product, $varName)
    {
        if (!empty($product) && !empty($varName)) {
            return "products[{$product->id}][{$varName}]";
        }

        return false;
    }

    /**
     * Get the form name attribute for a given product override and var
     * 
     * @param app\modules\product\models\Product $product
     * @return string
     */
    public static function getOverrideFormName(\app\modules\product\models\Product $product, $varName)
    {
        if (!empty($product) && !empty($varName)) {
            return "productDataOverrides[{$product->id}][{$varName}]";
        }

        return false;
    }

    /**
     * Check if a post products array contains at least 1 item with qty > 0
     * 
     * @param array $postProducts
     * @return bool
     */
    public static function checkPostProductsQty($postProducts)
    {
        if (is_array($postProducts)) {
            foreach ($postProducts as $key => $postProduct) {
                if (!empty($postProduct[self::$FIELD_QTY])) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Check if a post products array contains at least 1 item with qty > 0
     * that also has a start date before today
     * 
     * @param array $postProducts
     * @return bool
     */
    public static function checkPostProductsDates($postProducts)
    {
        $today = new \DateTime('today');
        
        if (is_array($postProducts)) {
            foreach ($postProducts as $key => $postProduct) {
                if (!empty($postProduct[self::$FIELD_QTY]) && !empty($postProduct[self::$FIELD_STR])) {
                    $start = new \DateTime($postProduct[self::$FIELD_STR]);
                    if ($start < $today) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    /**
     * Check if a post products array contains at least 1 item with qty > 0
     * 
     * @param array $postProducts
     * @return array
     */
    public static function filterPostProducts($postProducts)
    {
        if (is_array($postProducts)) {
            foreach ($postProducts as $key => $postProduct) {
                if (empty($postProduct[self::$FIELD_QTY])) {
                    unset($postProducts[$key]);
                }
            }
        }

        return !empty($postProducts) ? $postProducts : [];
    }

    /**
     * Check if a post products array contains at least 1 item with overrides
     * 
     * @param array $postProductDataOverrides
     * @return array
     */
    public static function filterPostProductDataOverrides($postProductDataOverrides)
    {
        if (is_array($postProductDataOverrides)) {
            foreach ($postProductDataOverrides as $key => $postProductDataOverride) {
                if (empty($postProductDataOverride[self::$FIELD_OCI]) && empty($postProductDataOverride[self::$FIELD_OCR])) {
                    unset($postProductDataOverrides[$key]);
                }
            }
        }

        return !empty($postProductDataOverrides) ? $postProductDataOverrides : [];
    }

}

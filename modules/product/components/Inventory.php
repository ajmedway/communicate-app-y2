<?php

namespace app\modules\product\components;

use yii\base\BaseObject;
use yii\base\Arrayable;
use yii\base\ArrayableTrait;
use yii\helpers\ArrayHelper;
use app\modules\product\models\Product;
use app\modules\product\models\ProductCategory;
use app\modules\product\components\helpers\ProductCategoryDiscountData;

/**
 * @author Alec Pritchard <alec@r-evolution.co.uk>
 * 
 * Class Inventory
 */
class Inventory extends BaseObject implements Arrayable, \JsonSerializable
{

    use ArrayableTrait;

    /**
     * @var array Inventory data container
     */
    private $_inventory;

    /**
     * @var ProductCategory[] $_productCategories
     */
    private $_productCategories;

    /**
     * @var array Product category discount objects
     */
    private $_productCategoryDiscounts;

    /**
     * @var bool If inventory data should be loaded with model attributes only
     */
    public $attributesOnly;

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // ... initialization after configuration is applied
        if ($this->attributesOnly === null) {
            $this->attributesOnly = false;
        }

        // load required data components
        $this->loadProductCategories();
        $this->loadProductCategoryDiscounts();
        $this->loadInventoryData($this->attributesOnly);
    }

    /**
     * {@inheritdoc}
     * 
     * @see ArrayableTrait::toArray()
     */
    public function fields()
    {
        $fields = [
            'inventory' => '_inventory'
        ];

        return $fields;
    }

    /**
     * {@inheritdoc}
     * 
     * @see ArrayableTrait::toArray()
     */
    public function jsonSerialize()
    {
        return $this->toArray();
    }

    /**
     * Load the inventory data.
     * 
     * @param bool $attributesOnly If true, only get model attributes, not full
     * objects.
     * @return void
     */
    public function loadInventoryData($attributesOnly = false)
    {
        foreach ($this->_productCategories as $productCategory) {
            // create new blank object container for category products data
            $categoryProducts = new \stdClass();

            $products = Product::findAll([
                    'ProductID' => $productCategory->ProductID,
                    'active' => 1
            ]);

            $categoryDiscounts = $this->getCategoryDiscountsForProductCategory($productCategory);

            // inject category products & category-level discounts
            if ($attributesOnly) {
                foreach ($products as $product) {
                    $categoryProducts->products[] = (object) $product->getAttributes();
                }
                foreach ($categoryDiscounts as $categoryDiscount) {
                    $categoryProducts->categoryDiscounts[] = (object) $categoryDiscount->getAttributes();
                }
            } else {
                $categoryProducts->products = $products;
                $categoryProducts->categoryDiscounts = $categoryDiscounts;
            }
            
            // index the products by id
            if (!empty($categoryProducts->products)) {
                $categoryProducts->products = ArrayHelper::index($categoryProducts->products, 'id');
            }

            // add the category products data to the inventory array
            $this->_inventory[$productCategory->ProductGroupID][$productCategory->ProductID] = $categoryProducts;
        }
    }

    /**
     * Get multi-dimensional inventory array of structure:
     * 
     * php```
     * [
     *     'ProductGroupID' => [
     *         'ProductID' => [
     *             'products' => [
     *                 <Product>,
     *                 <Product>,
     *                 ...
     *             ],
     *             'categoryDiscounts' => [
     *                 <ProductCategoryDiscount>,
     *                 <ProductCategoryDiscount>,
     *                 ...
     *             ],
     *         ],
     *     ],
     * ]
     * ```
     * 
     * @param bool $attributesOnly If true, only get model attributes, not full
     * objects.
     * @param bool $refresh If true, refreshes the inventory data.
     * @return array
     */
    public function getInventoryData($attributesOnly = false, $refresh = false)
    {
        if ($this->_inventory === null || $refresh) {
            $this->loadInventoryData($attributesOnly);
        }

        return $this->_inventory;
    }

    /**
     * Load all ProductCategory instances from database.
     * 
     * @return void
     */
    public function loadProductCategories()
    {
        $this->_productCategories = ProductCategory::find()->all();
    }

    /**
     * Get all ProductCategory instances.
     * 
     * @param bool $refresh If true, refreshes the ProductCategories.
     * @return app\modules\product\models\ProductCategory[]|array
     */
    public function getProductCategories($refresh = false)
    {
        if ($this->_productCategories === null || $refresh) {
            $this->loadProductCategories();
        }

        return $this->_productCategories;
    }

    /**
     * Load all ProductCategoryDiscount instances from
     * ProductCategoryDiscountData helper.
     * 
     * @return void
     */
    public function loadProductCategoryDiscounts()
    {
        $this->_productCategoryDiscounts = ProductCategoryDiscountData::createDiscounts();
    }

    /**
     * Get all ProductCategory instances.
     * 
     * @param bool $refresh If true, refreshes the ProductCategoryDiscounts.
     * @return array An array of arrays indexed by ProductID, containing
     * app\modules\product\models\ProductCategoryDiscount instances.
     */
    public function getProductCategoryDiscounts($refresh = false)
    {
        if ($this->_productCategoryDiscounts === null || $refresh) {
            $this->loadProductCategoryDiscounts();
        }

        return $this->_productCategoryDiscounts;
    }

    /**
     * Get applicable category-level discounts for a ProductCategory instance.
     * 
     * @param app\modules\product\models\ProductCategory $productCategory
     * @return array $categoryDiscounts
     */
    public function getCategoryDiscountsForProductCategory(\app\modules\product\models\ProductCategory $productCategory)
    {
        // Check if product category has any category discounts - get array of
        // objects if so, injecting into categoryDiscounts property
        if (array_key_exists($productCategory->ProductID, $this->_productCategoryDiscounts)) {
            $categoryDiscounts = $this->_productCategoryDiscounts[$productCategory->ProductID];
        } else {
            $categoryDiscounts = [];
        }

        return $categoryDiscounts;
    }

    /**
     * Get all discounts for a Product instance.
     * 
     * @param app\modules\product\models\Product $product
     * @return array $discounts
     */
    public function getAllDiscountsForProduct(\app\modules\product\models\Product $product)
    {
        $discounts = new \stdClass();
        $discounts->categoryDiscounts = [];

        if (!empty($product->productCategory) && !empty($product->ProductID)) {
            $productGroupID = $product->productCategory->ProductGroupID;
            
            // Check if product category has any category discounts - get array of
            // objects if so, injecting into categoryDiscounts property
            if (array_key_exists($productGroupID, $this->_inventory) &&
                array_key_exists($product->ProductID, $this->_inventory[$productGroupID])) {
                $discounts->categoryDiscounts = $this->_inventory[$productGroupID][$product->ProductID]->categoryDiscounts;
            }
        }

        return $discounts;
    }

}

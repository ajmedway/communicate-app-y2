<?php

namespace app\modules\quote\controllers;

use Yii;
use app\modules\product\models\Product;
use app\modules\product\models\ProductCategory;
use app\modules\product\components\Inventory;
use app\modules\product\components\helpers\ProductData as PD;
use app\modules\quote\models\Quote;
use app\modules\quote\models\QuoteSearch;
use app\modules\quote\models\DirectDebitDetails;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\AjaxFilter;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use Da\User\Filter\AccessRuleFilter;
use thamtech\uuid\helpers\UuidHelper;

/**
 * Copyright © 2018 r//evolution Marketing. All rights reserved.
 * 
 * @author Alec Pritchard <alec@r-evolution.co.uk>
 * 
 * QuoteController implements the CRUD actions for Quote model.
 */
class QuoteController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'ruleConfig' => [
                    'class' => AccessRuleFilter::class,
                ],
                'rules' => [
                    [
                        // allow users to access the quote builder
                        'actions' => ['manage', 'checkout', 'process-charges', 'get-totals'],
                        'allow' => true,
                        'roles' => ['user'],
                        'matchCallback' => function ($rule, $action) {
                            $user = Yii::$app->user->getIdentity();
                            // check user has valid profile & company, or deny access
                            return ($user->profile->validate() && !empty($user->profile->company) && $user->profile->company->validate());
                        }
                    ],
                    [
                        // allow users to access the quote builder
                        'actions' => ['manage'],
                        'allow' => false,
                        'roles' => ['admin'],
                    ],
                    [
                        // disable access to quote builder CRUD actions
                        'actions' => ['create', 'update', 'delete'],
                        'allow' => false,
                    ],
                    [
                        // open all other quote controller actions to admins only
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
                'denyCallback' => function($rule, $action) {
                    $user = Yii::$app->user->getIdentity();

                    // custom deny callbacks...
                    if ($action->id === 'manage' && Yii::$app->user->can('admin')) {
                        // set flash and redirect to admin version of the page
                        Yii::$app->session->setFlash('info', 'Redirected from customer quote manager page');
                        $redirectUrl = Url::toRoute(array_merge(['quote/manage-admin'], Yii::$app->request->getQueryParams()));
                        return $action->controller->redirect($redirectUrl);
                    } else if ($action->id === 'manage') {
                        // redirect to edit profile is not validating with required data
                        if (!$user->profile->validate()) {
                            Yii::$app->session->setFlash('warning', "<strong>Please Note!</strong> You cannot create a quote until you have completed your profile & company details. We have redirected you to your profile page to enter your details in full.</strong>");
                            return $action->controller->redirect(['/user/settings/profile']);
                        }
                        // redirect to edit company is not validating with required dat
                        if (empty($user->profile->company) || !$user->profile->company->validate()) {
                            Yii::$app->session->setFlash('warning', "<strong>Please Note!</strong> You cannot create a quote until you have completed your profile & company details. We have redirected you to your company page to enter your details in full.</strong>");
                            return $action->controller->redirect(['/company/manage']);
                        }
                    }

                    // use code from AccessControl::denyAccess() as a default behavior
                    if (Yii::$app->user !== false && Yii::$app->user->getIsGuest()) {
                        Yii::$app->user->loginRequired();
                    } else {
                        throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
                    }
                },
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                    'get-totals' => ['POST'],
                ],
            ],
            [
                'class' => AjaxFilter::class,
                'only' => ['get-totals'],
            ],
        ];
    }

    /**
     * Lists all Quote models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new QuoteSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Quote model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
                'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Quote model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Quote();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
                'model' => $model,
        ]);
    }

    /**
     * Updates an existing Quote model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
                'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Quote model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Renders the customer quote manager
     * @param string $uuid
     * @return mixed
     */
    public function actionManage($uuid = '')
    {
        $request = Yii::$app->request;
        $postProducts = $request->post('products');
        $postUuid = $request->post('existing-uuid');

        $user = Yii::$app->user->getIdentity();
        $userId = $user->getId();

        // load inventory component instance
        $inventory = Yii::createObject([
                'class' => Inventory::class,
                'attributesOnly' => false,
        ]);

        // check if the user already has any live quotes
        $existingQuotes = Quote::find()->where(['user_id' => $userId])->active()->all();

        // if there is no specific quote requested and there are any active quotes for this user, list them
        if (empty($uuid) && count($existingQuotes) > 0) {
            return $this->render('manage-listing', [
                    'quotes' => $existingQuotes,
            ]);
        }

        // look up existing user quote from url get variable, if populated
        if (($quoteExisting = Quote::findOne(['user_id' => $userId, 'uuid' => $uuid])) === null && !empty($uuid)) {
            throw new NotFoundHttpException('The requested page does not exist. Invalid Quote ID.');
        } else if (!empty($quoteExisting) && in_array($quoteExisting->status, Quote::getLiveStatuses())) {
            // attempting to load a live quote into the form... let's check if there is a pending quote first
            if (($quoteExistingPending = Quote::find()->where(['user_id' => $userId])->pending()->one()) !== null) {
                // we cannot allow loading/edit of a live quote while there is a pending quote in the pipeline
                // set message and redirect to the new/updated quote url
                Yii::$app->session->setFlash('warning', "<strong>Please Note!</strong> You cannot directly edit a live quote while there is a pending quote in the pipeline. We have redirected you to your pending quote with reference: <strong>{$quoteExistingPending->getUniqueReference()}</strong>");
                return $this->redirect(['/quote/manage', 'uuid' => $quoteExistingPending->uuid]);
            }
        }

        if (!empty($postProducts)) {
            if (($postProducts = PD::filterPostProducts($postProducts)) === []) {
                Yii::$app->session->setFlash('warning', '<strong>No products were selected.</strong> Please add at least 1 product with a quantity above zero before submitting.');
                return $this->refresh();
            } else if (($quote = Quote::findOne(['user_id' => $userId, 'uuid' => $postUuid])) !== null && !in_array($quote->status, Quote::getLiveStatuses())) {
                // we have received a valid post that contains an existing uuid that could be loaded and matched to the logged in user
                // also, this is not an edit of a live quote, in which case we create a new working copy in the `else` block below
                $quoteDataChanged = !($quote->quote_data === $postProducts);
                $quote->quote_data = $postProducts;
                if ($quote->status === Quote::$STATUSES['PENDING_CUSTOMER_APPROVAL'] && !$quoteDataChanged && PD::checkPostProductsDates($quote->quote_data)) {
                    $quote->status = Quote::$STATUSES['APPROVED_PENDING_CHARGES'];
                    $quote->notes = Quote::$STANDARD_NOTES['APPROVED_BY_CUSTOMER'];
                    $flashMsg = '<strong>Great!</strong> You have just approved your quote!';
                } else if ($quoteDataChanged) {
                    // customer has changed things again... back round the loop for sales to approve
                    $quote->status = Quote::$STATUSES['PENDING_SALES_APPROVAL'];
                    $quote->notes = Quote::$STANDARD_NOTES['UPDATED_BY_CUSTOMER'];
                    $flashMsg = '<strong>Well done!</strong> You have just updated your quote. The sales team will review your submission soon and confirm if everything is ok.';
                }
                $quote->save();
            } else {
                // customise the notes based on if creating from editing a live quote, or just creating a new one from scratch
                $notes = !empty($quote) && in_array($quote->status, Quote::getLiveStatuses()) ?
                    Quote::$STANDARD_NOTES['CREATED_FROM_EXISTING'] :
                    Quote::$STANDARD_NOTES['NEW'];

                // create new quote object from submission and persist to db
                $quote = new Quote();
                $quote->quote_data = $postProducts;
                $quote->status = Quote::$STATUSES['PENDING_SALES_APPROVAL'];
                $quote->notes = $notes;
                $quote->link('profile', $user->profile);

                // customise the flash message based on if created from editing a live quote, or just created a new one from scratch
                $flashMsg = !empty($quote) && in_array($quote->status, Quote::getLiveStatuses()) ?
                    '<strong>Congratulations!</strong> You have just created a new quote from editing your existing one. The sales team will review your submission soon and confirm if everything is ok.' :
                    '<strong>Congratulations!</strong> You have just created your new quote. The sales team will review your submission soon and confirm if everything is ok.';
            }

            // set message and redirect to the new/updated quote url
            $quote->refresh();
            if (!PD::checkPostProductsDates($quote->quote_data)) {
                Yii::$app->session->setFlash('warning', '<strong>Product start dates are invalid!</strong> Please ensure all product start dates are set to a present or future date.');
            } else if (!empty($flashMsg)) {
                Yii::$app->session->setFlash('success', $flashMsg);
            }
            return $this->redirect(['/quote/manage', 'uuid' => $quote->uuid]);
        }

        // if the quote requires approval from customer, issue instructional info message
        if (!empty($quoteExisting) && $quoteExisting->status === Quote::$STATUSES['PENDING_CUSTOMER_APPROVAL']) {
            Yii::$app->session->setFlash('info', '<strong>You need to approve your quote!</strong> Please ensure you are happy with everything, then click the <strong>&quotSave &amp; Approve Quote&quot</strong> button at the bottom of the form to proceed.');
        }

        // if the quote products data is invalid and this is a pending quote, issue warning message
        if (!empty($quoteExisting) && in_array($quoteExisting->status, Quote::getPendingStatuses()) && !PD::checkPostProductsDates($quoteExisting->quote_data)) {
            Yii::$app->session->setFlash('warning', '<strong>Product start dates are invalid!</strong> Please ensure all product start dates are set to a present or future date.');
        }

        return $this->render('manage', [
                'totalsBar' => $this->renderPartial('_quote-totals', [
                    'totals' => $quoteExisting->calculateTotals(),
                ]),
                'inventory' => $inventory,
                'quote' => $quoteExisting,
                'canSave' => true,
        ]);
    }

    /**
     * Renders the customer quote manager for admins only
     * @param string $uuid
     * @return mixed
     */
    public function actionManageAdmin($uuid = '')
    {
        $request = Yii::$app->request;
        $postProducts = $request->post('products');
        $postProductDataOverrides = $request->post('productDataOverrides');
        $postUuid = $request->post('existing-uuid');

        // load inventory component instance
        $inventory = Yii::createObject([
                'class' => Inventory::class,
                'attributesOnly' => false,
        ]);

        // check if the user already has any pending quotes
        $pendingQuotes = Quote::find()->pendingSales()->all();

        // if there is no specific quote requested and there are any pending quotes for sales to approve, list them
        if (empty($uuid)) {
            return $this->render('manage-listing-admin', [
                    'quotes' => $pendingQuotes,
            ]);
        }

        // look up existing user quote from url get variable, if populated
        if (($quoteExisting = Quote::findOne(['uuid' => $uuid])) === null && !empty($uuid)) {
            throw new NotFoundHttpException('The requested page does not exist. Invalid Quote ID.');
        }

        if (!empty($postProducts)) {
            if (($postProducts = PD::filterPostProducts($postProducts)) === []) {
                Yii::$app->session->setFlash('warning', '<strong>No products were selected.</strong> Please add at least 1 product with a quantity above zero before submitting.');
                return $this->refresh();
            } else if (($quote = Quote::findOne(['uuid' => $postUuid])) !== null && Yii::$app->user->can('admin')) {
                // admin only - we have a valid post that contains an existing uuid that could be loaded
                $quote->quote_data = $postProducts;
                $quote->product_data_overrides = PD::filterPostProductDataOverrides($postProductDataOverrides);
                $quote->status = Quote::$STATUSES['PENDING_CUSTOMER_APPROVAL'];
                $quote->notes = Quote::$STANDARD_NOTES['APPROVED_BY_SALES'];
                $quote->save();

                // send the sales approved email
                $quote->salesApprovedEmail();

                $flashMsg = '<strong>Great!</strong> The quote has been updated successfully and approved, ready for the customer to approve.';
            }

            // set message and redirect to the new/updated quote url
            Yii::$app->session->setFlash('success', $flashMsg);
            return $this->redirect(['/quote/manage-admin', 'uuid' => $quote->uuid]);
        }

        // if the quote products data is invalid, issue warning message
        if (!empty($quoteExisting) && !PD::checkPostProductsDates($quoteExisting->quote_data)) {
            Yii::$app->session->setFlash('warning', '<strong>Product start dates are invalid!</strong> Please ensure all product start dates are set to a present or future date.');
        }

        return $this->render('manage', [
                'totalsBar' => $this->renderPartial('_quote-totals', [
                    'totals' => $quoteExisting->calculateTotals(),
                ]),
                'inventory' => $inventory,
                'quote' => $quoteExisting,
                'canSave' => !in_array($quoteExisting->status, Quote::getLiveStatuses()),
        ]);
    }

    /**
     * Ajax action for getting rendered quote totals bar partial.
     * @return mixed
     */
    public function actionGetTotals()
    {
        $request = Yii::$app->request;
        $postUuid = $request->post('existing-uuid');

        if (($quote = Quote::findOne(['uuid' => $postUuid])) === null) {
            $quote = new Quote();
        }

        if (($postProducts = PD::filterPostProducts($request->post('products'))) !== []) {
            $quote->quote_data = $postProducts;
        }

        $totals = $quote->calculateTotals();

        return $this->renderAjax('_quote-totals', [
                'totals' => $totals,
        ]);
    }

    /**
     * Renders the Direct Debit form to finalise and submit to Anvil.
     * @param string $uuid
     * @return mixed
     */
    public function actionCheckout($uuid = '')
    {
        $userId = Yii::$app->user->getId();

        // look up existing user quote from url get variable, if populated
        if (($quote = Quote::findOne(['user_id' => $userId, 'uuid' => $uuid])) === null && $quote->status !== Quote::$STATUSES['APPROVED_PENDING_CHARGES']) {
            throw new NotFoundHttpException('The requested page does not exist. Invalid Quote ID.');
        }

        $ddDetails = new DirectDebitDetails();
        $ddDetailsReference = "COMNCT {$quote->getUniqueReference()}";
        if (($ddDetailsExisting = $quote->decryptDirectDebitDetails()) !== false) {
            // decrypt and load existing details for this quote, if already submitted
            $ddDetails->load($ddDetailsExisting, '');
        } else if (($quoteExistingLive = Quote::find()->where(['user_id' => $userId])->live()->one()) !== null && ($ddDetailsExistingLive = $quoteExistingLive->decryptDirectDebitDetails()) !== false) {
            // extract the direct debit details from their previous live quote
            $ddDetails->load($ddDetailsExistingLive, '');
            // overwrite reference with new one
            $ddDetails->reference = $ddDetailsReference;
        } else {
            // set reference
            $ddDetails->reference = $ddDetailsReference;
        }

        if ($ddDetails->load(Yii::$app->request->post())) {
            if ($ddDetails->validate()) {
                $quote->direct_debit_details = Yii::$app->request->post('DirectDebitDetails');
                $quote->encryptDirectDebitDetails();
                $quote->notes = Quote::$STANDARD_NOTES['PENDING_CHARGES'];
                $quote->save();

                Yii::$app->session->setFlash('success', '<strong>Excellent!</strong> Direct Debit details submitted successfully. Please check/correct the details below then click Submit to finalise.');

                return $this->refresh();
            }
        }

        return $this->render('checkout', [
                'quote' => $quote,
                'ddDetails' => $ddDetails,
        ]);
    }

    /**
     * Processes new charges to Anvil
     * @todo handle updates to existing quotes, supersede old quotes, update quote to live status once successfully processed charges... details TBC
     * @todo cancel the charges in anvil for this old superseded quote... details TBC
     * @todo process-charges action template to be written
     * @param string $uuid
     * @return mixed
     */
    public function actionProcessCharges($uuid = '')
    {
        $userId = Yii::$app->user->getId();

        // look up existing quote from url get variable
        if (($quote = Quote::findOne(['user_id' => $userId, 'uuid' => $uuid, 'status' => Quote::$STATUSES['APPROVED_PENDING_CHARGES']])) === null || empty($uuid)) {
            throw new NotFoundHttpException('The requested page does not exist. Invalid Quote ID.');
        }

        // if the quote products data is invalid, send them back to quote manager
        if (!PD::checkPostProductsQty($quote->quote_data)) {
            Yii::$app->session->setFlash('warning', '<strong>No products were selected.</strong> Please add at least 1 product with a quantity above zero before submitting.');
            return $this->redirect(['quote/manage', 'uuid' => $uuid]);
        } else if (!PD::checkPostProductsDates($quote->quote_data)) {
            Yii::$app->session->setFlash('warning', '<strong>Product start dates are invalid!</strong> Please ensure all product start dates are set to a present or future date.');
            return $this->redirect(['quote/manage', 'uuid' => $uuid]);
        }

        // if the dd details don't validate, send them back to checkout
        $ddDetails = new DirectDebitDetails();
        if (($ddDetailsExisting = $quote->decryptDirectDebitDetails()) !== false) {
            $ddDetails->load($ddDetailsExisting, '');
        }
        if (!$ddDetails->validate()) {
            Yii::$app->session->setFlash('warning', '<strong>Invalid Direct Debit Details!</strong> Please submit your Direct Debit details in full.');
            return $this->redirect(['quote/checkout', 'uuid' => $uuid]);
        }

        // get AnvilDataManager singleton
        $anvil = Yii::$app->anvilDataManager;

        if (($profile = $anvil->createUpdateAnvilCustomerRecords($quote->profile)) === false) {
            Yii::error(['message' => "Error QPC001. Unable to create/update Customer records in Anvil for quote #{$quote->id}", '$quote' => $quote], __METHOD__);
            Yii::$app->session->setFlash('error', '<strong>Error Q-PC-001.</strong> There has been a problem processing your details! Please contact a member of our team via ' . Html::mailto(Yii::$app->params['salesEmail'], Yii::$app->params['salesEmail']) . ' for further assistance.');
            return $this->redirect(['quote/checkout', 'uuid' => $uuid]);
        }

        if (($Charges = $anvil->generateCharges($quote)) === []) {
            Yii::error(['message' => "Error QPC002. Unable to create any Product-based Charges in Anvil for quote #{$quote->id}", '$quote' => $quote], __METHOD__);
            Yii::$app->session->setFlash('error', '<strong>Error Q-PC-002.</strong> There has been a problem processing your details! Please contact a member of our team via ' . Html::mailto(Yii::$app->params['salesEmail'], Yii::$app->params['salesEmail']) . ' for further assistance.');
            return $this->redirect(['quote/checkout', 'uuid' => $uuid]);
        }

        // successfully processed charges! check if the customer had an old existing live quote
        if (($oldLiveQuote = Quote::findOne(['user_id' => $userId, 'status' => Quote::$STATUSES['LIVE_CHARGES_CREATED']])) !== null) {
            // supersede the old live quote if one exists
            $oldLiveQuote->status = Quote::$STATUSES['OLD_SUPERSEDED'];
            $oldLiveQuote->save(false);

            // cancel the charges in anvil for this old quote
        }

        // serialise & store $Charges for cache/display purposes
        $quote->status = Quote::$STATUSES['LIVE_CHARGES_CREATED'];
        $quote->notes = Quote::$STANDARD_NOTES['CHARGES_CREATED'];
        $quote->live_charges_data = $Charges;
        $quote->save(false);
        $quote->refresh();
        
        // send confirmation email
        $quote->orderCompleteEmail();

        Yii::$app->session->setFlash('success', "Order successfully created for quote with reference: <strong>{$quote->getUniqueReference()}</strong>");

        return $this->render('process-charges', [
                'quote' => $quote,
        ]);
    }

    /**
     * Renders a pre-populated quote based on GET parameters. This endpoint was
     * initially created for the Communicate VR app produced by Spearhead
     * Interactive.
     * 
     * Notes: It was proposed that the incoming url had a query string structure
     * like this:
     * 
     * https://my.communicateplc.com/quote/create-my-quote
     * ?products[0][id]=prodId1&products[0][qty]=2
     * &products[1][id]=prodId2&products[1][qty]=9
     * 
     * To be parsed into an array like this:
     * 
     * products => [
     *     0 => [
     *        'id'  => 'prodId1',
     *        'qty' => 2,
     *     ]
     *     1 => [
     *        'id'  => 'prodId2',
     *        'qty' => 9,
     *     ]
     * ]
     * 
     * @todo Complete implementation. Full details TBC.
     * @return mixed
     */
    public function actionCreateMyQuote()
    {
        $request = Yii::$app->request;
        $get = $request->get();

        echo '<pre>';
        echo '$get: ' . print_r($get, true);
        echo '</pre>';
        exit;

        return $this->render('index', [
        ]);
    }

    /**
     * Finds the Quote model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Quote the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Quote::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}

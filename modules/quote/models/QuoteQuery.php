<?php

namespace app\modules\quote\models;

use app\modules\quote\models\Quote;

/**
 * This is the ActiveQuery class for [[Quote]].
 *
 * @see Quote
 */
class QuoteQuery extends \yii\db\ActiveQuery
{

    /**
     * @return \yii\db\ActiveQuery
     */
    public function live()
    {
        return $this->andWhere([
                'in',
                'status',
                Quote::getLiveStatuses(),
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function notLive()
    {
        return $this->andWhere([
                'not in',
                'status',
                Quote::getLiveStatuses(),
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function pendingSales()
    {
        return $this->andWhere([
            'status' => Quote::$STATUSES['PENDING_SALES_APPROVAL']
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function pending()
    {
        return $this->andWhere([
                'in',
                'status',
                Quote::getPendingStatuses(),
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function notPending()
    {
        return $this->andWhere([
                'not in',
                'status',
                Quote::getPendingStatuses(),
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function active()
    {
        return $this->andWhere([
                'not in',
                'status',
                Quote::getDeadStatuses(),
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function inactive()
    {
        return $this->andWhere([
                'in',
                'status',
                Quote::getDeadStatuses(),
        ]);
    }

    /**
     * {@inheritdoc}
     * @return Quote[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Quote|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

}

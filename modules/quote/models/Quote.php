<?php

namespace app\modules\quote\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;
use yii\helpers\Url;
use app\models\Profile;
use app\modules\product\models\Product;
use app\modules\product\models\ProductCategoryDiscount;
use app\modules\product\components\helpers\ProductData as PD;
use app\modules\product\components\Inventory;
use thamtech\uuid\helpers\UuidHelper;
use thamtech\uuid\helpers\UuidValidator;

/**
 * This is the model class for table "quote".
 *
 * @property int $id
 * @property string $uuid
 * @property int $user_id
 * @property string $created_date
 * @property string $modified_date
 * @property resource $quote_data
 * @property resource $product_data_overrides
 * @property resource $direct_debit_details
 * @property resource $live_charges_data
 * @property string $notes
 * @property string $status
 *
 * @property Profile $profile
 */
class Quote extends \yii\db\ActiveRecord
{

    /**
     * @var Quote object statuses
     */
    public static $STATUSES = [
        'DRAFT' => 'DRAFT',
        'PENDING_SALES_APPROVAL' => 'PENDING_SALES_APPROVAL',
        'PENDING_CUSTOMER_APPROVAL' => 'PENDING_CUSTOMER_APPROVAL',
        'APPROVED_PENDING_CHARGES' => 'APPROVED_PENDING_CHARGES',
        'LIVE_CHARGES_CREATED' => 'LIVE_CHARGES_CREATED',
        'OLD_SUPERSEDED' => 'OLD_SUPERSEDED',
        'CANCELLED' => 'CANCELLED',
    ];

    /**
     * @var Standardised notes assigned to quotes as their states are changed
     */
    public static $STANDARD_NOTES = [
        'NEW' => 'Newly created quote via quote builder.',
        'CREATED_FROM_EXISTING' => 'Created from editing existing live quote via quote builder.',
        'APPROVED_BY_CUSTOMER' => 'Customer approved quote via quote builder.',
        'APPROVED_BY_SALES' => 'Sales team approved quote via quote builder.',
        'UPDATED_BY_CUSTOMER' => 'Customer updated quote via quote builder.',
        'PENDING_CHARGES' => 'Direct Debit details entered. Pending charges to be applied in Anvil billing system.',
        'CHARGES_CREATED' => 'Charges applied in Anvil billing system.',
    ];

    /**
     * @var app\modules\product\components\Inventory Inventory instance.
     */
    private $_inventory;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'quote';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['created_date', 'modified_date'], 'safe'],
            [['uuid', 'direct_debit_details', 'notes'], 'string'],
            [['status'], 'string', 'max' => 255],
            [['uuid'], 'thamtech\uuid\validators\UuidValidator'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Profile::className(), 'targetAttribute' => ['user_id' => 'user_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uuid' => 'UUID',
            'user_id' => 'User ID',
            'created_date' => 'Created Date',
            'modified_date' => 'Modified Date',
            'quote_data' => 'Quote Data',
            'product_data_overrides' => 'Product Data Overrides',
            'direct_debit_details' => 'Direct Debit Details',
            'live_charges_data' => 'Live Charges Details',
            'notes' => 'Notes',
            'status' => 'Status',
        ];
    }

    /**
     * Get bootstrap label html for a quote status
     * @return string
     */
    public static function getStatusLabelHtml($status)
    {
        if (!in_array($status, self::$STATUSES)) {
            return '';
        }

        switch ($status) {
            case self::$STATUSES['PENDING_SALES_APPROVAL']:
                $type = 'warning';
                break;
            case self::$STATUSES['PENDING_CUSTOMER_APPROVAL']:
                $type = 'info';
                break;
            case self::$STATUSES['APPROVED_PENDING_CHARGES']:
                $type = 'primary';
                break;
            case self::$STATUSES['LIVE_CHARGES_CREATED']:
                $type = 'success';
                break;
            case self::$STATUSES['OLD_SUPERSEDED']:
            case self::$STATUSES['CANCELLED']:
                $type = 'danger';
                break;
            default:
                $type = 'default';
                break;
        }

        return '<span class="label label-' . $type . '">' . Inflector::humanize($status, true) . '</span>';
    }

    /**
     * Get array of active/live quote statuses
     * @return array
     */
    public static function getLiveStatuses()
    {
        return [
            self::$STATUSES['LIVE_CHARGES_CREATED'],
        ];
    }

    /**
     * Get array of active/pending completion quote statuses
     * @return array
     */
    public static function getPendingStatuses()
    {
        return [
            self::$STATUSES['PENDING_SALES_APPROVAL'],
            self::$STATUSES['PENDING_CUSTOMER_APPROVAL'],
            self::$STATUSES['APPROVED_PENDING_CHARGES'],
        ];
    }

    /**
     * Get array of dead quote statuses
     * @return array
     */
    public static function getDeadStatuses()
    {
        return [
            self::$STATUSES['OLD_SUPERSEDED'],
            self::$STATUSES['CANCELLED'],
        ];
    }

    /**
     * Basic implementation for getting first 8 chars of uuid (for display purposes only)
     * @return string
     */
    public function getUniqueReference()
    {
        $parts = explode('-', $this->uuid);
        return $parts[0];
    }
    
    /**
     * Get the SignNow contract reference.
     * 
     * Client specs:
     * First 3 letters of company (e.g. David & Sons Ltd – Read DSL) followed by
     * start date, so DSL-15-08-2018 for example.
     * 
     * @return string
     */
    public function getSignNowContractReference()
    {
        $_getCompanyAcronym = function($maxLength, $padCharacter) {
            $expr = '/(?<=\s|^)[a-z]/i';
            preg_match_all($expr, $this->profile->company->hs_name, $matches);
            $companyAcronym3Chars = str_pad(mb_substr(strtoupper(implode('', $matches[0])), 0, $maxLength), $maxLength, $padCharacter);
            return $companyAcronym3Chars;
        };
        $_getStartDate = function($format) {
            $startDate = $this->getServicesStartDate(false, $format);
            if (!empty($startDate)) {
                return $startDate;
            }
            return '00-00-0000';
        };
        
        return $_getCompanyAcronym(3, 'X') . '-' . $_getStartDate('d-m-Y');
    }

    /**
     * Get the quote services start date.
     * 
     * @param bool $getDateTimeObject If true a full DateTime object is returned
     * @param string $format PHP date format
     * @return DateTime|string|false A full DateTime object or a formatted date
     * string. False on failure.
     */
    public function getServicesStartDate($getDateTimeObject = false, $format = 'Y-m-d')
    {
        $earliestStartDate = null;
        foreach ($this->quote_data as $productLine) {
            $dt = new \DateTime($productLine[PD::$FIELD_STR]);
            if (empty($earliestStartDate) || $dt < $earliestStartDate) {
                $earliestStartDate = $dt;
            }
        }

        return ($earliestStartDate instanceof \DateTime) ?
            ($getDateTimeObject ? $earliestStartDate : $earliestStartDate->format($format)) : false;
    }

    /**
     * Get the quote services end date.
     * 
     * @param bool $getDateTimeObject If true a full DateTime object is returned
     * @param string $format PHP date format
     * @return DateTime|string|false A full DateTime object or a formatted date
     * string. False on failure.
     */
    public function getServicesEndDate($getDateTimeObject = false, $format = 'Y-m-d')
    {
        $startDateDt = $this->getServicesStartDate(true);
        $maxServicesDuration = $this->getMaxServicesDuration();
        
        if (!empty($startDateDt) && !empty($maxServicesDuration)) {
            $startDateDt->add(new \DateInterval("P{$maxServicesDuration}M"));
        }

        return ($startDateDt instanceof \DateTime) ?
            ($getDateTimeObject ? $startDateDt : $startDateDt->format($format)) : false;
    }

    /**
     * Get the longest quote service duration (total months).
     * 
     * @return int|false
     */
    public function getMaxServicesDuration()
    {
        $maxServicesDuration = false;
        foreach ($this->quote_data as $productLine) {
            if (empty($maxServicesDuration) || $productLine[PD::$FIELD_DUR] > $maxServicesDuration) {
                $maxServicesDuration = $productLine[PD::$FIELD_DUR];
            }
        }

        return $maxServicesDuration;
    }

    /**
     * Get the public/admin url for this quote
     * @param bool $public If false will create admin quote url. Defaults to
     * true, giving the public url.
     * @return string
     */
    public function getQuoteUrl($public = true)
    {
        $path = $public ? '/quote/manage' : '/quote/manage-admin';
        return Url::toRoute([$path, 'uuid' => $this->uuid], true);
    }

    /**
     * Safely serialize and encrypt the loaded Direct Debit Details
     * @return void
     */
    public function encryptDirectDebitDetails()
    {
        if (!empty($this->direct_debit_details)) {
            $this->direct_debit_details = Yii::$app->getSecurity()->encryptByKey(
                base64_encode(serialize($this->direct_debit_details)), DirectDebitDetails::getEncryptionKey()
            );
        }
    }

    /**
     * Get decrypted Direct Debit Details and unserialize
     * @return string|bool
     */
    public function decryptDirectDebitDetails()
    {
        if (empty($this->direct_debit_details)) {
            return false;
        }

        // $secretKey is obtained from user input, $encryptedData is from the database
        $directDebitDetails = Yii::$app->getSecurity()->decryptByKey(
            $this->direct_debit_details, DirectDebitDetails::getEncryptionKey()
        );
        return unserialize(base64_decode($directDebitDetails));
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'user_id']);
    }

    /**
     * {@inheritdoc}
     * @return QuoteQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new QuoteQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public function afterFind()
    {
        parent::afterFind();

        // Unserialize blob fields after the AR object is created and populated with the query result
        // SAFER SERIALISATION: https://www.jackreichert.com/2014/02/handling-a-php-unserialize-offset-error/
        if (is_string($this->quote_data)) {
            $this->quote_data = unserialize(base64_decode($this->quote_data));
        }
        if (is_string($this->product_data_overrides)) {
            $this->product_data_overrides = unserialize(base64_decode($this->product_data_overrides));
        }
        if (is_string($this->live_charges_data)) {
            $this->live_charges_data = unserialize(base64_decode($this->live_charges_data));
        }
    }

    /**
     * Handle any nuances of data in the model before we commit to insert/update
     * 
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        // Serialize blob fields after the AR object is created and populated with the query result
        // SAFER SERIALISATION: https://www.jackreichert.com/2014/02/handling-a-php-unserialize-offset-error/
        $this->quote_data = (!empty($this->quote_data) && is_array($this->quote_data)) ?
            base64_encode(serialize($this->quote_data)) :
            null;
        $this->product_data_overrides = (!empty($this->product_data_overrides) && is_array($this->product_data_overrides)) ?
            base64_encode(serialize($this->product_data_overrides)) :
            null;
        $this->live_charges_data = (!empty($this->live_charges_data) && is_array($this->live_charges_data)) ?
            base64_encode(serialize($this->live_charges_data)) :
            null;

        $this->modified_date = new \yii\db\Expression('NOW()');
        if (empty($this->created_date)) {
            $this->created_date = new \yii\db\Expression('NOW()');
        }

        if ($this->getIsNewRecord()) {
            $this->uuid = UuidHelper::uuid();
        }

        return true;
    }

    /**
     * Handle any nuances of data in the model after we commit to insert/update
     * 
     * {@inheritdoc}
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert) {
            // custom post-insert code here
        } else {
            // custom post-update code here
        }
    }

    /**
     * Load all ProductCategory instances from database.
     * 
     * @param bool $attributesOnly If true, only get model attributes, not full
     * objects.
     * @return void
     */
    public function loadInventory($attributesOnly = false)
    {
        $this->_inventory = Yii::createObject([
                'class' => Inventory::className(),
                'attributesOnly' => $attributesOnly,
        ]);
    }

    /**
     * Get Inventory component instance.
     * 
     * @param bool $attributesOnly If true, only get model attributes, not full
     * objects.
     * @param bool $refresh If true, refreshes the inventory data.
     * @return app\modules\product\components\Inventory
     */
    public function getInventory($attributesOnly = false, $refresh = false)
    {
        if ($this->_inventory === null || $refresh) {
            $this->loadInventory($attributesOnly);
        }

        return $this->_inventory;
    }

    /**
     * Build a package of totals for the products in the current Quote instance,
     * incorporating product price overrides, discounts etc.
     * 
     * @return object Totals
     */
    public function calculateTotals()
    {
        // initialise empty container for the quote totals
        $totals = new \stdClass();
        if (Yii::$app->user->can('admin')) {
            // include cost totals for admins
            $totals->CostInitial = 0;
            $totals->CostRecurring = 0;
        }
        $totals->ChargeInitial = 0;
        $totals->ChargeRecurring = 0;

        if (is_array($this->quote_data) && !empty($this->quote_data)) {
            foreach ($this->quote_data as $productId => $productLine) {
                if (($product = Product::findOne($productId)) !== null) {

                    // get discount for this product, if available
                    $discount = $this->getBestDiscountForProduct($product, $productLine);

                    foreach ($totals as $property => $value) {
                        // get price to be calculated on, checking for overrides
                        if ($property === 'ChargeInitial' && !empty($this->product_data_overrides[$productId][PD::$FIELD_OCI])) {
                            $price = (float) $this->product_data_overrides[$productId][PD::$FIELD_OCI];
                        } else if ($property === 'ChargeRecurring' && !empty($this->product_data_overrides[$productId][PD::$FIELD_OCR])) {
                            $price = (float) $this->product_data_overrides[$productId][PD::$FIELD_OCR];
                        } else {
                            $price = $product->{$property};
                        }

                        // calculate product line total
                        $productLineTotal = ($price * $productLine[PD::$FIELD_QTY]);

                        // calculate discounted price if a discount was applied
                        if (!empty($discount)) {
                            $productLineTotal = $discount->applyDiscountToPrice($productLineTotal);
                        }

                        $totals->{$property} += $productLineTotal;
                    }
                }
            }
        }

        return $totals;
    }

    /**
     * Build a package of prices for a given product line in the current Quote
     * instance, incorporating product price overrides, discounts etc.
     * 
     * @param app\modules\product\models\Product $product
     * @param array $productLine Expected structure:
     * php```
     * [
     *     'qty' => 1,
     *     'duration' => 24,
     *     'start' => 2018-11-30,
     * ]
     * ```
     * @param bool $individualPrices If true, gets prices for qty of 1 instead
     * of multiplied by $productLine qty. We need quantity of 1-based prices for
     * now because Anvil currently does not support multiples in API - must
     * create one charge for each of selected qty of a product.
     * @return object Prices
     */
    public function calculateQuoteProductPrices(\app\modules\product\models\Product $product, $productLine, $individualPrices = false)
    {
        // initialise empty container for the price totals
        $prices = new \stdClass();
        $prices->ChargeInitial = 0;
        $prices->ChargeRecurring = 0;
        
        // get discount for this product, if available
        $discount = $this->getBestDiscountForProduct($product, $productLine);
        
        foreach ($prices as $property => $value) {
            // get price to be calculated on, checking for overrides
            if ($property === 'ChargeInitial' && !empty($this->product_data_overrides[$product->id][PD::$FIELD_OCI])) {
                $price = (float) $this->product_data_overrides[$product->id][PD::$FIELD_OCI];
            } else if ($property === 'ChargeRecurring' && !empty($this->product_data_overrides[$product->id][PD::$FIELD_OCR])) {
                $price = (float) $this->product_data_overrides[$product->id][PD::$FIELD_OCR];
            } else {
                $price = $product->{$property};
            }

            // calculate product line total price for this property, if requested
            if (!$individualPrices) {
                $productLinePrice = ($price * $productLine[PD::$FIELD_QTY]);
            }

            // calculate discounted price if a discount was applied
            if (!empty($discount)) {
                $productLinePrice = $discount->applyDiscountToPrice($price);
            }

            $prices->{$property} += $productLinePrice;
        }

        return $prices;
    }

    /**
     * Get the best applicable discount for a product line.
     * 
     * @param app\modules\product\models\Product $product
     * @param array $productLine Expected structure:
     * php```
     * [
     *     'qty' => 1,
     *     'duration' => 24,
     *     'start' => 2018-11-30,
     * ]
     * ```
     * @return app\modules\product\models\ProductCategoryDiscount Discount instance
     */
    public function getBestDiscountForProduct(\app\modules\product\models\Product $product, $productLine)
    {
        // get all discounts for this product
        $discounts = $this->inventory->getAllDiscountsForProduct($product);

        // filter out non-applicable discounts for this product line
        foreach ($discounts->categoryDiscounts as $key => $discount) {
            // filter discounts with insufficient quantity
            if ($productLine[PD::$FIELD_QTY] < $discount->min_quantity) {
                unset($discounts->categoryDiscounts[$key]);
                continue;
            }
            // filter discounts with insufficient duration
            if ($productLine[PD::$FIELD_DUR] < $discount->min_duration_months) {
                unset($discounts->categoryDiscounts[$key]);
                continue;
            }
        }

        // get top-ranked discount (highest qualifying by qty and duration)
        if (!empty($discounts->categoryDiscounts)) {
            // sort remaining applicable discounts so that the one
            // with highest quantity and duration comes first, then grab it
            ArrayHelper::multisort($discounts->categoryDiscounts, ['min_quantity', 'min_duration_months'], [SORT_DESC, SORT_DESC]);
            return $discounts->categoryDiscounts[0];
        }

        return false;
    }

    /**
     * Sends Admin Approved email to customer
     * 
     * @return boolean whether the send operation was reported as successful
     */
    public function salesApprovedEmail()
    {
        $subject = "Communicate Quote Ref {$this->getUniqueReference()} Approved!";

        $sent = Yii::$app->mailer->compose('quote/sales-approved', [
                'title' => $subject,
                'uniqueRef' => $this->getUniqueReference(),
                'quoteUrl' => $this->getQuoteUrl(),
                'salesEmail' => Yii::$app->params['salesEmail'],
                'salesPhone' => Yii::$app->params['salesPhone'],
                'salesPhoneTel' => Yii::$app->params['salesPhoneTel'],
            ])
            ->setFrom([Yii::$app->params['salesEmail'] => 'Communicate Sales Team'])
            //->setTo([$this->profile->public_email => $this->profile->name])
            ->setTo([Yii::$app->params['alecEmail'] => 'Alec Pritchard'])
            ->setSubject($subject)
            ->send();

        return $sent;
    }

    /**
     * Sends Order Complete email to customer & sales team
     * 
     * @return boolean whether the send operation was reported as successful
     */
    public function orderCompleteEmail()
    {
        $subject = "Communicate Order Complete! Quote Ref {$this->getUniqueReference()}";

        $sent = Yii::$app->mailer->compose('quote/order-complete', [
                'title' => $subject,
                'uniqueRef' => $this->getUniqueReference(),
                'quoteUrl' => $this->getQuoteUrl(),
                'salesEmail' => Yii::$app->params['salesEmail'],
                'salesPhone' => Yii::$app->params['salesPhone'],
                'salesPhoneTel' => Yii::$app->params['salesPhoneTel'],
            ])
            ->setFrom([Yii::$app->params['salesEmail'] => 'Communicate Sales Team'])
            //->setTo([$this->profile->public_email => $this->profile->name])
            ->setTo([Yii::$app->params['alecEmail'] => 'Alec Pritchard'])
            ->setBcc(Yii::$app->params['salesEmail'])
            ->setSubject($subject)
            ->send();

        return $sent;
    }

}

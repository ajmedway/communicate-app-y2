<?php

namespace app\modules\quote\models;

use Yii;
use yii\base\Model;

/**
 * DirectDebitDetails is the model behind the direct debit details form.
 */
class DirectDebitDetails extends Model
{

    /**
     * @var Randomly generated encryption key. Do not change!
     */
    private static $ENCRYPTION_KEY = '«}¥~åhK¿¯J½dý~EÛ¾¸FßbÁ\G¹Å©';

    public $account_name;
    public $account_number;
    public $sort_code;
    public $address;
    public $reference;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['account_name', 'account_number', 'sort_code', 'address', 'reference'], 'required'],
            [['account_name', 'sort_code', 'address', 'reference'], 'string'],
            [['account_number'], 'string', 'length' => 8],
            [['sort_code'], 'string', 'length' => 6],
            [['account_number', 'sort_code'], 'integer'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'account_name' => 'Name of account holder',
            'account_number' => 'Bank/Building Society account number',
            'sort_code' => 'Branch sort code',
            'address' => 'Name and full postal address of your Bank/Building Society',
            'reference' => 'Reference',
        ];
    }
    
    /**
     * @return string Encryption Key
     */
    public static function getEncryptionKey() {
        return self::$ENCRYPTION_KEY;
    }

}

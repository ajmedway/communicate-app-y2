<?php

namespace app\modules\quote\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\quote\models\Quote;

/**
 * QuoteSearch represents the model behind the search form of `app\modules\quote\models\Quote`.
 */
class QuoteSearch extends Quote
{

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        // add related fields to searchable attributes
        return array_merge(parent::attributes(), ['profile.name']);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id'], 'integer'],
            [['uuid', 'profile.name', 'created_date', 'modified_date', 'quote_data', 'direct_debit_details', 'live_charges_data', 'notes', 'status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Quote::find()->indexBy('id')->with('profile');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        // join with relation `profile` that is a relation to the table `profile`
        // and set the table alias to be `p`
        $query->joinWith('profile AS p');

        // enable sorting for the related columns
        $dataProvider->sort->attributes['profile.name'] = [
            'asc' => ['p.name' => SORT_ASC],
            'desc' => ['p.name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'p.user_id' => $this->user_id,
            'created_date' => $this->created_date,
            'modified_date' => $this->modified_date,
        ]);

        $query->andFilterWhere(['like', 'uuid', $this->uuid])
            ->andFilterWhere(['like', 'p.name', $this->getAttribute('profile.name')])
            ->andFilterWhere(['like', 'quote_data', $this->quote_data])
            ->andFilterWhere(['like', 'direct_debit_details', $this->direct_debit_details])
            ->andFilterWhere(['like', 'live_charges_data', $this->live_charges_data])
            ->andFilterWhere(['like', 'notes', $this->notes])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }

}

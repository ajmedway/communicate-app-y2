<?php

namespace app\modules\quote;

use Yii;
use yii\base\Module;
use yii\base\BootstrapInterface;
use yii\web\Application as WebApplication;

/**
 * quote module definition class
 */
class Quote extends Module implements BootstrapInterface
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\quote\controllers';

    /**
     * @var string the route prefix
     */
    public $prefix = 'quote';

    /**
     * @var array the url rules (routes)
     */
    public $routes = [
        '<action:[\w-]+>' => 'quote/<action>',
    ];

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

    /**
     * {@inheritdoc}
     *
     * @throws InvalidConfigException
     */
    public function bootstrap($app)
    {
        if ($app->hasModule('quote') && $app->getModule('quote') instanceof Module) {
            if ($app instanceof WebApplication) {
                $this->initUrlRoutes($app);
            }
        }
    }

    /**
     * Initializes web url routes (rules in Yii2).
     *
     * @param WebApplication $app
     *
     * @throws InvalidConfigException
     */
    protected function initUrlRoutes(WebApplication $app)
    {
        /** @var $module Module */
        $module = $app->getModule('quote');
        $config = [
            'class' => 'yii\web\GroupUrlRule',
            'prefix' => $module->prefix,
            'rules' => $module->routes,
        ];

        if ($module->prefix !== 'quote') {
            $config['routePrefix'] = 'quote';
        }

        $rule = Yii::createObject($config);

        $app->getUrlManager()->addRules([$rule], false);
    }

}

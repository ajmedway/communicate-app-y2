<?php
/* @var $this yii\web\View */
/* @var $quotes app\modules\quote\models\Quote[] */

$this->title = 'Quote Manager';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="quote-quote-index">
    <h1>Welcome to the Communicate Quote Manager</h1>
    <div class="row">
        <div class="about-us-text col-sm-12">
            <p>
                Please select one of your existing quotes below, which you can make changes to at any time. Please note that any changes made will require checking and approval from a member of our sales team, who may need to make some adjustments to your package if necessary. If approved, you will then have a final say over whether to proceed with the updated quote package.
            </p>
        </div>
    </div>

    <hr>

    <h2>My Quotes</h2>

    <div id="quote-details">
        <div class="row">
            <?php if (!empty($quotes)): ?>
                <?php foreach ($quotes as $quote): ?>
                    <?=
                    $this->render('_quote-summary', [
                        'quote' => $quote,
                        'includeManageButton' => true,
                    ])
                    ?>
                <?php endforeach; ?>
            <?php else: ?>
                <div class="col-lg-6">
                    <p class="text-danger">
                        There are no quotes pending sales approval at present, please check back later...
                    </p>
                </div>
            <?php endif; ?>
        </div>
    </div>

</div>

<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\modules\quote\models\Quote;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\quote\models\QuoteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Quotes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="quote-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php /*<p>
        <?= Html::a('Create Quote', ['create'], ['class' => 'btn btn-success']) ?>
    </p>*/ ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'contentOptions' => ['style' => 'width: 100px;'],
            ],
            [
                'attribute' => 'profile.name',
                'label' => 'Customer Name',
                'format' => 'html',
                'value' => function ($model) {
                        return Html::a($model->profile->name, ['/user/admin/update', 'id' => $model->profile->user_id]);
                },
            ],
            [
                'attribute' => 'uuid',
                'format' => 'html',
                'value' => function ($model) {
                        return Html::a($model->uuid, ['/quote/manage-admin', 'uuid' => $model->uuid]);
                },
            ],
            //'quote_data',
            //'product_data_overrides',
            //'direct_debit_details',
            //'live_charges_data',
            //'notes:ntext',
            [
                'attribute' => 'status',
                'format' => 'html',
                'value' => function ($model) {
                        return Quote::getStatusLabelHtml($model->status);
                },
            ],
            'created_date:datetime',
            'modified_date:datetime',

            [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view}',
            'urlCreator' => function ($action, $model, $key, $index) {
                if ($action === 'view') {
                    return Url::toRoute(['/quote/manage-admin', 'uuid' => $model->uuid]);
                }
            }
        ],
    ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>

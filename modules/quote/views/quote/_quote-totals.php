<?php

use yii\helpers\Inflector;
use yii\helpers\Url;
use app\modules\product\models\Product;

/* @var $this yii\web\View */
/* @var $totals array */

/* @var $product app\modules\product\models\Product */
$product = new Product();

/* @var $formatter \yii\i18n\Formatter */
$formatter = Yii::$app->formatter;
?>

<div id="quote-totals-data">
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <?php foreach ($totals as $attribute => $total): ?>
                    <div class="col-md-<?= 12 / count((array) $totals) ?>">
                        <div class="total-item">
                            <?= $product->getAttributeLabel($attribute) ?>: <span class="label label-primary"><?= $formatter->asCurrency($total) ?></span>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>

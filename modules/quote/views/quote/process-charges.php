<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $quote app\modules\quote\models\Quote */

$this->title = 'Checkout Complete';
$this->params['breadcrumbs'][] = ['label' => 'Quote Manager', 'url' => ['manage']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modules-quote-views-quote-process-charges">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-sm-12">
            <p class="lead"><strong>You have now created your new order successfully. We will be in touch!</strong></p>
        </div>
    </div>
    <div class="row">
        <div id="cross-sell-links" class="col-sm-12">
            <div class="cross-sell-link">
                <a href="<?= Url::to(['/site/cyber-security']) ?>">
                    <div class="inner">
                        <div id="csl-bg-img-1" class="bg-img"></div>
                        <div class="text">
                            <h4>Cyber Security</h4>
                            <p>Queries about our cyber security products? Click here.</p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="cross-sell-link">
                <a href="#">
                    <div class="inner">
                        <div id="csl-bg-img-2" class="bg-img"></div>
                        <div class="text">
                            <h4>This Is A Business Area</h4>
                            <p>Check it out...</p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="cross-sell-link">
                <a href="#">
                    <div class="inner">
                        <div id="csl-bg-img-3" class="bg-img"></div>
                        <div class="text">
                            <h4>This Is A Business Area</h4>
                            <p>Check it out...</p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="cross-sell-link">
                <a href="#">
                    <div class="inner">
                        <div id="csl-bg-img-4" class="bg-img"></div>
                        <div class="text">
                            <h4>This Is A Business Area</h4>
                            <p>Check it out...</p>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12" style="display: none;">
            <h2>Raw Charges Data Received From Anvil</h2>
            <p class="muted">Note: This is not to be displayed to the customers.</p>
            <div>
                <?php
                echo '<pre>';
                echo '$quote->live_charges_data: ' . print_r($quote->live_charges_data, true);
                echo '</pre>';
                ?>
            </div>
        </div>
    </div>

</div>

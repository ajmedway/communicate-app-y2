<?php

use yii\helpers\Inflector;
use yii\helpers\Url;
use app\modules\quote\models\Quote;

/* @var $this yii\web\View */
/* @var $quote app\modules\quote\models\Quote */
/* @var $includeManageButton bool */
/* @var $includeCheckoutButton bool */

/* @var $formatter \yii\i18n\Formatter */
$formatter = Yii::$app->formatter;
?>

<div class="col-lg-6">
    <ul class="list-group">
        <li class="list-group-item">Quote Unique Reference: <span class="label label-primary"><?= $quote->getUniqueReference() ?></span></li>
        <li class="list-group-item">Quote Status: <?= Quote::getStatusLabelHtml($quote->status) ?></li>
        <li class="list-group-item">Created Date: <span class="label label-default"><?= $formatter->asDatetime($quote->created_date, 'long') ?></span></li>
        <li class="list-group-item">Modified Date: <span class="label label-default"><?= $formatter->asDatetime($quote->modified_date, 'long') ?></span></li>
        <li class="list-group-item">Contact: <span class="label label-default"><?= $quote->profile->name . " ({$quote->profile->public_email})" ?></span></li>
        <li class="list-group-item">Company Name: <span class="label label-default"><?= $quote->profile->company->hs_name ?></span></li>
        <li class="list-group-item">
            <h5 class="list-group-item-heading">Notes...</h5>
            <br>
            <blockquote class="list-group-item-text">
                <p><i><?= Inflector::humanize($quote->notes, false) ?></i></p>
            </blockquote>
        </li>
        <?php if (!empty($includeManageButton)): ?>
            <li class="list-group-item"><a href="<?= Url::to(['/quote/manage' . (Yii::$app->user->can('admin') ? '-admin' : ''), 'uuid' => $quote->uuid]) ?>" class="btn btn-lg btn-block btn-success">Manage This Quote</a></li>
        <?php endif; ?>
        <?php if (!empty($includeCheckoutButton)): ?>
            <li class="list-group-item"><a href="<?= Url::to(['/quote/checkout', 'uuid' => $quote->uuid]) ?>" class="btn btn-lg btn-block btn-danger">Finalise &amp; Checkout</a></li>
        <?php endif; ?>
    </ul>
</div>

<?php

use yii\helpers\Inflector;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use kartik\date\DatePicker;
use app\modules\product\models\Product;
use app\modules\product\components\helpers\ProductData as PD;
use app\modules\quote\models\Quote;

/* @var $this yii\web\View */
/* @var $inventory app\modules\product\components\Inventory */
/* @var $quote app\modules\quote\models\Quote */
/* @var $canSave bool */

$this->title = 'Quote Manager';
if (Yii::$app->user->can('admin')) {
    $this->params['breadcrumbs'][] = ['label' => 'Quotes', 'url' => ['index']];
}
$this->params['breadcrumbs'][] = $this->title;

$formatter = Yii::$app->formatter;

$qtyRange = range(0, 50);
$qtyOptions = array_combine($qtyRange, $qtyRange);
$approvedPendingCharges = (!empty($quote->status) && $quote->status === Quote::$STATUSES['APPROVED_PENDING_CHARGES']);
?>

<div class="quote-quote-index">
    <h1>Welcome to the Communicate Quote Manager</h1>

    <?php if (Yii::$app->user->can('admin')): ?>
        <div class="row">
            <div class="about-us-text col-sm-12">
                <p class="lead">
                    <span class="label label-default"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span> Sales Team</span> Please review the quote below, making any changes necessary. Then click &QUOT;Save &amp; Approve Quote&QUOT; once you are happy with everything.
                </p>
                <p class="lead">
                    <span class="label label-info"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span> Hint</span> You can override initial and recurring charges for each product. Just blank out the price override fields to cancel and revert to default prices.
                </p>
            </div>
        </div>
    <?php else: ?>
        <div class="row">
            <div class="about-us-text col-sm-12">
                <p class="lead">
                    Please follow through the form to build your perfect office solution, then click &QUOT;Save &amp; Approve Quote&QUOT; once you are happy with everything. You may customise various aspects of your chosen services at a later time.
                </p>
            </div>
        </div>
        <div class="row">
            <div class="about-us-text col-sm-12">
                <p>
                    All services are primarily based on a simple per user per month basis (PUPM) making it easy for you to add and remove users as your business requirements change over time. Certain services are contracted over 1, 3, 5 or 10 year terms, whilst others provide easy in easy out, low cost monthly contracts making it simple for clients to forecast and budget.
                </p>
            </div>
        </div>
    <?php endif; ?>

    <hr>

    <?php if (!empty($quote->uuid)): ?>
        <div id="quote-details">
            <h2>Quote Details</h2>
            <div class="row">
                <?=
                $this->render('_quote-summary', [
                    'quote' => $quote,
                    'includeCheckoutButton' => ($approvedPendingCharges && !Yii::$app->user->can('admin')),
                ])
                ?>
            </div>
        </div>
    <?php endif; ?>

    <?php
    /**
     * Render totals bar inside pjax container.
     * NOTE: this uses a custom pjax.reload() implementation in @web/js/main.js 
     * @see https://www.yiiframework.com/doc/api/2.0/yii-widgets-pjax
     */
    Pjax::begin([
        'id' => 'quote-totals',
        'formSelector' => false,
        'linkSelector' => false,
        'enablePushState' => false,
        'enableReplaceState' => false,
    ]);
    ?>
    <?= $totalsBar ?>
    <?php Pjax::end(); ?>

    <?php if (!$approvedPendingCharges): ?>

        <h2>Quote Products</h2>

        <?php
        $form = ActiveForm::begin([
                'id' => 'quote-form',
                'options' => [
                    'readonly' => !$canSave,
                    'disabled' => !$canSave,
                ],
            ])
        ?>
        <?= Html::hiddenInput('existing-uuid', (!empty($quote->uuid) ? $quote->uuid : '')) ?>

        <?php foreach ($inventory->getInventoryData() as $groupName => $category): ?>

            <?php foreach ($category as $categoryName => $data): ?>

                <?php if (!empty($data->products)): ?>

                    <h4><?= $groupName . ' &gt; ' . $categoryName ?></h4>

                    <?php
                    foreach ($data->products as $product):
                        // initialise product default selections and price override vars
                        $qty = $duration = $overrideChargeInitial = $overrideChargeRecurring = null;
                        $start = date('Y-m-d', strtotime('today'));

                        // if the customer has an existing quote, load the product selection values from this
                        if (!empty($quote->quote_data[$product->id])) {
                            $qty = $quote->quote_data[$product->id][PD::$FIELD_QTY];
                            $duration = $quote->quote_data[$product->id][PD::$FIELD_DUR];
                            $start = $quote->quote_data[$product->id][PD::$FIELD_STR];

                            // obtain product line price overrides
                            $overrideChargeInitial = !empty($quote->product_data_overrides[$product->id][PD::$FIELD_OCI]) ? $quote->product_data_overrides[$product->id][PD::$FIELD_OCI] : null;
                            $overrideChargeRecurring = !empty($quote->product_data_overrides[$product->id][PD::$FIELD_OCR]) ? $quote->product_data_overrides[$product->id][PD::$FIELD_OCR] : null;
                        }
                        ?>

                        <div class="row quote-product">
                            <div class="col-sm-3 product-data">
                                <p class="product-description">
                                    <strong>
                                        <?= Inflector::humanize($product->Description, true) ?>
                                    </strong>
                                </p>
                                <p class="text-info">
                                    <small>
                                        <?= $product->getAttributeLabel('BillingFrequency') ?>:
                                        <span class="value"><?= Inflector::humanize($product->BillingFrequency, true) ?></span>
                                    </small>
                                </p>
                                <?php if (Yii::$app->user->can('admin')): ?>
                                    <p class="text-info">
                                        <small>
                                            <?= $product->getAttributeLabel('CostInitial') ?>:
                                            <span class="price"><?= $formatter->asCurrency($product->CostInitial) ?></span>
                                        </small>
                                    </p>
                                    <p class="text-info">
                                        <small>
                                            <?= $product->getAttributeLabel('CostRecurring') ?>:
                                            <span class="price"><?= $formatter->asCurrency($product->CostRecurring) ?></span>
                                        </small>
                                    </p>
                                <?php endif; ?>
                                <p class="text-info <?= !empty($overrideChargeInitial) ? 'overridden' : '' ?>">
                                    <small>
                                        <?= $product->getAttributeLabel('ChargeInitial') ?>: <span class="price"><?= $formatter->asCurrency($product->ChargeInitial) ?></span>
                                        <?php if (!empty($overrideChargeInitial)): ?>
                                            <span class="price-override"><?= $formatter->asCurrency($overrideChargeInitial) ?></span>
                                        <?php endif; ?>
                                    </small>
                                </p>
                                <p class="text-info <?= !empty($overrideChargeRecurring) ? 'overridden' : '' ?>">
                                    <small>
                                        <?= $product->getAttributeLabel('ChargeRecurring') ?>: <span class="price"><?= $formatter->asCurrency($product->ChargeRecurring) ?></span>
                                        <?php if (!empty($overrideChargeRecurring)): ?>
                                            <span class="price-override"><?= $formatter->asCurrency($overrideChargeRecurring) ?></span>
                                        <?php endif; ?>
                                    </small>
                                </p>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <?php $name = PD::getProductFormName($product, PD::$FIELD_QTY) ?>
                                    <?= Html::label('Quantity', $name) ?>
                                    <?= Html::dropDownList($name, $qty, $qtyOptions, ['class' => 'form-control', 'id' => $name]) ?>
                                </div>
                                <?php if (Yii::$app->user->can('admin')): ?>
                                    <div class="form-group">
                                        <?php $name = PD::getOverrideFormName($product, PD::$FIELD_OCI) ?>
                                        <?= Html::label("Override {$product->getAttributeLabel('ChargeInitial')}", $name) ?>
                                        <div class="input-group">
                                            <div class="input-group-addon"><span class="glyphicon glyphicon-gbp" aria-hidden="true"></span></div>
                                            <?= Html::textInput($name, $overrideChargeInitial, ['class' => 'form-control', 'id' => $name, 'placeholder' => $product->ChargeInitial, 'aria-required' => true]) ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <?php $name = PD::getOverrideFormName($product, PD::$FIELD_OCR) ?>
                                        <?= Html::label("Override {$product->getAttributeLabel('ChargeRecurring')}", $name) ?>
                                        <div class="input-group">
                                            <div class="input-group-addon"><span class="glyphicon glyphicon-gbp" aria-hidden="true"></span></div>
                                            <?= Html::textInput($name, $overrideChargeRecurring, ['class' => 'form-control', 'id' => $name, 'placeholder' => $product->ChargeRecurring, 'aria-required' => true]) ?>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <?php $name = PD::getProductFormName($product, PD::$FIELD_DUR) ?>
                                    <?= Html::label('Contract Duration', $name) ?>
                                    <?= Html::dropDownList($name, $duration, Product::contractDurationOptions(), ['class' => 'form-control', 'id' => $name]) ?>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <?php $name = PD::getProductFormName($product, PD::$FIELD_STR) ?>
                                    <?= Html::label(Yii::$app->user->can('admin') ? 'Start Date' : 'Preferred Start Date', $name) ?>
                                    <?=
                                    DatePicker::widget([
                                        'name' => $name,
                                        'value' => $start,
                                        'options' => ['placeholder' => 'Select date...'],
                                        'pluginOptions' => [
                                            'format' => 'yyyy-mm-dd',
                                            'todayHighlight' => true,
                                        ],
                                    ]);
                                    ?>
                                </div>
                            </div>
                        </div>
                        <hr>

                    <?php endforeach; ?>
                <?php else: ?>
                    <!--
                    <div class="row" style="margin-bottom: 20px;">
                        <div class="col-sm-6">
                            <p class="text-danger"><small>No products available in this category at present.</small></p>
                        </div>
                    </div>
                    <hr>
                    -->
                <?php endif; ?>

            <?php endforeach; ?>

        <?php endforeach; ?>

        <?php if ($canSave): ?>
            <div class="form-group">
                <?= Html::submitButton('Save &amp; Approve Quote', ['name' => 'save-approve', 'value' => 1, 'class' => 'btn btn-success']) ?>
            </div>
        <?php endif; ?>

        <?php ActiveForm::end() ?>

    <?php endif; ?>

</div>

<script>
    // Create JSON representation of the inventory, parse as a JavaScript object
    var inventory = '<?= json_encode($inventory, JSON_HEX_APOS); ?>';
    var inventory_data = JSON.parse(inventory);
    //console.log(inventory_data);
</script>

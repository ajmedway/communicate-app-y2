<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\quote\models\DirectDebitDetails;

/* @var $this yii\web\View */
/* @var $quote app\modules\quote\models\Quote */
/* @var $ddDetails app\modules\quote\models\DirectDebitDetails */
/* @var $form ActiveForm */

$this->title = 'Quote Checkout';
$this->params['breadcrumbs'][] = ['label' => 'Quote Manager', 'url' => ['manage']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modules-quote-views-quote-checkout">
    <h1>Enter Your Direct Debit Details</h1>

    <div class="row">
        <div class="col-sm-6">
            <address>
                <strong>Communicate PLC</strong><br>
                Wynyard Park House<br>
                Billingham House<br>
                Teesside, TS22 5TB<br>
            </address>
        </div>
        <div class="col-sm-6">
            <p class="lead">
                <strong>Instruction to your bank or building society<br>to pay by Direct Debit</strong>
            </p>
        </div>
    </div>

    <hr>

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-12">
                    <?= $form->field($ddDetails, 'account_name') ?>
                </div>
                <div class="col-sm-12">
                    <?= $form->field($ddDetails, 'address')->textarea() ?>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-12">
                    <?= $form->field($ddDetails, 'account_number') ?>
                </div>
                <div class="col-sm-12">
                    <?= $form->field($ddDetails, 'sort_code') ?>
                </div>
                <div class="col-sm-12">
                    <?= $form->field($ddDetails, 'reference')->textInput(['readonly' => true]) ?>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(($ddDetails->validate() ? 'Update' : 'Submit') . ' Direct Debit Details', ['class' => 'btn btn-primary']) ?>
        <?php
        // verify that the DirectDebitDetails saved into the current quote are valid before we display the link to process charges
        $ddDetailsOnCurrentQuote = new DirectDebitDetails($quote->decryptDirectDebitDetails());
        ?>
        <?php if ($ddDetailsOnCurrentQuote->validate()): ?>
            <?= Html::a('Submit &amp; Create Charges', ['/quote/process-charges', 'uuid' => $quote->uuid], ['class' => 'btn btn-danger']) ?>
        <?php endif; ?>
    </div>
    <?php ActiveForm::end(); ?>

    <hr>

    <div class="row">
        <div id="dd-guarantee" class="col-sm-10 col-sm-offset-1">
            <div id="dd-logo" class="pull-right">
                <?= Html::img('@web/img/direct-debit-logo.png', ['alt' => 'Direct Debit Logo']); ?>
            </div>
            <h3>The Direct Debit Guarantee</h3>
            <ul>
                <li>The Guarantee is offered by all banks and building societies that accept instructions to pay Direct Debits.</li>
                <li>If there are any changes to the amount, date or frequency of your Direct Debit the organisation will notify you (normally 10 working days) in advance of your account being debited or as otherwise agreed. If you request the organisation to collect a payment, confirmation of the amount and date will be given to you at the time of the request.</li>
                <li>If an error is made in the payment of your Direct Debit, by the organisation or your bank or building society, you are entitled to a full and immediate refund of the amount paid from your bank or building society.
                    <ul>
                        <li>If you receive a refund you are not entitled to, you must pay it back when the organisation asks you to.</li>
                    </ul>
                </li>
                <li>You can cancel a Direct Debit at any time by simply contacting your bank or building society. Written confirmation may be required. Please also notify the organisation.</li>
            </ul>
        </div>
    </div>

</div><!-- modules-quote-views-quote-checkout -->

<?php

namespace app\modules\quote\migrations;

use yii\db\Migration;

/**
 * Class m180529_171409_add_status_index_to_quote_table
 */
class m180529_171409_add_status_index_to_quote_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // creates index for column `status`
        $this->createIndex(
            'idx-quote-status',
            'quote',
            'status'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180529_171409_add_status_index_to_quote_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180529_171409_add_status_index_to_quote_table cannot be reverted.\n";

        return false;
    }
    */
}

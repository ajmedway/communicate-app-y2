<?php

namespace app\modules\quote\migrations;

use yii\db\Migration;

/**
 * Handles adding live_charges_data to table `quote`.
 */
class m180621_140433_add_live_charges_data_column_to_quote_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('quote', 'live_charges_data', $this->binary()->after('direct_debit_details')->defaultValue(NULL));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('quote', 'live_charges_data');
    }
}

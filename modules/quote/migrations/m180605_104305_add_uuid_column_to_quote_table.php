<?php

namespace app\modules\quote\migrations;

use yii\db\Migration;

/**
 * Handles adding uuid to table `quote`.
 */
class m180605_104305_add_uuid_column_to_quote_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('quote', 'uuid', $this->char(36)->after('id'));
        
        // creates index for column `uuid`
        $this->createIndex(
            'idx-quote-uuid',
            'quote',
            'uuid'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('quote', 'uuid');
    }
}

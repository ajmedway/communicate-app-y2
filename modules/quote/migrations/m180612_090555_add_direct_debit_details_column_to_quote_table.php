<?php

namespace app\modules\quote\migrations;

use yii\db\Migration;

/**
 * Handles adding direct_debit_details to table `quote`.
 */
class m180612_090555_add_direct_debit_details_column_to_quote_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('quote', 'direct_debit_details', $this->binary()->defaultValue(NULL)->after('quote_data'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('quote', 'direct_debit_details');
    }
}

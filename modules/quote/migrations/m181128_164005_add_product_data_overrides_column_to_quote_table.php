<?php

namespace app\modules\quote\migrations;

use yii\db\Migration;

/**
 * Handles adding product_data_overrides to table `quote`.
 */
class m181128_164005_add_product_data_overrides_column_to_quote_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('quote', 'product_data_overrides', $this->binary()->after('quote_data')->defaultValue(NULL));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('quote', 'product_data_overrides');
    }
}

<?php

namespace app\modules\quote\migrations;

use yii\db\Migration;

/**
 * Handles the creation of table `quote`.
 * Has foreign keys to the tables:
 *
 * - `profile`
 */
class m180529_165128_create_quote_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('quote', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->defaultValue(NULL),
            'created_date' => $this->dateTime()->defaultValue(NULL),
            'modified_date' => $this->dateTime()->defaultValue(NULL),
            'quote_data' => $this->binary()->defaultValue(NULL),
            'notes' => $this->text()->defaultValue(NULL),
            'status' => $this->string()->defaultValue(NULL),
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            'idx-quote-user_id',
            'quote',
            'user_id'
        );

        // add foreign key for table `profile`
        $this->addForeignKey(
            'fk-quote-user_id',
            'quote',
            'user_id',
            'profile',
            'user_id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `profile`
        $this->dropForeignKey(
            'fk-quote-user_id',
            'quote'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'idx-quote-user_id',
            'quote'
        );

        $this->dropTable('quote');
    }
}

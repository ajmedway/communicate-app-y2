<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use Da\User\Filter\AccessRuleFilter;
use app\components\managers\HubspotDataManager;
use app\models\User;
use app\models\ContactForm;
use app\modules\product\models\Product;
use app\modules\product\models\Supplier;

class SiteController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRuleFilter::class,
                ],
                // ? = "guest users".
                // @ = "authenticated users".
                //'only' => ['about'],
                'rules' => [
                    [
                        // prevent authenticated admins from accessing the contact page
                        'actions' => ['contact'],
                        'allow' => false,
                        'roles' => ['admin'],
                    ],
                    [
                        // allow guest and users to access the contact page
                        'actions' => ['contact'],
                        'allow' => true,
                        'roles' => ['?', 'user'],
                    ],
                    [
                        // open all general site controller actions to admins/users
                        'allow' => true,
                        'roles' => ['admin', 'user'],
                    ],
                    [
                        // allow all to see index, about, captcha and error pages
                        'actions' => ['index', 'about', 'captcha', 'error'],
                        'allow' => true,
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Testing Fixtures Data
     */
    public function actionTestProductFixture()
    {
        throw new NotFoundHttpException('The requested page does not exist.');

        $testProductFixture = new \app\tests\unit\fixtures\TestProductFixture();
        $testProductFixture->load();

        echo '<pre>';
        echo '$testProductFixture: ' . print_r($testProductFixture->data, true);
        echo '</pre>';
    }

    /**
     * General Anvil testing action
     *
     * @return string
     */
    public function actionAnvilTest()
    {
        throw new NotFoundHttpException('The requested page does not exist.');

        if (0 && isset($_SERVER['HTTP_ALEC'])) {
            echo '<pre>';
            echo '$data = ' . var_export(json_decode('
                    [
                        {
                          "ID": "1",
                          "Name": "Small business"
                        },
                        {
                          "ID": "2",
                          "Name": "Medium business"
                        },
                        {
                          "ID": "3",
                          "Name": "Corporate"
                        },
                        {
                          "ID": "4",
                          "Name": "Charity"
                        },
                        {
                          "ID": "5",
                          "Name": "Governmental"
                        },
                        {
                          "ID": "6",
                          "Name": "Residential"
                        },
                        {
                          "ID": "7",
                          "Name": "Other organisation"
                        },
                        {
                          "ID": "8",
                          "Name": "M2M"
                        },
                        {
                          "ID": "9",
                          "Name": "M2M Trial"
                        },
                        {
                          "ID": "10",
                          "Name": "Wholesale"
                        }
                    ]'
                , true), true);
            echo ';</pre>';
            exit;
        }

        $anvil = Yii::$app->anvilDataManager;

        # ChargeProducts
        $response = $anvil->test();
        //$response = $anvil->getChargeProduct(13, 8);
        //$response = $anvil->getChargeProducts(13);
        // Getting 500: insufficient rights on POST/PATCH
        //$product = Product::findOne(1);
        //$response = $anvil->createChargeProduct($product);
        //$response = $anvil->updateChargeProduct(2, $product);
        # Customers
        //$response = $anvil->getCustomersSearch('test');
        //$response = $anvil->getCustomer(264);
        // Getting 500: insufficient rights on POST/PATCH
        
        $customer = [
            'CompanyID' => 13,
            'Name' => 'Test customer (Alec) v1',
            'CompanyName' => 'Test Company (Alec) v1',
            'PartnerID' => 37,
            'CurrencyID' => 'GBP',
            'CountryID' => 'GB',
            'LanguageID' => 'EN',
            //'PrimaryContactID' => null,
            //'PrimaryAddressID' => null,
            'Website' => 'www.testalec123.com',
            'CustomerTypeID' => 2,
            'CompanyNumber' => '1274567',
            'VATref' => 'GB 987 6543 21',
            'EstimatedSpend' => 1000,
            'DealerInits' => 'aa',
            'ContractDuration' => 3,
            'PaymentTermsDays' => 14,
            'MinimumSpend' => 100,
            'Category' => 'Test',
        ];
        
        //$response = $anvil->createCustomer($customer);
        //$response = $anvil->updateCustomer(266, $customer);
        # Contacts
        //$response = $anvil->getContactsSearch('jane.doe@example.com');
        //$response = $anvil->getContacts(264);
        //$response = $anvil->getContact(264, 132);

        //$user = User::findOne(1);
        //$response = $anvil->createContact(264, $user->profile);
        //$response = $anvil->updateContact(264, 0, $user->profile);
        # Addresses
        //$response = $anvil->getAddresses(264);
        //$response = $anvil->getAddress(264, 245);
        //$response = $anvil->createAddress(264, $user->profile);
        //$response = $anvil->updateAddress(264, 245, $user->profile);

        # Charges
        /*$patchChargeAdHoc = array(
            'Description' => 'string',
            'BillingFrequency' => 'string',
            'CostInitial' => 0,
            'CostRecurring' => 0,
            'SupplierAccountID' => 0,
            'ProductID' => 'string',
            'CurrencyID' => 'string',
            'CountryID' => 'string',
            'TaxRateID' => 0,
            'ObjectName' => 'string',
            'OtherInfo' => 'string',
            'DateEnd' => '2018-05-23T16:23:51.832Z',
            'ChargeInitial' => 0,
            'ChargeRecurring' => 0,
        );
        $postChargeAdHoc = array(
            'Description' => 'string',
            'BillingFrequency' => 'string',
            'CostInitial' => 0,
            'CostRecurring' => 0,
            'SupplierAccountID' => 0,
            'ProductID' => 'string',
            'CurrencyID' => 'string',
            'CountryID' => 'string',
            'TaxRateID' => 0,
            'DateStart' => '2018-05-23T16:23:51.815Z',
            'ObjectName' => 'string',
            'OtherInfo' => 'string',
            'DateEnd' => '2018-05-23T16:23:51.815Z',
            'ChargeInitial' => 0,
            'ChargeRecurring' => 0,
        );*/
        //$response = $anvil->getCharges(264);
        //$response = $anvil->getCharge(264, 123);
        //$response = $anvil->createChargeAdHoc(264, $postChargeAdHoc);
        //$response = $anvil->updateChargeAdHoc(264, 123, $patchChargeAdHoc);

        echo '<pre>';
        echo '$response: ' . print_r($response, true);
        echo '</pre>';
    }

    /**
     * General Hubspot testing action
     *
     * @return string
     */
    public function actionHubspotTest()
    {
        throw new NotFoundHttpException('The requested page does not exist.');

        $hubspot = Yii::$app->hubspotDataManager;

        // get 100 contacts starting from a certain Hubspot user ID
//        $contacts = $hubspot->getAllContacts(100, 19351);
//        echo '<pre>';
//        echo '$contact: ' . print_r($contact, true);
//        echo '</pre>';
//        $company = $hubspot->getCompanyById(682953615);
//        echo '<pre>';
//        echo '$company: ' . print_r($company, true);
//        echo '</pre>';
//        die('here1');
        // get a contact by email
        //$contact = $hubspot->getContactByEmail('alec@r-evolution.co.uk');

        $user = User::findOne(['email' => 'ajmedway@hotmail.com']);

        $response = $hubspot->updateProfileFromHubspotContactByUser($user);
        //$response = $hubspot->updateHubspotContactFromProfileByUser($user);

        echo '<pre>';
        echo '$response: ' . print_r($response, true);
        echo '</pre>';
        die('here2');

        $email = 'alec@r-evolution.co.uk';
        $properties = [
            [
                'property' => 'email',
                'value' => 'alec@r-evolution.co.uk',
            ],
            [
                'property' => 'firstname',
                'value' => 'Plec',
            ],
            [
                'property' => 'lastname',
                'value' => 'Pritchard TEST01',
            ],
        ];
        $contact = $hubspot->updateContactByEmail($email, $properties);
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['salesEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
                'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionCyberSecurity()
    {
        return $this->render('cyber-security');
    }

}

<?php
namespace app\components\helpers;

/**
 * @author Alec Pritchard <alec@r-evolution.co.uk>
 * 
 * Class Template helper
 */
class Template {
    
    /**
     * Get yii\bootstrap\ActiveField template for textInput with symbol/glyphicon addon
     * 
     * @param string $symbol The symbol to be displayed
     * @return string
     */
    public static function getTextInputAddonTemplate($symbol = '&pound;', $isGlyphicon = false)
    {
        if ($isGlyphicon) {
            $symbol = "<span class='glyphicon {$symbol}' aria-hidden='true'></span>";
        }
        
        return "
            {label}
            <div class='input-group'>
                <div class='input-group-addon'>{$symbol}</div>
                {input}
            </div>
            {error}
            {hint}";
    }
    
    /**
     * Get yii\bootstrap\ActiveField template for textInput with a button addon
     * 
     * @return string
     */
    public static function getTextInputButtonTemplate($glyphicon)
    {
        return "
            {label}
            <div class='input-group'>
                {input}
                <span class='input-group-btn'>
                    <button class='btn btn-default' type='button'><span class='glyphicon {$glyphicon}' aria-hidden='true'></span></button>
                </span>
            </div>
            {error}
            {hint}";
    }

}
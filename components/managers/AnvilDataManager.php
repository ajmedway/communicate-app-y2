<?php

namespace app\components\managers;

use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\httpclient\Client;
use app\models\Company;
use app\models\Profile;
use app\modules\product\models\Product;
use app\modules\product\models\Supplier;
use app\modules\product\components\helpers\ProductData as PD;

/**
 * Class AnvilDataManager
 * 
 * @author Alec Pritchard <alec@r-evolution.co.uk>
 * @see https://testapi.custservice.co/swagger/ui/index
 */
class AnvilDataManager extends Component
{

    /**
     * @var string Anvil API Address Pseudo-Type
     * 
     * NOTE: Not an Anvil API Entity type, only for mapping billing address data
     * to Anvil API Address Type
     */
    public static $PSEUDO_ANVTYPE_ADDRESS_BILLING = 'PseudoAddressBilling';

    /**
     * @var string Anvil API Address Type
     */
    public static $ANVTYPE_ADDRESS = 'Address';

    /**
     * @var string Anvil API Charge Type
     */
    public static $ANVTYPE_CHARGE_ADHOC = 'ChargeAdHoc';

    /**
     * @var string Anvil API Charge Type
     */
    public static $ANVTYPE_CHARGE_CHARGEPRODUCT = 'ChargeChargeProduct';

    /**
     * @var string Anvil API Charge Product Type
     */
    public static $ANVTYPE_CHARGEPRODUCT = 'ChargeProduct';

    /**
     * @var string Anvil API Contact Type
     */
    public static $ANVTYPE_CONTACT = 'Contact';

    /**
     * @var string Anvil API Customer Type
     */
    public static $ANVTYPE_CUSTOMER = 'Customer';

    /**
     * @var string Anvil API Product (Category) Type
     */
    public static $ANVTYPE_PRODUCTCATEGORY = 'ProductCategory';

    /**
     * @var string Anvil API Supplier Type
     */
    public static $ANVTYPE_SUPPLIER = 'Supplier';

    /**
     * @var string Anvil API base url
     */
    public $baseUrl;

    /**
     * @var string Anvil API account username
     */
    public $username;

    /**
     * @var string Anvil API account password
     */
    public $password;

    /**
     * @var string Anvil API Key
     * @see https://swagger.io/docs/specification/authentication/basic-authentication/
     */
    protected $basicAuthToken;

    /**
     * @var yii\httpclient\Client instance
     */
    protected $client;

    /**
     * @var array Anvil Customer Address properties for POST, indexed by natively used field names (using billing address data)
     */
    public $PseudoAddressBillingProperties_POST;

    /**
     * @var array Anvil Customer Address properties for PATCH, indexed by natively used field names (using billing address data)
     */
    public $PseudoAddressBillingProperties_PATCH;

    /**
     * @var array Anvil Customer Address properties for POST, indexed by natively used field names
     */
    public $AddressProperties_POST;

    /**
     * @var array Anvil Customer Address properties for PATCH, indexed by natively used field names
     */
    public $AddressProperties_PATCH;

    /**
     * @var array Anvil Ad-Hoc Charge properties for POST, indexed by Product model field names
     */
    public $ChargeAdHocProperties_POST;

    /**
     * @var array Anvil Ad-Hoc Charge properties for PATCH, indexed by Product model field names
     */
    public $ChargeAdHocProperties_PATCH;

    /**
     * @var array Anvil Product-based Charge properties for POST, indexed by Product model field names
     */
    public $ChargeChargeProductProperties_POST;

    /**
     * @var array Anvil Product-based Charge properties for PATCH, indexed by Product model field names
     */
    public $ChargeChargeProductProperties_PATCH;

    /**
     * @var array Anvil Charge Product properties for POST, indexed by Product model field names
     */
    public $ChargeProductProperties_POST;

    /**
     * @var array Anvil Charge Product properties for PATCH, indexed by Product model field names
     */
    public $ChargeProductProperties_PATCH;

    /**
     * @var array Anvil Customer properties for POST, indexed by natively used field names
     */
    public $CustomerProperties_POST;

    /**
     * @var array Anvil Customer properties for PATCH, indexed by natively used field names
     */
    public $CustomerProperties_PATCH;

    /**
     * @var array Anvil Customer Contact properties for POST, indexed by Profile model field names
     */
    public $ContactProperties_POST;

    /**
     * @var array Anvil Customer Contact properties for PATCH, indexed by Profile model field names
     */
    public $ContactProperties_PATCH;

    /**
     * @var array Anvil Product Category properties, indexed by Supplier model field names
     */
    public $ProductCategoryProperties;

    /**
     * @var array Anvil Supplier properties, indexed by Supplier model field names
     */
    public $SupplierProperties;

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        if (empty($this->username)) {
            throw new InvalidConfigException('Anvil API username cannot be empty.');
        }
        if (empty($this->password)) {
            throw new InvalidConfigException('Anvil API password cannot be empty.');
        }

        parent::init();

        // initialise Anvil basic authentication token
        $this->basicAuthToken = $this->getBasicAuthToken();

        // create yii\httpclient\Client instance
        $this->client = $this->getHttpClientInstance();

        // initialise Anvil Customer Address common field mapping definitions (for mapping from billing address data)
        $this->PseudoAddressBillingProperties_POST = $this->PseudoAddressBillingProperties_PATCH = [
            'AddressDescription' => 'AddressDescription',
            'hs_company' => 'CompanyName',
            'SubPremises' => 'SubPremises',
            'billing_address_1' => 'Premises',
            'Number' => 'Number',
            'billing_address_2' => 'Street',
            'billing_address_3' => 'Locality',
            'DependentThoroughfare' => 'DependentThoroughfare',
            'billing_city' => 'Town',
            'billing_region' => 'Region',
            'billing_zip' => 'Postcode',
            'CountryID' => 'CountryID',
        ];

        // initialise Anvil Customer Address common field mapping definitions
        $this->AddressProperties_POST = $this->AddressProperties_PATCH = [
            'AddressDescription' => 'AddressDescription',
            'hs_company' => 'CompanyName',
            'SubPremises' => 'SubPremises',
            'hs_address_1' => 'Premises',
            'Number' => 'Number',
            'hs_address_2' => 'Street',
            'hs_address_3' => 'Locality',
            'DependentThoroughfare' => 'DependentThoroughfare',
            'hs_city' => 'Town',
            'hs_region' => 'Region',
            'hs_zip' => 'Postcode',
            'CountryID' => 'CountryID',
        ];

        // initialise Anvil Ad-Hoc Charge common field mapping definitions
        $this->ChargeAdHocProperties_POST = $this->ChargeAdHocProperties_PATCH = [
            'Description' => 'Description',
            'BillingFrequency' => 'BillingFrequency',
            'CostInitial' => 'CostInitial',
            'CostRecurring' => 'CostRecurring',
            'SupplierAccountID' => 'SupplierAccountID',
            'ProductID' => 'ProductID',
            'CurrencyID' => 'CurrencyID',
            'CountryID' => 'CountryID',
            'TaxRateID' => 'TaxRateID',
            'ObjectName' => 'ObjectName',
            'OtherInfo' => 'OtherInfo',
            'DateEnd' => 'DateEnd',
            'ChargeInitial' => 'ChargeInitial',
            'ChargeRecurring' => 'ChargeRecurring',
        ];
        // add method-specific elements
        $this->ChargeAdHocProperties_POST['DateStart'] = 'DateStart';

        // initialise Anvil Product-based Charge common field mapping definitions
        $this->ChargeChargeProductProperties_POST = $this->ChargeChargeProductProperties_PATCH = [
            'ObjectName' => 'ObjectName',
            'OtherInfo' => 'OtherInfo',
            'DateEnd' => 'DateEnd',
            'ChargeInitial' => 'ChargeInitial',
            'ChargeRecurring' => 'ChargeRecurring',
        ];
        // add method-specific elements
        $this->ChargeChargeProductProperties_POST['DateStart'] = 'DateStart';

        // initialise Anvil Charge Product common field mapping definitions
        $this->ChargeProductProperties_POST = $this->ChargeProductProperties_PATCH = [
            'Description' => 'Description',
            'BillingFrequency' => 'BillingFrequency',
            'CostInitial' => 'CostInitial',
            'CostRecurring' => 'CostRecurring',
            'ChargeInitial' => 'ChargeInitial',
            'ChargeRecurring' => 'ChargeRecurring',
            'SupplierAccountID' => 'SupplierAccountID',
            'ProductID' => 'ProductID',
            'CurrencyID' => 'CurrencyID',
            'CountryID' => 'CountryID',
            'TaxRateID' => 'TaxRateID',
        ];
        // add method-specific elements
        $this->ChargeProductProperties_POST['CompanyID'] = 'CompanyID';

        // initialise Anvil Customer common field mapping definitions
        $this->CustomerProperties_POST = $this->CustomerProperties_PATCH = [
            'hs_name' => 'Name',
            'CompanyName' => 'CompanyName',
            'PartnerID' => 'PartnerID',
            'CurrencyID' => 'CurrencyID',
            'CountryID' => 'CountryID',
            'LanguageID' => 'LanguageID',
            'hs_website' => 'Website',
            'CustomerTypeID' => 'CustomerTypeID',
            'CompanyNumber' => 'CompanyNumber',
            'VATref' => 'VATref',
            'EstimatedSpend' => 'EstimatedSpend',
            'DealerInits' => 'DealerInits',
            'ContractDuration' => 'ContractDuration',
            'PaymentTermsDays' => 'PaymentTermsDays',
            'IsCreditControlSuspended' => 'IsCreditControlSuspended',
            'IsOutputSuspended' => 'IsOutputSuspended',
            'NoEmail' => 'NoEmail',
            'MinimumSpend' => 'MinimumSpend',
            'ImportantNote' => 'ImportantNote',
            'PopupAlert' => 'PopupAlert',
            'ParentCustomerID' => 'ParentCustomerID',
            'IsParentable' => 'IsParentable',
            'BillToParent' => 'BillToParent',
            'Custom1' => 'Custom1',
            'Custom2' => 'Custom2',
            'Custom3' => 'Custom3',
            'Custom4' => 'Custom4',
            'Custom5' => 'Custom5',
            'Custom6' => 'Custom6',
            'Custom1name' => 'Custom1name',
            'Custom2name' => 'Custom2name',
            'Custom3name' => 'Custom3name',
            'Custom4name' => 'Custom4name',
            'Custom5name' => 'Custom5name',
            'Custom6name' => 'Custom6name',
            'NoBranding' => 'NoBranding',
            'CustomerReference' => 'CustomerReference',
            'NoUpsalesCalls' => 'NoUpsalesCalls',
            'Category' => 'Category',
        ];
        // add method-specific elements
        $this->CustomerProperties_POST['CompanyID'] = 'CompanyID';
        $this->CustomerProperties_PATCH['PrimaryContactID'] = 'PrimaryContactID';
        $this->CustomerProperties_PATCH['PrimaryAddressID'] = 'PrimaryAddressID';

        // initialise Anvil Customer Contact common field mapping definitions
        $this->ContactProperties_POST = $this->ContactProperties_PATCH = [
            'hs_lastname' => 'Surname',
            'hs_firstname' => 'FirstName',
            'hs_salutation' => 'Title',
            'hs_jobtitle' => 'JobPosition',
            'hs_phone' => 'DirectLine',
            'hs_mobile_phone' => 'MobilePhone',
            'hs_fax' => 'Fax',
            'public_email' => 'Email',
            'NoEmail' => 'NoEmail',
        ];

        // initialise Anvil Product Category common field mapping definitions
        $this->ProductCategoryProperties = [
            'ProductID' => 'ProductID',
            'ProductGroupID' => 'ProductGroupID',
            'Notes' => 'Notes',
        ];

        // initialise Anvil Supplier common field mapping definitions
        $this->SupplierProperties = [
            'ID' => 'ID',
            'Name' => 'Name',
        ];
    }

    /**
     * Get Anvil API basic authentication token
     * 
     * @return string Basic authentication token
     * 
     * @see https://swagger.io/docs/specification/authentication/basic-authentication/
     */
    protected function getBasicAuthToken()
    {
        return base64_encode("{$this->username}:{$this->password}");
    }

    /**
     * Get yii\httpclient\Client instance using CurlTransport, which sends HTTP
     * messages using [Client URL Library (cURL)](http://php.net/manual/en/book.curl.php)
     * 
     * @return \yii\httpclient\Client
     */
    protected function getHttpClientInstance()
    {
        return new Client([
            'transport' => 'yii\httpclient\CurlTransport',
            'baseUrl' => $this->baseUrl,
            'requestConfig' => [
                'headers' => [
                    'Authorization' => "Basic {$this->basicAuthToken}",
                ],
                'format' => Client::FORMAT_JSON,
            ],
            'responseConfig' => [
                'format' => Client::FORMAT_JSON,
            ],
        ]);
    }

    /**
     * Process request and return response, handle error/warning logging
     * 
     * @param yii\httpclient\Request $request
     * @param bool $logWarnings if non-OK response, log the yii\httpclient\Response object
     * @return mixed false on fail or response data
     */
    protected function processRequest(\yii\httpclient\Request $request, $logWarnings = false)
    {
        if (!($request instanceof \yii\httpclient\Request)) {
            Yii::error('$request is not an instance of yii\httpclient\Request', __METHOD__);
            return false;
        }

        try {
            $response = $request->send();

            if ($response->getIsOk()) {
                // DELETE endpoints return 204 status code with empty body on
                // success, so return a bool true in this case
                if ((int) $response->getStatusCode() === 204) {
                    return true;
                }

                return $response->getData();
            } else if ($logWarnings) {
                Yii::warning([
                    'Response Status Code' => $response->getStatusCode(),
                    'Response Content' => $response->getContent(),
                    ], __METHOD__);
            }
        } catch (Exception $e) {
            Yii::error($e->getMessage(), __METHOD__);
        }

        return false;
    }

    /**
     * Map native data array or model fields to Anvil properties
     *
     * @param array|object $data Array or native Model/ActiveRecord instance
     * @param string $anvType Anvil object type
     * @param string $verb (optional) the request method used
     * @param array $extraFields (optional) any extra fields to merge into the final properties array
     * @return bool
     */
    public function mapPropertiesNativeToAnvil($data, $anvType, $verb = '', $extraFields = [])
    {
        $properties = [];

        // formulate identifier signature for class field mappings property
        $identifier = "{$anvType}Properties" . ($verb ? "_{$verb}" : '');
        $anvPropertyMap = ArrayHelper::getValue($this, $identifier);

        if (!empty($anvPropertyMap) && !empty($data)) {

            // if we are dealing with a model/ActiveRecord instance, extract array of model attributes
            if ($data instanceof \yii\base\Model) {
                $data = $data->getAttributes();
            }

            if (!empty($extraFields)) {
                $data = ArrayHelper::merge($data, $extraFields);
            }

            foreach ($data as $property => $value) {
                $anvProperty = ArrayHelper::getValue($anvPropertyMap, $property);

                if ($anvProperty) {
                    // use property mapping if possible
                    $properties[$anvProperty] = $value;
                } else if (ArrayHelper::getValue($extraFields, $property)) {
                    // else if in the extra fields array, use the key from that array
                    $properties[$property] = $value;
                }
            }
        }

        return $properties;
    }

    /**
     * Map Anvil properties to native model fields
     *
     * @param array|object $data Array or native Model/ActiveRecord instance
     * @param string $anvType Anvil object type
     * @param string $verb (optional) the request method used
     * @param array $extraFields (optional) any extra fields to merge into the final properties array
     * @return bool
     */
    public function mapPropertiesAnvilToNative($data, $anvType, $verb = '', $extraFields = [])
    {
        $properties = [];

        // formulate identifier signature for class field mappings property
        $identifier = "{$anvType}Properties" . ($verb ? "_{$verb}" : '');
        $anvPropertyMap = ArrayHelper::getValue($this, $identifier);

        if (!empty($anvPropertyMap) && !empty($data)) {

            // flip array values so we are mapping the anvil field names to the native ones
            $anvPropertyMap = array_flip($anvPropertyMap);

            // if we are dealing with a model/ActiveRecord instance, extract array of model attributes
            if ($data instanceof \yii\base\Model) {
                $data = $data->getAttributes();
            }

            if (!empty($extraFields)) {
                $data = ArrayHelper::merge($data, $extraFields);
            }

            foreach ($data as $property => $value) {
                $anvProperty = ArrayHelper::getValue($anvPropertyMap, $property);

                if ($anvProperty) {
                    // use property mapping if possible
                    $properties[$anvProperty] = $value;
                } else if (ArrayHelper::getValue($extraFields, $property)) {
                    // else if in the extra fields array, use the key from that array
                    $properties[$property] = $value;
                }
            }
        }

        return $properties;
    }

    /**
     * Get internal constant, used to map correct local data to Anvil for address type
     * 
     * @param array|app\models\Profile $address model instance or custom array of address data
     * @return string self::$PSEUDO_ANVTYPE_ADDRESS_BILLING | self::$ANVTYPE_ADDRESS
     */
    public static function getAddressType($address)
    {
        if (($address instanceof \app\models\Profile) && $address->getBillingAddressPrecedence()) {
            $anvType = self::$PSEUDO_ANVTYPE_ADDRESS_BILLING;
        } else {
            $anvType = self::$ANVTYPE_ADDRESS;
        }
        
        return $anvType;
    }

    /**
     * The Anvil API serializer uses the Newtonsoft JSON library, which uses ISO 8601
     * 
     * @see https://www.newtonsoft.com/json/help/html/DatesInJSON.htm
     * 
     * @param string $date MySQL-formatted date string
     * @return string Anvil-friendly Atom datetime format, e.g. 2018-06-10T00:00:00+01:00
     */
    public static function formatAnvilDatetime($date)
    {
        $formatter = \Yii::$app->formatter;
        $dateTime = $formatter->asDatetime($date, 'php:' . \DateTime::ATOM);
        //$dateTime = $formatter->asDatetime($date, "php:Y-m-d\TH:i:s.000\Z");

        return $dateTime;
    }

    /**
     * Add total of calendar months to start date to calculate and format end date
     * 
     * @param string $date MySQL-formatted date string
     * @return string Anvil-friendly Atom datetime format, e.g. 2018-06-10T00:00:00+01:00
     */
    public static function calculateEndAnvilDatetime($startDate, $durationMonths)
    {
        $start = new \DateTime($startDate, new \DateTimeZone(\Yii::$app->formatter->timeZone));
        $dateEndRaw = clone $start;
        $dateEndRaw->add(new \DateInterval("P{$durationMonths}M"));
        $dateTimeEnd = self::formatAnvilDatetime($dateEndRaw->format('Y-m-d'));

        return $dateTimeEnd;
    }

    /**
     * API test method - returns current date and time
     * 
     * GET /api/v1/Test/Ping
     * 
     * @return mixed false on fail or response data
     */
    public function test()
    {
        $request = $this->client->createRequest()
            ->setMethod('GET')
            ->setUrl('Test/Ping')
            ->setOptions([
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_TIMEOUT => 30,
        ]);

        return $this->processRequest($request);
    }

    /**
     * Returns the single ChargeProduct
     * 
     * GET /api/v1/ChargeProducts/{CompanyID}/{ChargeProductID}
     * 
     * @param int $CompanyID
     * @param int $ChargeProductID
     * @return mixed false on fail or response data
     */
    public function getChargeProduct($CompanyID, $ChargeProductID)
    {
        if (!is_numeric($CompanyID) || !is_numeric($ChargeProductID)) {
            return false;
        }

        $request = $this->client->createRequest()
            ->setMethod('GET')
            ->setUrl("ChargeProducts/{$CompanyID}/{$ChargeProductID}")
            ->setOptions([
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_TIMEOUT => 30,
        ]);

        return $this->processRequest($request);
    }

    /**
     * Returns the list of ChargeProducts for given company (includes products
     * available to all companies)
     * 
     * GET /api/v1/ChargeProducts/{CompanyID}
     * 
     * @param int $CompanyID
     * @return mixed false on fail or response data
     */
    public function getChargeProducts($CompanyID)
    {
        if (!is_numeric($CompanyID)) {
            return false;
        }

        $request = $this->client->createRequest()
            ->setMethod('GET')
            ->setUrl("ChargeProducts/{$CompanyID}")
            ->setOptions([
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_TIMEOUT => 30,
        ]);

        return $this->processRequest($request);
    }

    /**
     * Create a new charge product
     * 
     * POST /api/v1/ChargeProduct
     * 
     * @param app\modules\product\models\Product $product
     * @return mixed false on fail or response data
     */
    public function createChargeProduct($product)
    {
        if (empty($product)) {
            return false;
        }

        $properties = $this->mapPropertiesNativeToAnvil($product, self::$ANVTYPE_CHARGEPRODUCT, 'POST', [
            'CompanyID' => Company::getAnvilCompanyId(),
        ]);
        if (empty($properties)) {
            return false;
        }

        $request = $this->client->createRequest()
            ->setMethod('POST')
            ->setUrl('ChargeProduct')
            ->setData($properties)
            ->setOptions([
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_TIMEOUT => 30,
        ]);

        return $this->processRequest($request, true);
    }

    /**
     * Update a charge product
     * 
     * PATCH /api/v1/ChargeProducts/{ChargeProductID}
     * 
     * @param int $ChargeProductID
     * @param app\modules\product\models\Product $product
     * @return mixed false on fail or response data
     */
    public function updateChargeProduct($ChargeProductID, $product)
    {
        if (empty($ChargeProductID) || empty($product)) {
            return false;
        }

        $properties = $this->mapPropertiesNativeToAnvil($product, self::$ANVTYPE_CHARGEPRODUCT, 'PATCH');
        if (empty($properties)) {
            return false;
        }

        $request = $this->client->createRequest()
            ->setMethod('PATCH')
            ->setUrl("ChargeProducts/{$ChargeProductID}")
            ->setData($properties)
            ->setOptions([
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_TIMEOUT => 30,
        ]);

        return $this->processRequest($request, true);
    }

    /**
     * Delete the given charge product
     * 
     * DELETE /api/v1/ChargeProducts/{ChargeProductID}
     * 
     * @param int $ChargeProductID
     * @return bool whether deleted succeessfully
     */
    public function deleteChargeProduct($ChargeProductID)
    {
        if (empty($ChargeProductID)) {
            return false;
        }

        $request = $this->client->createRequest()
            ->setMethod('DELETE')
            ->setUrl("ChargeProducts/{$ChargeProductID}")
            ->setOptions([
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_TIMEOUT => 30,
        ]);

        return $this->processRequest($request, true);
    }

    /**
     * Search for customers containing the given text in the name
     * 
     * GET /api/v1/Customers/Search?term={text}
     * 
     * Response contains a basic list of customers objects in following format:
     * 
     * [
     *   {
     *     "CustomerID": 264,
     *     "Name": "Test Company 1"
     *   }
     * ]
     * 
     * @param string $text
     * @return mixed false on fail or response data
     */
    public function getCustomersSearch($text)
    {
        if (empty($text) || !is_string($text)) {
            return false;
        }

        $request = $this->client->createRequest()
            ->setMethod('GET')
            ->setUrl("Customers/Search?term={$text}")
            ->setOptions([
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_TIMEOUT => 30,
        ]);

        return $this->processRequest($request);
    }

    /**
     * Get a single customer by ID
     * 
     * GET /api/v1/Customers/{CustomerID}
     * 
     * @param int $CustomerID
     * @return mixed false on fail or response data
     */
    public function getCustomer($CustomerID)
    {
        if (!is_numeric($CustomerID)) {
            return false;
        }

        $request = $this->client->createRequest()
            ->setMethod('GET')
            ->setUrl("Customers/{$CustomerID}")
            ->setOptions([
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_TIMEOUT => 30,
        ]);

        return $this->processRequest($request);
    }

    /**
     * Create a new customer
     * 
     * POST /api/v1/Customers
     * 
     * @param array $customer custom array of customer data
     * @return mixed false on fail or response data
     */
    public function createCustomer($customer)
    {
        if (empty($customer)) {
            return false;
        }

        $properties = $this->mapPropertiesNativeToAnvil($customer, self::$ANVTYPE_CUSTOMER, 'POST', [
            'CompanyID' => Company::getAnvilCompanyId(),
            'CompanyName' => $customer['hs_name'],
            'PartnerID' => 37, // where should this live? TBC
            'CurrencyID' => 'GBP',
            'CountryID' => 'GB',
            'LanguageID' => 'EN',
        ]);
        if (empty($properties)) {
            return false;
        }

        $request = $this->client->createRequest()
            ->setMethod('POST')
            ->setUrl('Customers')
            ->setData($properties)
            ->setOptions([
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_TIMEOUT => 30,
        ]);

        return $this->processRequest($request, true);
    }

    /**
     * Update a customer
     * 
     * PATCH /api/v1/Customers/{CustomerID}
     * 
     * @param int $CustomerID
     * @param array $customer custom array of customer data
     * @return mixed false on fail or response data
     */
    public function updateCustomer($CustomerID, $customer)
    {
        if (empty($CustomerID) || empty($customer)) {
            return false;
        }

        $properties = $this->mapPropertiesNativeToAnvil($customer, self::$ANVTYPE_CUSTOMER, 'PATCH', [
            'PartnerID' => 37, // where should this live? TBC
            'CurrencyID' => 'GBP',
            'CountryID' => 'GB',
            'LanguageID' => 'EN',
        ]);
        if (empty($properties)) {
            return false;
        }

        $request = $this->client->createRequest()
            ->setMethod('PATCH')
            ->setUrl("Customers/{$CustomerID}")
            ->setData($properties)
            ->setOptions([
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_TIMEOUT => 30,
        ]);

        return $this->processRequest($request, true);
    }

    /**
     * Search for contacts containing the given text in either the first name,
     * last name or email fields.
     * 
     * GET /api/v1/Customers/Contacts/Search?term={text}
     * 
     * Response contains full contact object.
     * 
     * @param string $text
     * @return mixed false on fail or response data
     */
    public function getContactsSearch($text)
    {
        if (empty($text) || !is_string($text)) {
            return false;
        }

        $request = $this->client->createRequest()
            ->setMethod('GET')
            ->setUrl("Customers/Contacts/Search?term={$text}")
            ->setOptions([
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_TIMEOUT => 30,
        ]);

        return $this->processRequest($request);
    }

    /**
     * Get all contacts for customer
     * 
     * GET /api/v1/Customers/{CustomerID}/Contacts
     * 
     * @param int $CustomerID
     * @return mixed false on fail or response data
     */
    public function getContacts($CustomerID)
    {
        if (!is_numeric($CustomerID)) {
            return false;
        }

        $request = $this->client->createRequest()
            ->setMethod('GET')
            ->setUrl("Customers/{$CustomerID}/Contacts")
            ->setOptions([
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_TIMEOUT => 30,
        ]);

        return $this->processRequest($request);
    }

    /**
     * Get single contact
     * 
     * GET /api/v1/Customers/{CustomerID}/Contacts/{ContactID}
     * 
     * @param int $CustomerID
     * @param int $ContactID
     * @return mixed false on fail or response data
     */
    public function getContact($CustomerID, $ContactID)
    {
        if (!is_numeric($CustomerID) || !is_numeric($ContactID)) {
            return false;
        }

        $request = $this->client->createRequest()
            ->setMethod('GET')
            ->setUrl("Customers/{$CustomerID}/Contacts/{$ContactID}")
            ->setOptions([
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_TIMEOUT => 30,
        ]);

        return $this->processRequest($request);
    }

    /**
     * Create a new contact
     * 
     * POST /api/v1/Customers/{CustomerID}/Contacts
     * 
     * @param int $CustomerID
     * @param app\models\Profile $profile
     * @return mixed false on fail or response data
     */
    public function createContact($CustomerID, $profile)
    {
        if (!is_numeric($CustomerID) || empty($profile)) {
            return false;
        }

        $properties = $this->mapPropertiesNativeToAnvil($profile, self::$ANVTYPE_CONTACT, 'POST', [
            'NoEmail' => true,
        ]);
        if (empty($properties)) {
            return false;
        }

        $request = $this->client->createRequest()
            ->setMethod('POST')
            ->setUrl("Customers/{$CustomerID}/Contacts")
            ->setData($properties)
            ->setOptions([
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_TIMEOUT => 30,
        ]);

        return $this->processRequest($request, true);
    }

    /**
     * Update a contact
     * 
     * PATCH /api/v1/Customers/{CustomerID}/Contacts/{ContactID}
     * 
     * @param int $CustomerID
     * @param int $ContactID
     * @param app\models\Profile $profile
     * @return mixed false on fail or response data
     */
    public function updateContact($CustomerID, $ContactID, $profile)
    {
        if (!is_numeric($CustomerID) || !is_numeric($ContactID) || empty($profile)) {
            return false;
        }

        $properties = $this->mapPropertiesNativeToAnvil($profile, self::$ANVTYPE_CONTACT, 'PATCH');
        if (empty($properties)) {
            return false;
        }

        $request = $this->client->createRequest()
            ->setMethod('PATCH')
            ->setUrl("Customers/{$CustomerID}/Contacts/{$ContactID}")
            ->setData($properties)
            ->setOptions([
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_TIMEOUT => 30,
        ]);

        return $this->processRequest($request, true);
    }

    /**
     * Get all addresses for customer
     * 
     * GET /api/v1/Customers/{CustomerID}/Addresses
     * 
     * @param int $CustomerID
     * @return mixed false on fail or response data
     */
    public function getAddresses($CustomerID)
    {
        if (!is_numeric($CustomerID)) {
            return false;
        }

        $request = $this->client->createRequest()
            ->setMethod('GET')
            ->setUrl("Customers/{$CustomerID}/Addresses")
            ->setOptions([
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_TIMEOUT => 30,
        ]);

        return $this->processRequest($request);
    }

    /**
     * Get single address
     * 
     * GET /api/v1/Customers/{CustomerID}/Addresses/{AddressID}
     * 
     * @param int $CustomerID
     * @param int $AddressID
     * @return mixed false on fail or response data
     */
    public function getAddress($CustomerID, $AddressID)
    {
        if (!is_numeric($CustomerID) || !is_numeric($AddressID)) {
            return false;
        }

        $request = $this->client->createRequest()
            ->setMethod('GET')
            ->setUrl("Customers/{$CustomerID}/Addresses/{$AddressID}")
            ->setOptions([
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_TIMEOUT => 30,
        ]);

        return $this->processRequest($request);
    }
    
    /**
     * Create an address
     * 
     * POST /api/v1/Customers/{CustomerID}/Addresses
     * 
     * @param int $CustomerID
     * @param array|object $address model instance or custom array of address data
     * @return mixed false on fail or response data
     */
    public function createAddress($CustomerID, $address)
    {
        if (!is_numeric($CustomerID) || empty($address)) {
            return false;
        }

        $properties = $this->mapPropertiesNativeToAnvil($address, self::getAddressType($address), 'POST', [
            'AddressDescription' => 'Communicate Quote Builder Address',
            'CountryID' => 'GB',
        ]);
        if (empty($properties)) {
            return false;
        }

        $request = $this->client->createRequest()
            ->setMethod('POST')
            ->setUrl("Customers/{$CustomerID}/Addresses")
            ->setData($properties)
            ->setOptions([
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_TIMEOUT => 30,
        ]);

        return $this->processRequest($request, true);
    }

    /**
     * Update an address
     * 
     * PATCH /api/v1/Customers/{CustomerID}/Addresses/{AddressID}
     * 
     * @param int $CustomerID
     * @param int $AddressID
     * @param array|object $address model instance or custom array of address data
     * @return mixed false on fail or response data
     */
    public function updateAddress($CustomerID, $AddressID, $address)
    {
        if (!is_numeric($CustomerID) || !is_numeric($AddressID) || empty($address)) {
            return false;
        }

        $properties = $this->mapPropertiesNativeToAnvil($address, self::getAddressType($address), 'PATCH', [
            'AddressDescription' => 'Communicate Quote Builder Address',
            'CountryID' => 'GB',
        ]);
        if (empty($properties)) {
            return false;
        }

        $request = $this->client->createRequest()
            ->setMethod('PATCH')
            ->setUrl("Customers/{$CustomerID}/Addresses/{$AddressID}")
            ->setData($properties)
            ->setOptions([
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_TIMEOUT => 30,
        ]);

        return $this->processRequest($request, true);
    }

    /**
     * Get all charges for customer
     * 
     * GET /api/v1/Customers/{CustomerID}/Charges
     * 
     * @param int $CustomerID
     * @return mixed false on fail or response data
     */
    public function getCharges($CustomerID)
    {
        if (!is_numeric($CustomerID)) {
            return false;
        }

        $request = $this->client->createRequest()
            ->setMethod('GET')
            ->setUrl("Customers/{$CustomerID}/Charges")
            ->setOptions([
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_TIMEOUT => 30,
        ]);

        return $this->processRequest($request);
    }

    /**
     * Get single charge
     * 
     * GET /api/v1/Customers/{CustomerID}/Charges/{ChargeID}
     * 
     * @param int $CustomerID
     * @param int $ChargeID
     * @return mixed false on fail or response data
     */
    public function getCharge($CustomerID, $ChargeID)
    {
        if (!is_numeric($CustomerID) || !is_numeric($ChargeID)) {
            return false;
        }

        $request = $this->client->createRequest()
            ->setMethod('GET')
            ->setUrl("Customers/{$CustomerID}/Charges/{$ChargeID}")
            ->setOptions([
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_TIMEOUT => 30,
        ]);

        return $this->processRequest($request);
    }

    /**
     * Create a new ad-hoc charge
     * 
     * POST /api/v1/Customers/{CustomerID}/Charges/AdHoc
     * 
     * @param int $CustomerID
     * @param array $charge custom array of ad-hoc charge data
     * @return mixed false on fail or response data
     */
    public function createChargeAdHoc($CustomerID, $charge)
    {
        if (!is_numeric($CustomerID) || empty($charge)) {
            return false;
        }

        $properties = $this->mapPropertiesNativeToAnvil($charge, self::$ANVTYPE_CHARGE_ADHOC, 'POST', [
            'CountryID' => 'GB',
        ]);
        if (empty($properties)) {
            return false;
        }

        $request = $this->client->createRequest()
            ->setMethod('POST')
            ->setUrl("Customers/{$CustomerID}/Charges/AdHoc")
            ->setData($properties)
            ->setOptions([
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_TIMEOUT => 30,
        ]);

        return $this->processRequest($request, true);
    }

    /**
     * Create a new product-based charge
     * 
     * POST /api/v1/Customers/{CustomerID}/Charges/Product/{ChargeProductID}
     * 
     * @param int $CustomerID
     * @param int $ChargeProductID
     * @param array $charge custom array of product-based charge data
     * @return mixed false on fail or response data
     */
    public function createChargeChargeProduct($CustomerID, $ChargeProductID, $charge)
    {
        if (!is_numeric($CustomerID) || !is_numeric($ChargeProductID) || empty($charge)) {
            return false;
        }

        $properties = $this->mapPropertiesNativeToAnvil($charge, self::$ANVTYPE_CHARGE_CHARGEPRODUCT, 'POST', [
            'CountryID' => 'GB',
        ]);
        if (empty($properties)) {
            return false;
        }

        $request = $this->client->createRequest()
            ->setMethod('POST')
            ->setUrl("Customers/{$CustomerID}/Charges/Product/{$ChargeProductID}")
            ->setData($properties)
            ->setOptions([
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_TIMEOUT => 30,
        ]);

        return $this->processRequest($request, true);
    }

    /**
     * Update an ad-hoc charge
     * 
     * PATCH /api/v1/Customers/{CustomerID}/Charges/AdHoc/{ChargeID}
     * 
     * @param int $CustomerID
     * @param int $ChargeID
     * @param array $charge custom array of ad-hoc charge data
     * @return mixed false on fail or response data
     */
    public function updateChargeAdHoc($CustomerID, $ChargeID, $charge)
    {
        if (!is_numeric($CustomerID) || !is_numeric($ChargeID) || empty($charge)) {
            return false;
        }

        $properties = $this->mapPropertiesNativeToAnvil($charge, self::$ANVTYPE_CHARGE_ADHOC, 'PATCH', ['CountryID' => 'GB']);
        if (empty($properties)) {
            return false;
        }

        $request = $this->client->createRequest()
            ->setMethod('PATCH')
            ->setUrl("Customers/{$CustomerID}/Charges/AdHoc/{$ChargeID}")
            ->setData($properties)
            ->setOptions([
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_TIMEOUT => 30,
        ]);

        return $this->processRequest($request, true);
    }

    /**
     * Update a new product-based charge
     * 
     * PATCH /api/v1/Customers/{CustomerID}/Charges/Product/{ChargeID}
     * 
     * @param int $CustomerID
     * @param int $ChargeID
     * @param array $charge custom array of product-based charge data
     * @return mixed false on fail or response data
     */
    public function updateChargeChargeProduct($CustomerID, $ChargeID, $charge)
    {
        if (!is_numeric($CustomerID) || !is_numeric($ChargeID) || empty($charge)) {
            return false;
        }

        $properties = $this->mapPropertiesNativeToAnvil($charge, self::$ANVTYPE_CHARGE_CHARGEPRODUCT, 'PATCH', ['CountryID' => 'GB']);
        if (empty($properties)) {
            return false;
        }

        $request = $this->client->createRequest()
            ->setMethod('PATCH')
            ->setUrl("Customers/{$CustomerID}/Charges/Product/{$ChargeID}")
            ->setData($properties)
            ->setOptions([
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_TIMEOUT => 30,
        ]);

        return $this->processRequest($request, true);
    }

    /**
     * Delete the given charge
     * 
     * DELETE /api/v1/Customers/{CustomerID}/Charges/{ChargeID}
     * 
     * @param int $CustomerID
     * @param int $ChargeID
     * @return bool whether deleted succeessfully
     */
    public function deleteCharge($CustomerID, $ChargeID)
    {
        if (!is_numeric($CustomerID) || !is_numeric($ChargeID)) {
            return false;
        }

        $request = $this->client->createRequest()
            ->setMethod('DELETE')
            ->setUrl("Customers/{$CustomerID}/Charges/{$ChargeID}")
            ->setOptions([
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_TIMEOUT => 30,
        ]);

        return $this->processRequest($request, true);
    }

    /**
     * Returns a list of products (categories)
     * 
     * GET /api/v1/StaticData/Products
     * 
     * @return mixed false on fail or response data
     */
    public function getProductCategories()
    {
        $request = $this->client->createRequest()
            ->setMethod('GET')
            ->setUrl('StaticData/Products')
            ->setOptions([
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_TIMEOUT => 30,
        ]);

        return $this->processRequest($request);
    }

    /**
     * Returns the list of SupplierAccounts for given company
     * 
     * GET /api/v1/StaticData/SupplierAccounts/{CompanyID}
     * 
     * @param int $CompanyID
     * @return mixed false on fail or response data
     */
    public function getSupplierAccounts($CompanyID)
    {
        if (!is_numeric($CompanyID)) {
            return false;
        }

        $request = $this->client->createRequest()
            ->setMethod('GET')
            ->setUrl("StaticData/SupplierAccounts/{$CompanyID}")
            ->setOptions([
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_TIMEOUT => 30,
        ]);

        return $this->processRequest($request);
    }

    /**
     * Create or update Anvil Customer, Contact and Address data from profile
     * 
     * Notes: I ran out of time to get this method broken out into more discrete
     * logical steps. It can be improved upon, but handles the API transactions
     * and local data changes well enough, with good error logging.
     * 
     * @todo Better handling of creating/updating customer seperately first of
     * all, then child contact, then child address.
     * @param app\models\Profile $profile
     * @return mixed
     */
    public function createUpdateAnvilCustomerRecords(\app\models\Profile $profile)
    {
        if (empty($profile)) {
            Yii::error(['message' => 'Empty profile came through to method', '$profile' => $profile], __METHOD__);
            return false;
        }

        // check if the current user profile is associated with an Anvil contact
        if (($contact = $this->getContact($profile->anv_customer_id, $profile->anv_contact_id)) !== false) {

            // linked contact found in Anvil! just update the Anvil records with the current customer/company data (see base of this method)
        } else if (($contacts = $this->getContactsSearch($profile->public_email)) !== false && count($contacts) === 1) {

            // single unlinked contact found in Anvil! update profile and company with Anvil id's
            $contact = $contacts[0];
            $profile->anv_contact_id = $contact['ContactID'];
            $profile->anv_customer_id = $profile->company->anv_customer_id = $contact['CustomerID'];
        } else {

            // no contact found... create customer and contact records
            if (($customerId = $this->createCustomer($profile->company)) !== false) {
                $profile->anv_customer_id = $profile->company->anv_customer_id = $customerId;

                if (($contact = $this->createContact($profile->anv_customer_id, $profile)) !== false) {
                    $profile->anv_contact_id = $profile->company->PrimaryContactID = $contact['ContactID'];
                } else {
                    Yii::error(['message' => "Unable to create Contact in Anvil for newly created Customer #{$customerId}", '$profile' => $profile], __METHOD__);
                    return false;
                }
            } else {
                Yii::error(['message' => 'Unable to create Customer in Anvil', '$profile' => $profile], __METHOD__);
                return false;
            }
        }

        // ensure we have an existing company relation
        if ($profile->company === null) {
            Yii::error(['message' => 'There is no company relation for this profile', '$profile' => $profile], __METHOD__);
            return false;
        }

        /**
         * Finally, create or update the address.
         * Notes:
         * - We are only supporting sync with a single Anvil Address.
         * - Anvil Addresses belong to the Customer (i.e. Company), not the
         * Contact, thus we sync the address ID in the PrimaryAddressID
         * field in the local Company record and remote Anvil Customer record.
         */
        if (!empty($profile->company->PrimaryAddressID)) {
            if (($address = $this->getAddress($profile->anv_customer_id, $profile->company->PrimaryAddressID)) !== false) {
                if (($address = $this->updateAddress($profile->anv_customer_id, $profile->company->PrimaryAddressID, $profile)) !== false) {
                    $companyPrimaryAddressUpdated = true;
                } else {
                    Yii::error(['message' => "Unable to update Address #{$profile->company->PrimaryAddressID} in Anvil", '$profile' => $profile], __METHOD__);
                    return false;
                }
            } else {
                Yii::error(['message' => "Address #{$profile->company->PrimaryAddressID} does not exist in Anvil", '$profile' => $profile], __METHOD__);
            }
        }
        
        /**
         * Create a new address in all cases except where we have an Anvil
         * address that has been successfully found and updated in Anvil.
         */
        if (empty($companyPrimaryAddressUpdated)) {
            if (($address = $this->createAddress($profile->anv_customer_id, $profile)) !== false) {
                $profile->company->PrimaryAddressID = !empty($address['AddressID']) ? $address['AddressID'] : null;
            } else {
                Yii::error(['message' => 'Unable to create Address in Anvil', '$profile' => $profile], __METHOD__);
                return false;
            }
        }

        // save changes to native profile and company records
        $profile->save(false);
        $profile->company->save(false);

        // repopulate profile ActiveRecord instance with latest data
        $profile->refresh();

        // update the Anvil records with the current customer/company data
        $contactData = $this->updateContact($profile->anv_customer_id, $profile->anv_contact_id, $profile);
        if (empty($contactData)) {
            Yii::error(['message' => "Unable to update Contact #{$profile->anv_contact_id} in Anvil", '$profile' => $profile], __METHOD__);
            return false;
        }
        $customerData = $this->updateCustomer($profile->anv_customer_id, $profile->company);
        if (empty($customerData)) {
            Yii::error(['message' => "Unable to update Customer #{$profile->anv_customer_id} in Anvil", '$profile' => $profile], __METHOD__);
            return false;
        }

        return $profile;
    }

    /**
     * Generate charges in Anvil
     * 
     * @todo check if the customer already has a live quote, then do we delete charges or update? TBC
     * @param app\modules\quote\models\Quote $quote
     * @return array Created charges data returned from Anvil
     */
    public function generateCharges(\app\modules\quote\models\Quote $quote)
    {
        $Charges = [];

        // iterate products and create the charges
        foreach ($quote->quote_data as $productId => $quoteProduct) {
            if (empty($quoteProduct[PD::$FIELD_QTY]) || empty($quoteProduct[PD::$FIELD_DUR])) {
                continue;
            }

            // calculate and format Anvil-friendly start and end datetime values
            $dateTimeStart = self::formatAnvilDatetime($quoteProduct[PD::$FIELD_STR]);
            $dateTimeEnd = self::calculateEndAnvilDatetime($quoteProduct[PD::$FIELD_STR], $quoteProduct[PD::$FIELD_DUR]);

            // handle qty > 1, loop through for 1 charge per
            for ($i = 1; $i <= $quoteProduct[PD::$FIELD_QTY]; $i++) {

                // get Product instance and prices calculated in the Quote instance
                $product = Product::findOne($productId);
                $quoteProductPrices = $quote->calculateQuoteProductPrices($product, $quoteProduct, true);

                // generate internal reference for the charge
                $internalChargeRef = 'c:' . $quote->profile->company->id . '_u:' . $quote->profile->user_id . '_q:' . $quote->id . '_p:' . $product->id . '_i:' . $i;

                $chargeData = ArrayHelper::merge($product->getAttributes(), [
                        'ObjectName' => "Product-based charge: {$product->Description}",
                        'OtherInfo' => $internalChargeRef,
                        'DateStart' => $dateTimeStart,
                        'DateEnd' => $dateTimeEnd,
                        // overwrite Charge prices based on business logic in the Quote instance,
                        // e.g. incorporating any contract length-based discounts
                        'ChargeInitial' => $quoteProductPrices->ChargeInitial,
                        'ChargeRecurring' => $quoteProductPrices->ChargeRecurring,
                ]);

                /**
                 * It might be necessary to make a call to the getChargeProduct
                 * endpoint at Anvil to ensure that the $ChargeProductID exists,
                 * has not been deleted etc. so that the issue can be reported
                 * to the user/admins. Might be necessary to delete all the
                 * charges that have been created in this session if one fails.
                 */
                if (($ChargeChargeProduct = $this->createChargeChargeProduct($quote->profile->anv_customer_id, $product->ChargeProductID, $chargeData)) !== false && !empty($ChargeChargeProduct['ChargeID'])) {
                    $Charges[] = $ChargeChargeProduct;
                } else {
                    Yii::error(['message' => 'Unable to create Product-based Charge in Anvil', '$chargeData' => $chargeData], __METHOD__);
                }
            }
        }

        return $Charges;
    }

}

<?php

namespace app\components\managers;

use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\httpclient\Client;
use app\models\Company;
use app\models\Profile;
use app\models\SignnowApiToken;
use app\modules\product\models\Product;
use app\modules\product\models\Supplier;
use app\modules\product\components\helpers\ProductData as PD;

/**
 * Class SignNowManager
 * 
 * @author Alec Pritchard <alec@r-evolution.co.uk>
 * @see https://university.signnow.com/signnow-api-documentation/
 * @see https://help.signnow.com/v1.0/reference
 * @see https://help.signnow.com/docs/create-a-signing-session-invite-with-fields
 */
class SignNowManager extends Component
{

    /**
     * @var string Main client account user key
     */
    private static $CLIENT = 'client';

    /**
     * @var string Sender user key
     */
    private static $SENDER = 'sender';

    /**
     * @var string Signer user key
     */
    private static $SIGNER = 'signer';
    
    /**
     * @var string SignNow API base url
     */
    public $baseUrl;

    /**
     * @var string SignNow API account clientId
     */
    public $clientId;

    /**
     * @var string SignNow API account clientSecret
     */
    public $clientSecret;

    /**
     * @var string SignNow API sender account email
     */
    public $senderEmail;

    /**
     * @var string SignNow API sender account password
     */
    public $senderPassword;

    /**
     * @var string SignNow API sender auth token
     */
    public $senderAuthToken;

    /**
     * @var string SignNow API signer account email
     */
    public $signerEmail;

    /**
     * @var string SignNow API signer account password
     */
    public $signerPassword;

    /**
     * @var string SignNow API signer auth token
     */
    public $signerAuthToken;

    /**
     * @var string SignNow API Key
     * @see https://help.signnow.com/docs/getting-started#section-security-and-access-control
     */
    protected $clientAuthToken;

    /**
     * @var yii\httpclient\Client instance
     */
    protected $client;

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        if (empty($this->clientId)) {
            throw new InvalidConfigException('SignNow API Client ID cannot be empty.');
        }
        if (empty($this->clientSecret)) {
            throw new InvalidConfigException('SignNow API Client Secret cannot be empty.');
        }

        parent::init();

        // initialise SignNow client authentication token
        $this->clientAuthToken = $this->getClientAuthToken();

        // create yii\httpclient\Client instance for client user
        $this->setHttpClientInstance(self::$CLIENT);

        // load the sender/signer authentication tokens from the database
        $this->loadUserAuthToken(self::$SENDER);
        $this->loadUserAuthToken(self::$SIGNER);
        
        // set the client instance back to client token user after loading other tokens
        $this->changeHttpClientInstanceUserAuthToken(self::$CLIENT);
    }

    /**
     * Get SignNow API client authentication token
     * 
     * @return string Client authentication token
     * 
     * @see https://swagger.io/docs/specification/authentication/bearer-authentication/
     */
    protected function getClientAuthToken()
    {
        return base64_encode("{$this->clientId}:{$this->clientSecret}");
    }

    /**
     * Load SignNow API authentication token from the database for a given user,
     * or create a new one if it has become invalid.
     * 
     * @param string $userKey
     * @return void
     */
    protected function loadUserAuthToken($userKey)
    {
        // get the username
        $username = $this->{"{$userKey}Email"};
        
        // get token from database for this user, if not present create one
        if (($token = SignnowApiToken::find()->byUsername($username)) === null) {
            $token = new SignnowApiToken();
            $token->username = $username;
        }
        
        // check if we have an access token
        if (!empty($token->access_token)) {
            // if we do, set it and set token in client instance
            $this->{"{$userKey}AuthToken"} = $token->access_token;
            $this->changeHttpClientInstanceUserAuthToken($userKey);

            // attempt to verify the token is still valid
            $tokenVerify = $this->verifyUserOAuth2Token($token->access_token);

            if ($tokenVerify === false || empty($tokenVerify['access_token'])) {
                // db token is invalid, set flag for it to be updated
                $refreshToken = true;
            }
        }
        
        // check if token not present or has expired
        if (empty($token->access_token) || !empty($refreshToken)) {
            // create/obtain new token
            $newToken = $this->createUserOAuth2Token([
                'username' => $username,
                'password' => $this->{"{$userKey}Password"},
                'grant_type' => 'password',
            ]);

            if ($newToken !== false && !empty($newToken['access_token'])) {
                // update token record in the database
                $token->setAttributes($newToken);
                $token->access_token = $this->{"{$userKey}AuthToken"} = $newToken['access_token'];
                $token->save();
            }
        }
    }

    /**
     * Get yii\httpclient\Client instance for specified user
     * 
     * @param string $userKey
     * @return void
     * 
     * @see https://swagger.io/docs/specification/authentication/bearer-authentication/
     */
    protected function setHttpClientInstance($userKey = null)
    {
        $this->client = $this->getHttpClientInstance($userKey);
    }

    /**
     * Change the token in yii\httpclient\Client instance for specified user
     * 
     * @param string $userKey
     * @return void
     * 
     * @see https://swagger.io/docs/specification/authentication/bearer-authentication/
     */
    protected function changeHttpClientInstanceUserAuthToken($userKey = null)
    {
        $token = $this->{"{$userKey}AuthToken"};
        $this->client->requestConfig['headers']['Authorization'] = "Bearer {$token}";
    }

    /**
     * Get yii\httpclient\Client instance using CurlTransport, which sends HTTP
     * messages using [Client URL Library (cURL)](http://php.net/manual/en/book.curl.php)
     * 
     * @param string $userKey
     * @return \yii\httpclient\Client
     */
    protected function getHttpClientInstance($userKey = null)
    {
        switch ($userKey) {
            case self::$SENDER:
                $token = $this->senderAuthToken;
                break;
            case self::$SIGNER:
                $token = $this->signerAuthToken;
                break;
            case self::$CLIENT:
            default:
                $token = $this->clientAuthToken;
                break;
        }
        
        return new Client([
            'transport' => 'yii\httpclient\CurlTransport',
            'baseUrl' => $this->baseUrl,
            'requestConfig' => [
                'headers' => [
                    'Authorization' => "Bearer {$this->clientAuthToken}",
                ],
                'format' => Client::FORMAT_JSON,
            ],
            'responseConfig' => [
                'format' => Client::FORMAT_JSON,
            ],
        ]);
    }

    /**
     * Process request and return response, handle error/warning logging
     * 
     * @param yii\httpclient\Request $request
     * @param bool $logWarnings if non-OK response, log the yii\httpclient\Response object
     * @return mixed false on fail or response data
     */
    protected function processRequest(\yii\httpclient\Request $request, $logWarnings = false)
    {
        if (!($request instanceof \yii\httpclient\Request)) {
            Yii::error('$request is not an instance of yii\httpclient\Request', __METHOD__);
            return false;
        }

        try {
            $response = $request->send();

            if ($response->getIsOk()) {
                // DELETE endpoints return 204 status code with empty body on
                // success, so return a bool true in this case
                if ((int) $response->getStatusCode() === 204) {
                    return true;
                }

                return $response->getData();
            } else if ($logWarnings) {
                Yii::warning([
                    'Response Status Code' => $response->getStatusCode(),
                    'Response Content' => $response->getContent(),
                    ], __METHOD__);
            }
        } catch (Exception $e) {
            Yii::error($e->getMessage(), __METHOD__);
        }

        return false;
    }

    /**
     * Retrieve a user resource.
     * 
     * GET /user
     * 
     * @return mixed false on fail or response data
     */
    public function getUser()
    {
        $request = $this->client->createRequest()
            ->setMethod('GET')
            ->setUrl('user')
            ->setOptions([
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_TIMEOUT => 30,
        ]);

        return $this->processRequest($request);
    }

    /**
     * Create a user. Recently changed to not generate a token. See POST
     * /oauth2/token for instructions on generating a token for the new user.
     * 
     * POST /user
     * 
     * @params array $params
     * @return mixed false on fail or response data
     */
    public function createUser($properties = [])
    {
        $request = $this->client->createRequest()
            ->setMethod('POST')
            ->setUrl('user')
            ->setData($properties)
            ->setOptions([
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_TIMEOUT => 30,
        ]);

        return $this->processRequest($request);
    }
    
    /**
     * Retrieve a list of the user's documents.
     * 
     * GET /user/documentsv2
     * 
     * @return mixed false on fail or response data
     */
    public function getUserDocuments()
    {
        $request = $this->client->createRequest()
            ->setMethod('GET')
            ->setUrl('user/documentsv2')
            ->setOptions([
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_TIMEOUT => 30,
        ]);

        return $this->processRequest($request);
    }

    /**
     * Request an access token for a user.
     * 
     * POST /oauth2/token
     * 
     * @return mixed false on fail or response data
     */
    public function createUserOAuth2Token($properties = [])
    {
        $request = $this->client->createRequest()
            ->setMethod('POST')
            ->setUrl('/oauth2/token')
            ->setData($properties)
            ->setHeaders([
                'Content-Type' => 'application/x-www-form-urlencoded',
            ])
            ->setOptions([
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_TIMEOUT => 30,
        ]);

        return $this->processRequest($request);
    }

    /**
     * Verify an access token for a user.
     * 
     * GET /oauth2/token
     * 
     * @return mixed false on fail or response data
     */
    public function verifyUserOAuth2Token($properties = [])
    {
        $request = $this->client->createRequest()
            ->setMethod('GET')
            ->setUrl('/oauth2/token')
            ->setOptions([
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_TIMEOUT => 30,
        ]);

        return $this->processRequest($request);
    }

}

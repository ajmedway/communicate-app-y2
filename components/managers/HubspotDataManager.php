<?php

namespace app\components\managers;

use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use app\models\Profile;
use app\models\Company;

/**
 * Class HubspotDataManager
 * 
 * @author Alec Pritchard <alec@r-evolution.co.uk>
 * @see https://github.com/ryanwinchester/hubspot-php
 */
class HubspotDataManager extends Component
{
    /**
     * @var string Hubspot API Contact Type
     */
    public static $HSTYPE_CONTACT = 'contact';

    /**
     * @var string Hubspot API Company Type
     */
    public static $HSTYPE_COMPANY = 'company';

    /**
     * @var string Hubspot API Key
     * @see https://developers.hubspot.com/docs/methods/auth/oauth-overview
     */
    public $hubspotApiKey;

    /**
     * @var \SevenShores\Hubspot\Factory Hubspot API factory object
     */
    private $_apiFactory;

    /**
     * @var array Hubspot contact properties
     */
    private $_contactPropertiesList;

    /**
     * @var array Hubspot company properties
     */
    private $_companyPropertiesList;

    /**
     * @var array Hubspot contact properties to fetch, indexed by user Profile model field names
     */
    public $contactPropertiesFetch = [
        'public_email' => 'email',
        'website' => 'website',
        'hs_contact_id' => 'vid',
        'hs_salutation' => 'salutation',
        'hs_firstname' => 'firstname',
        'hs_lastname' => 'lastname',
        'hs_jobtitle' => 'jobtitle',
        'hs_mobile_phone' => 'mobilephone',
        'hs_phone' => 'phone',
        'hs_fax' => 'fax',
        'hs_address_1' => 'address',
        'hs_address_2' => 'address_line_2',
        'hs_address_3' => 'address_line_3',
        'hs_city' => 'city',
        'hs_state' => 'state',
        'hs_region' => 'region',
        'hs_zip' => 'zip',
        'hs_country' => 'country',
        'hs_telecoms_platform_carrier' => 'telecoms_platform_carrier',
        'hs_park_building_name' => 'park_building_name',
        'hs_park_type' => 'park_type',
        'hs_company' => 'company',
        'hs_industry' => 'industry',
        'hs_associated_company_id' => 'associatedcompanyid',
        'hs_billing_contact' => 'billing_contact_',
        'hs_it_contact' => 'it_contact',
        'hs_hubspot_owner_id' => 'hubspot_owner_id',
        'hs_last_modified_date' => 'lastmodifieddate',
        'hs_created_date' => 'createdate',
    ];

    /**
     * @var array Hubspot contact properties to send, indexed by user Profile model field names
     */
    public $contactPropertiesSend = [
        'website' => 'website',
        'hs_salutation' => 'salutation',
        'hs_firstname' => 'firstname',
        'hs_lastname' => 'lastname',
        'hs_jobtitle' => 'jobtitle',
        'hs_mobile_phone' => 'mobilephone',
        'hs_phone' => 'phone',
        'hs_fax' => 'fax',
        'hs_address_1' => 'address',
        'hs_address_2' => 'address_line_2',
        'hs_address_3' => 'address_line_3',
        'hs_city' => 'city',
        'hs_state' => 'state',
        'hs_region' => 'region',
        'hs_zip' => 'zip',
        'hs_country' => 'country',
        'hs_company' => 'company',
        'hs_billing_contact' => 'billing_contact_',
    ];

    /**
     * @var array Hubspot company properties to fetch, indexed by Company model field names
     */
    public $companyPropertiesFetch = [
        'hs_company_id' => 'companyId',
        'hs_name' => 'name',
        'hs_phone' => 'phone',
        'hs_address_1' => 'address',
        'hs_address_2' => 'address2',
        'hs_city' => 'city',
        'hs_state' => 'state',
        'hs_region' => 'region',
        'hs_zip' => 'zip',
        'hs_country' => 'country',
        'hs_park_building_name' => 'park_building_name',
        'hs_park_building_type' => 'park_building_type',
        'hs_website' => 'website',
        'hs_domain' => 'domain',
        'hs_industry' => 'industry',
        'hs_description' => 'description',
        'hs_parent_company_id' => 'hs_parent_company_id',
        'hs_hubspot_owner_id' => 'hubspot_owner_id',
        'hs_last_modified_date' => 'hs_lastmodifieddate',
        'hs_created_date' => 'createdate',
    ];

    /**
     * @var array Hubspot company properties to fetch, indexed by Company model field names
     */
    public $companyPropertiesSend = [
        'hs_name' => 'name',
        'hs_phone' => 'phone',
        'hs_address_1' => 'address',
        'hs_address_2' => 'address2',
        'hs_city' => 'city',
        'hs_state' => 'state',
        'hs_region' => 'region',
        'hs_zip' => 'zip',
        'hs_country' => 'country',
        'hs_park_building_name' => 'park_building_name',
        'hs_park_building_type' => 'park_building_type',
        'hs_website' => 'website',
        'hs_domain' => 'domain',
        'hs_industry' => 'industry',
        'hs_description' => 'description',
    ];
    
    /**
     * {@inheritdoc}
     */
    public function init()
    {
        if (empty($this->hubspotApiKey)) {
            throw new InvalidConfigException('Hubspot API Key cannot be empty.');
        }
        
        parent::init();
        
        // initialise Hubspot factory instance after configuration is applied
        $this->_apiFactory = $this->getHubspotApiFactoryInstance();
    }
    
    /**
     * Get Hubspot API factory instance
     * 
     * @return \SevenShores\Hubspot\Factory
     */
    protected function getHubspotApiFactoryInstance()
    {
        return new \SevenShores\Hubspot\Factory([
            'key' => $this->hubspotApiKey,
            'oauth' => false, // default
            'base_url' => 'https://api.hubapi.com' // default
        ]);
    }

    /**
     * Get all active contact properties
     *
     * @return string JSON Response
     * 
     * @see https://developers.hubspot.com/docs/methods/contacts/v2/get_contacts_properties
     */
    public function getContactPropertiesList()
    {
        // if not cached in this instance, build from API request
        if (!isset($this->_contactPropertiesList)) {
            $contactPropertiesList = [];

            try {
                $response = $this->_apiFactory->contactProperties()->all();
                if ($response->getStatusCode() === 200) {
                    foreach ($response->getData() as $contactProperty) {
                        // exclude deleted properties
                        if ($contactProperty->deleted) {
                            continue;
                        }
                        $contactPropertiesList[] = $contactProperty->name;
                    }
                }
            } catch (\Exception $e) {
                // log error
                // $e->getMessage();
            }

            $this->_contactPropertiesList = $contactPropertiesList;
        }

        return $this->_contactPropertiesList;
    }

    /**
     * Get a specific contact by email
     *
     * @param string $email
     * @return string JSON Response
     * 
     * @see https://developers.hubspot.com/docs/methods/contacts/get_contact_by_email
     */
    public function getContactByEmail($email)
    {
        $params = [
            'property' => $this->contactPropertiesFetch,
        ];

        try {
            $response = $this->_apiFactory->contacts()->getByEmail($email, $params);
            return ($response->getStatusCode() === 200) ? $response->getData() : false;
        } catch (\Exception $e) {
            // log error
            // $e->getMessage();
        }

        return false;
    }

    /**
     * Map Hubspot properties to native model fields
     *
     * @param object $hsObject Hubspot API data object
     * @param string $hsType Hubspot object type
     * @return bool
     */
    protected function mapPropertiesHubspotToNative($hsObject, $hsType)
    {
        if ($hsType === self::$HSTYPE_CONTACT) {
            $model = 'Profile';
            $hsPropertyMap = array_flip($this->contactPropertiesFetch);
            $properties[$model] = [
                'hs_contact_id' => $hsObject->vid,
            ];
        } else if ($hsType === self::$HSTYPE_COMPANY) {
            $model = 'Company';
            $hsPropertyMap = array_flip($this->companyPropertiesFetch);
            $properties[$model] = [
                'hs_company_id' => $hsObject->companyId,
            ];
        }

        foreach ($hsObject->properties as $hsProperty => $value) {
            // extrapolate native model property name for this Hubspot property
            $property = ArrayHelper::getValue($hsPropertyMap, $hsProperty);
            if ($property) {
                // convert Hubspot booleans to tinyint
                if ($value->value === 'true') {
                    $value->value = 1;
                } else if ($value->value === 'false') {
                    $value->value = 0;
                }
                // convert Hubspot timestamps in milliseconds to mysql datetime format
                if (in_array($property, ['hs_last_modified_date', 'hs_created_date'])) {
                    $value->value = Yii::$app->formatter->asDatetime($value->value / 1000, 'php:Y-m-d H:i:s');
                }
                $properties[$model][$property] = $value->value;
            }
        }

        return $properties;
    }

    /**
     * Map native model fields to Hubspot properties
     *
     * @param array $data Model attributes
     * @param string $hsType Hubspot object type
     * @return bool
     */
    protected function mapPropertiesNativeToHubspot($data, $hsType)
    {
        if ($hsType === self::$HSTYPE_CONTACT) {
            $hsPropertyMap = $this->contactPropertiesSend;
        } else if ($hsType === self::$HSTYPE_COMPANY) {
            $hsPropertyMap = $this->companyPropertiesSend;
        }

        $properties = [];
        foreach ($data as $property => $value) {
            $hsProperty = ArrayHelper::getValue($hsPropertyMap, $property);
            if ($hsProperty) {
                $properties[] = [
                    'property' => $hsProperty,
                    'value' => $value,
                ];
            }
        }

        return $properties;
    }

    /**
     * Update a profile record with data from Hubspot contact.
     * Additionally creates corresponding Hubspot company, only if one is found
     * and one is not already natively associated with the user profile.
     * 
     * @param User $user
     * @return Profile|bool The updated profile text or false if not updated.
     */
    public function updateProfileFromHubspotContactByUser($user)
    {
        if (!empty($user->email)) {
            $hsContact = $this->getContactByEmail($user->email);
        }

        if (!empty($hsContact->vid)) {

            $properties = $this->mapPropertiesHubspotToNative($hsContact, self::$HSTYPE_CONTACT);
            if (empty($properties)) {
                // no user profile record exists
                Yii::error("No Hubspot properties mapped for User #{$user->id}", __METHOD__);
                return false;
            }

            // get user profile active record model instance
            $profile = Profile::findOne(['user_id' => $user->id]);
            if ($profile === null) {
                Yii::error("No Profile record found for User #{$user->id}", __METHOD__);
                return false;
            }

            // update the contact record in db
            if ($profile->load($properties) && $profile->save()) {

                Yii::info("Updated Profile for User #{$profile->user_id} from Hubspot contact data", __METHOD__);

                // get associated company data from Hubspot
                $hsCompany = $this->getCompanyById($profile->hs_associated_company_id);
                $properties = $this->mapPropertiesHubspotToNative($hsCompany, self::$HSTYPE_COMPANY);

                // if no company found and user not already associated with one, create new company record
                if (!($company = Company::findOne(['hs_company_id' => $hsCompany->companyId])) && $profile->company === null) {

                    Yii::info("Creating new Company from Hubspot companyId {$hsCompany->companyId}", __METHOD__);
                    $company = new Company();
                    
                    if ($company->load($properties) && $company->save()) {

                        Yii::info("Created Company #{$company->id} from Hubspot company {$company->hs_company_id} data", __METHOD__);

                        // link new company to the profile
                        $profile->link('company', $company);

                    } else {
                        Yii::error($company->getErrors(), __METHOD__);
                    }
                }

            } else {
                Yii::error($profile->getErrors(), __METHOD__);
            }

            return $profile;
        }

        // email address not found in Hubspot
        return false;
    }

    /**
     * Update a Hubspot contact with data from profile record
     *
     * @param User $user
     * @return bool
     */
    public function updateHubspotContactFromProfileByUser($user)
    {
        if ($user->profile) {
            $profileData = $user->profile->getAttributes(array_keys($this->contactPropertiesSend));
            $properties = $this->mapPropertiesNativeToHubspot($profileData, self::$HSTYPE_CONTACT);

            return $this->updateContactByEmail($user->email, $properties);
        }

        // email address does not correspond to any profiles
        return false;
    }

    /**
     * Get all contacts
     *
     * @param int $count Count records to return
     * @param int $start Start vid (Hubspot user id)
     * @return array Contacts
     * 
     * @see https://developers.hubspot.com/docs/methods/contacts/get_contacts
     */
    public function getAllContacts($count = 100, $start = 0)
    {
        $contacts = [];
        $params = [
            'count' => $count,
            'property' => $this->getContactPropertiesList(),
        ];
        $i = 0;

        // Iteratively get all contacts (from start vid if set)
        do {
            $i++;
            if (!empty($response->{'vid-offset'})) {
                $params['vidOffset'] = $response->{'vid-offset'};
            } elseif ($i === 1 && !empty($start)) {
                $params['vidOffset'] = $start;
            }

            try {
                $response = $this->_apiFactory->contacts()->all($params);
                if (($response->getStatusCode() === 200)) {
                    $contacts = array_merge($contacts, $response->contacts);
                }
            } catch (\Exception $e) {
                Yii::error($e->getMessage(), __METHOD__);
            }
        } while ($response->{'has-more'});

        return $contacts;
    }

    /**
     * Update a specific contact by email
     *
     * @param string $email
     * @param array $properties
     * @return null (blank body response, check status code)
     * 
     * @see https://developers.hubspot.com/docs/methods/contacts/update_contact-by-email
     */
    public function updateContactByEmail($email, $properties)
    {
        try {
            $response = $this->_apiFactory->contacts()->updateByEmail($email, $properties);
            return ($response->getStatusCode() === 204) ? true : false;
        } catch (\Exception $e) {
            Yii::warning($e->getMessage(), __METHOD__);
        }

        return false;
    }

    /**
     * Get all active company properties
     *
     * @return string JSON Response
     * 
     * @see https://developers.hubspot.com/docs/methods/companies/get_company_properties
     */
    public function getCompanyPropertiesList()
    {
        // if not cached in this instance, build from API request
        if (!isset($this->_companyPropertiesList)) {
            $companyPropertiesList = [];

            try {
                $response = $this->_apiFactory->companyProperties()->all();
                if ($response->getStatusCode() === 200) {
                    foreach ($response->getData() as $companyProperty) {
                        // exclude deleted properties
                        if ($companyProperty->deleted) {
                            continue;
                        }
                        $companyPropertiesList[] = $companyProperty->name;
                    }
                }
            } catch (\Exception $e) {
                Yii::error($e->getMessage(), __METHOD__);
            }

            $this->_companyPropertiesList = $companyPropertiesList;
        }

        return $this->_companyPropertiesList;
    }

    /**
     * Get a specific company by id
     *
     * @param integer $id Hubspot company Id
     * @return string JSON Response
     * 
     * @see https://developers.hubspot.com/docs/methods/companies/get_company
     */
    public function getCompanyById($id)
    {
        $params = [
            'property' => $this->companyPropertiesFetch,
        ];

        try {
            $response = $this->_apiFactory->companies()->getById($id);
            return ($response->getStatusCode() === 200) ? $response->getData() : false;
        } catch (\Exception $e) {
            Yii::error($e->getMessage(), __METHOD__);
        }

        return false;
    }

}

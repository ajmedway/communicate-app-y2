<?php

use yii\db\Migration;

/**
 * Handles adding gdpr_email_column_gdpr_sms_column_gdpr_phone_column_gdpr_post_column_gdpr_version to table `profile`.
 */
class m180622_124444_add_gdpr_email_column_gdpr_sms_column_gdpr_phone_column_gdpr_post_column_gdpr_version_column_to_profile_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('profile', 'gdpr_email', $this->boolean()->after('bio')->defaultValue(0));
        $this->addColumn('profile', 'gdpr_sms', $this->boolean()->after('gdpr_email')->defaultValue(0));
        $this->addColumn('profile', 'gdpr_phone', $this->boolean()->after('gdpr_sms')->defaultValue(0));
        $this->addColumn('profile', 'gdpr_post', $this->boolean()->after('gdpr_phone')->defaultValue(0));
        $this->addColumn('profile', 'gdpr_version', $this->integer()->after('gdpr_post')->defaultValue(NULL));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('profile', 'gdpr_email');
        $this->dropColumn('profile', 'gdpr_sms');
        $this->dropColumn('profile', 'gdpr_phone');
        $this->dropColumn('profile', 'gdpr_post');
        $this->dropColumn('profile', 'gdpr_version');
    }
}

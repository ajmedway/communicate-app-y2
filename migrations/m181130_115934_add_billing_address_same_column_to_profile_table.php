<?php

use yii\db\Migration;

/**
 * Handles adding billing_address_same to table `profile`.
 */
class m181130_115934_add_billing_address_same_column_to_profile_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('profile', 'billing_address_same', $this->tinyInteger(1)->defaultValue(1));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('profile', 'billing_address_same');
    }
}

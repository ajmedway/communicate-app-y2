<?php

use yii\db\Migration;

/**
 * Class m180426_154033_add_hs_associated_company_id_index_hs_hubspot_owner_id_index_to_profile_table
 */
class m180426_154033_add_hs_associated_company_id_index_hs_hubspot_owner_id_index_to_profile_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // creates index for column `hs_associated_company_id`
        $this->createIndex(
            'idx_hs_associated_company_id',
            'profile',
            'hs_associated_company_id'
        );
        
        // creates index for column `hs_hubspot_owner_id`
        $this->createIndex(
            'idx_hs_hubspot_owner_id',
            'profile',
            'hs_hubspot_owner_id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180426_154033_add_hs_associated_company_id_index_hs_hubspot_owner_id_index_to_profile_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180426_154033_add_hs_associated_company_id_index_hs_hubspot_owner_id_index_to_profile_table cannot be reverted.\n";

        return false;
    }
    */
}

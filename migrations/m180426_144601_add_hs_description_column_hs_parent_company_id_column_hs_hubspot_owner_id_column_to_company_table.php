<?php

use yii\db\Migration;

/**
 * Handles adding hs_description_column_hs_parent_company_id_column_hs_hubspot_owner_id to table `company`.
 */
class m180426_144601_add_hs_description_column_hs_parent_company_id_column_hs_hubspot_owner_id_column_to_company_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('company', 'hs_description', $this->string()->defaultValue(NULL));
        $this->addColumn('company', 'hs_parent_company_id', $this->integer()->defaultValue(NULL));
        $this->addColumn('company', 'hs_hubspot_owner_id', $this->integer()->defaultValue(NULL));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('company', 'hs_description');
        $this->dropColumn('company', 'hs_parent_company_id');
        $this->dropColumn('company', 'hs_hubspot_owner_id');
    }
}

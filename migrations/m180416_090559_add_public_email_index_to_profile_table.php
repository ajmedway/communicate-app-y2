<?php

use yii\db\Migration;

/**
 * Class m180416_090559_add_public_email_index_to_profile_table
 */
class m180416_090559_add_public_email_index_to_profile_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // creates index for column `public_email`
        $this->createIndex(
            'idx_public_email',
            'profile',
            'public_email'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180416_090559_add_public_email_index_to_profile_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180416_090559_add_public_email_index_to_profile_table cannot be reverted.\n";

        return false;
    }
    */
}

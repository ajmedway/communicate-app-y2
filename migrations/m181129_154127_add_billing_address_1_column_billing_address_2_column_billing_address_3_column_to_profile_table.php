<?php

use yii\db\Migration;

/**
 * Handles adding billing_address_1_column_billing_address_2_column_billing_address_3 to table `profile`.
 */
class m181129_154127_add_billing_address_1_column_billing_address_2_column_billing_address_3_column_to_profile_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('profile', 'billing_address_1', $this->string()->defaultValue(NULL));
        $this->addColumn('profile', 'billing_address_2', $this->string()->defaultValue(NULL));
        $this->addColumn('profile', 'billing_address_3', $this->string()->defaultValue(NULL));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('profile', 'billing_address_1');
        $this->dropColumn('profile', 'billing_address_2');
        $this->dropColumn('profile', 'billing_address_3');
    }
}

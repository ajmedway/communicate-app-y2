<?php

use yii\db\Migration;

/**
 * Handles adding company_id to table `profile`.
 * Has foreign keys to the tables:
 *
 * - `company`
 */
class m180430_165518_add_company_id_column_to_profile_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('profile', 'company_id', $this->integer()->after('user_id')->defaultValue(NULL));

        // creates index for column `company_id`
        $this->createIndex(
            'idx-profile-company_id',
            'profile',
            'company_id'
        );

        // add foreign key for table `company`
        $this->addForeignKey(
            'fk-profile-company_id',
            'profile',
            'company_id',
            'company',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `company`
        $this->dropForeignKey(
            'fk-profile-company_id',
            'profile'
        );

        // drops index for column `company_id`
        $this->dropIndex(
            'idx-profile-company_id',
            'profile'
        );

        $this->dropColumn('profile', 'company_id');
    }
}

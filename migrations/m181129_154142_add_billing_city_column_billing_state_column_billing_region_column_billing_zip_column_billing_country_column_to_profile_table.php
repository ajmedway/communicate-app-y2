<?php

use yii\db\Migration;

/**
 * Handles adding billing_city_column_billing_state_column_billing_region_column_billing_zip_column_billing_country to table `profile`.
 */
class m181129_154142_add_billing_city_column_billing_state_column_billing_region_column_billing_zip_column_billing_country_column_to_profile_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('profile', 'billing_city', $this->string()->defaultValue(NULL));
        $this->addColumn('profile', 'billing_state', $this->string()->defaultValue(NULL));
        $this->addColumn('profile', 'billing_region', $this->string()->defaultValue(NULL));
        $this->addColumn('profile', 'billing_zip', $this->string()->defaultValue(NULL));
        $this->addColumn('profile', 'billing_country', $this->string()->defaultValue(NULL));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('profile', 'billing_city');
        $this->dropColumn('profile', 'billing_state');
        $this->dropColumn('profile', 'billing_region');
        $this->dropColumn('profile', 'billing_zip');
        $this->dropColumn('profile', 'billing_country');
    }
}

<?php

use yii\db\Migration;

/**
 * Handles adding PrimaryContactID_column_PrimaryAddressID to table `company`.
 */
class m180614_154815_add_PrimaryContactID_column_PrimaryAddressID_column_to_company_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('company', 'PrimaryContactID', $this->integer()->defaultValue(NULL));
        $this->addColumn('company', 'PrimaryAddressID', $this->integer()->defaultValue(NULL));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('company', 'PrimaryContactID');
        $this->dropColumn('company', 'PrimaryAddressID');
    }
}

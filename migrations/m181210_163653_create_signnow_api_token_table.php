<?php

use yii\db\Migration;

/**
 * Handles the creation of table `signnow_api_token`.
 */
class m181210_163653_create_signnow_api_token_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('signnow_api_token', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->defaultValue(NULL),
            'access_token' => $this->string()->defaultValue(NULL),
            'token_type' => $this->string()->defaultValue(NULL),
            'expires_in' => $this->integer()->defaultValue(NULL),
            'refresh_token' => $this->string()->defaultValue(NULL),
            'scope' => $this->string()->defaultValue(NULL),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('signnow_api_token');
    }
}

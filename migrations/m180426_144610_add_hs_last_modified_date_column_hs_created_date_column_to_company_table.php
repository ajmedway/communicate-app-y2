<?php

use yii\db\Migration;

/**
 * Handles adding hs_last_modified_date_column_hs_created_date to table `company`.
 */
class m180426_144610_add_hs_last_modified_date_column_hs_created_date_column_to_company_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('company', 'hs_last_modified_date', $this->dateTime()->defaultValue(NULL));
        $this->addColumn('company', 'hs_created_date', $this->dateTime()->defaultValue(NULL));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('company', 'hs_last_modified_date');
        $this->dropColumn('company', 'hs_created_date');
    }
}

<?php

use yii\db\Migration;

/**
 * Class m181210_163703_add_username_index_to_signnow_api_token_table
 */
class m181210_163703_add_username_index_to_signnow_api_token_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // creates index for column `username`
        $this->createIndex(
            'idx_username',
            'signnow_api_token',
            'username'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181210_163703_add_username_index_to_signnow_api_token_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181210_163703_add_username_index_to_signnow_api_token_table cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Handles adding hs_park_building_name_column_hs_park_building_type_column_hs_website_column_hs_domain_column_hs_industry to table `company`.
 */
class m180426_144552_add_hs_park_building_name_column_hs_park_building_type_column_hs_website_column_hs_domain_column_hs_industry_column_to_company_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('company', 'hs_park_building_name', $this->string()->defaultValue(NULL));
        $this->addColumn('company', 'hs_park_building_type', $this->string()->defaultValue(NULL));
        $this->addColumn('company', 'hs_website', $this->string()->defaultValue(NULL));
        $this->addColumn('company', 'hs_domain', $this->string()->defaultValue(NULL));
        $this->addColumn('company', 'hs_industry', $this->string()->defaultValue(NULL));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('company', 'hs_park_building_name');
        $this->dropColumn('company', 'hs_park_building_type');
        $this->dropColumn('company', 'hs_website');
        $this->dropColumn('company', 'hs_domain');
        $this->dropColumn('company', 'hs_industry');
    }
}

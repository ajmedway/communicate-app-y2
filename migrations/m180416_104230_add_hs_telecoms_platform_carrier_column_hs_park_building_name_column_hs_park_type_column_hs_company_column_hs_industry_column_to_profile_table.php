<?php

use yii\db\Migration;

/**
 * Handles adding hs_telecoms_platform_carrier_column_hs_park_building_name_column_hs_park_type_column_hs_company_column_hs_industry to table `profile`.
 */
class m180416_104230_add_hs_telecoms_platform_carrier_column_hs_park_building_name_column_hs_park_type_column_hs_company_column_hs_industry_column_to_profile_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('profile', 'hs_telecoms_platform_carrier', $this->string()->defaultValue(NULL));
        $this->addColumn('profile', 'hs_park_building_name', $this->string()->defaultValue(NULL));
        $this->addColumn('profile', 'hs_park_type', $this->string()->defaultValue(NULL));
        $this->addColumn('profile', 'hs_company', $this->string()->defaultValue(NULL));
        $this->addColumn('profile', 'hs_industry', $this->string()->defaultValue(NULL));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('profile', 'hs_telecoms_platform_carrier');
        $this->dropColumn('profile', 'hs_park_building_name');
        $this->dropColumn('profile', 'hs_park_type');
        $this->dropColumn('profile', 'hs_company');
        $this->dropColumn('profile', 'hs_industry');
    }
}

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `company`.
 */
class m180426_122822_create_company_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('company', [
            'id' => $this->primaryKey(),
            'created_date' => $this->dateTime()->defaultValue(NULL),
            'modified_date' => $this->dateTime()->defaultValue(NULL),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('company');
    }
}

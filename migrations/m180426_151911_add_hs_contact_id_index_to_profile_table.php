<?php

use yii\db\Migration;

/**
 * Class m180426_151911_add_hs_contact_id_index_to_profile_table
 */
class m180426_151911_add_hs_contact_id_index_to_profile_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // creates index for column `public_email`
        $this->createIndex(
            'idx_hs_contact_id',
            'profile',
            'hs_contact_id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180426_151911_add_hs_contact_id_index_to_profile_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180426_151911_add_hs_contact_id_index_to_profile_table cannot be reverted.\n";

        return false;
    }
    */
}

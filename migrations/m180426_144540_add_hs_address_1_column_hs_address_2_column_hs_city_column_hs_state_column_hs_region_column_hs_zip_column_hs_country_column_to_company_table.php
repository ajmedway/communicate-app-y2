<?php

use yii\db\Migration;

/**
 * Handles adding hs_address_1_column_hs_address_2_column_hs_city_column_hs_state_column_hs_region_column_hs_zip_column_hs_country to table `company`.
 */
class m180426_144540_add_hs_address_1_column_hs_address_2_column_hs_city_column_hs_state_column_hs_region_column_hs_zip_column_hs_country_column_to_company_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('company', 'hs_address_1', $this->string()->defaultValue(NULL));
        $this->addColumn('company', 'hs_address_2', $this->string()->defaultValue(NULL));
        $this->addColumn('company', 'hs_city', $this->string()->defaultValue(NULL));
        $this->addColumn('company', 'hs_state', $this->string()->defaultValue(NULL));
        $this->addColumn('company', 'hs_region', $this->string()->defaultValue(NULL));
        $this->addColumn('company', 'hs_zip', $this->string()->defaultValue(NULL));
        $this->addColumn('company', 'hs_country', $this->string()->defaultValue(NULL));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('company', 'hs_address_1');
        $this->dropColumn('company', 'hs_address_2');
        $this->dropColumn('company', 'hs_city');
        $this->dropColumn('company', 'hs_state');
        $this->dropColumn('company', 'hs_region');
        $this->dropColumn('company', 'hs_zip');
        $this->dropColumn('company', 'hs_country');
    }
}

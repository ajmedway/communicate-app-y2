<?php

use yii\db\Migration;

/**
 * Handles adding hs_salutation_column_hs_firstname_column_hs_lastname_column_hs_jobtitle_column_hs_mobile_phone_column_hs_phone_column_hs_fax to table `profile`.
 */
class m180416_104008_add_hs_salutation_column_hs_firstname_column_hs_lastname_column_hs_jobtitle_column_hs_mobile_phone_column_hs_phone_column_hs_fax_column_to_profile_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('profile', 'hs_salutation', $this->string()->defaultValue(NULL));
        $this->addColumn('profile', 'hs_firstname', $this->string()->defaultValue(NULL));
        $this->addColumn('profile', 'hs_lastname', $this->string()->defaultValue(NULL));
        $this->addColumn('profile', 'hs_jobtitle', $this->string()->defaultValue(NULL));
        $this->addColumn('profile', 'hs_mobile_phone', $this->string()->defaultValue(NULL));
        $this->addColumn('profile', 'hs_phone', $this->string()->defaultValue(NULL));
        $this->addColumn('profile', 'hs_fax', $this->string()->defaultValue(NULL));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('profile', 'hs_salutation');
        $this->dropColumn('profile', 'hs_firstname');
        $this->dropColumn('profile', 'hs_lastname');
        $this->dropColumn('profile', 'hs_jobtitle');
        $this->dropColumn('profile', 'hs_mobile_phone');
        $this->dropColumn('profile', 'hs_phone');
        $this->dropColumn('profile', 'hs_fax');
    }
}

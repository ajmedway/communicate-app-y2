<?php

use yii\db\Migration;

/**
 * Handles adding hs_last_modified_date_column_hs_created_date to table `profile`.
 */
class m180416_104255_add_hs_last_modified_date_column_hs_created_date_column_to_profile_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('profile', 'hs_last_modified_date', $this->dateTime()->defaultValue(NULL));
        $this->addColumn('profile', 'hs_created_date', $this->dateTime()->defaultValue(NULL));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('profile', 'hs_last_modified_date');
        $this->dropColumn('profile', 'hs_created_date');
    }
}

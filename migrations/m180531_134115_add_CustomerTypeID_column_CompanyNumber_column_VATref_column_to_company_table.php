<?php

use yii\db\Migration;

/**
 * Handles adding CustomerTypeID_column_CompanyNumber_column_VATref to table `company`.
 */
class m180531_134115_add_CustomerTypeID_column_CompanyNumber_column_VATref_column_to_company_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('company', 'CustomerTypeID', $this->integer()->defaultValue(NULL));
        $this->addColumn('company', 'CompanyNumber', $this->string()->defaultValue(NULL));
        $this->addColumn('company', 'VATref', $this->string()->defaultValue(NULL));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('company', 'CustomerTypeID');
        $this->dropColumn('company', 'CompanyNumber');
        $this->dropColumn('company', 'VATref');
    }
}

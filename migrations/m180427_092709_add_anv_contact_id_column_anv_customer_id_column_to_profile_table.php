<?php

use yii\db\Migration;

/**
 * Handles adding anv_contact_id_column_anv_customer_id to table `profile`.
 */
class m180427_092709_add_anv_contact_id_column_anv_customer_id_column_to_profile_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('profile', 'anv_contact_id', $this->integer()->after('modified_date'));
        $this->addColumn('profile', 'anv_customer_id', $this->integer()->comment('An Anvil customer is equivalent to a Hubspot company')->after('anv_contact_id'));
        
        // creates index for column `anv_contact_id`
        $this->createIndex(
            'idx_anv_contact_id',
            'profile',
            'anv_contact_id'
        );
        
        // creates index for column `anv_customer_id`
        $this->createIndex(
            'idx_anv_customer_id',
            'profile',
            'anv_customer_id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('profile', 'anv_contact_id');
        $this->dropColumn('profile', 'anv_customer_id');
    }
}

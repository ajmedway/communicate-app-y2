<?php

use yii\db\Migration;

/**
 * Class m180426_154413_add_hs_company_id_index_hs_parent_company_id_index_hs_hubspot_owner_id_index_to_company_table
 */
class m180426_154413_add_hs_company_id_index_hs_parent_company_id_index_hs_hubspot_owner_id_index_to_company_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // creates index for column `hs_company_id`
        $this->createIndex(
            'idx_hs_company_id',
            'company',
            'hs_company_id'
        );
        
        // creates index for column `hs_parent_company_id`
        $this->createIndex(
            'idx_hs_parent_company_id',
            'company',
            'hs_parent_company_id'
        );
        
        // creates index for column `hs_hubspot_owner_id`
        $this->createIndex(
            'idx_hs_hubspot_owner_id',
            'company',
            'hs_hubspot_owner_id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180426_154413_add_hs_company_id_index_hs_parent_company_id_index_hs_hubspot_owner_id_index_to_company_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180426_154413_add_hs_company_id_index_hs_parent_company_id_index_hs_hubspot_owner_id_index_to_company_table cannot be reverted.\n";

        return false;
    }
    */
}

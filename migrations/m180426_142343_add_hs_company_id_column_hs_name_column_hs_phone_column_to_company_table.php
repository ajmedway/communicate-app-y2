<?php

use yii\db\Migration;

/**
 * Handles adding hs_company_id_column_hs_name_column_hs_phone to table `company`.
 */
class m180426_142343_add_hs_company_id_column_hs_name_column_hs_phone_column_to_company_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('company', 'hs_company_id', $this->integer()->defaultValue(NULL));
        $this->addColumn('company', 'hs_name', $this->string()->defaultValue(NULL));
        $this->addColumn('company', 'hs_phone', $this->string()->defaultValue(NULL));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('company', 'hs_company_id');
        $this->dropColumn('company', 'hs_name');
        $this->dropColumn('company', 'hs_phone');
    }
}

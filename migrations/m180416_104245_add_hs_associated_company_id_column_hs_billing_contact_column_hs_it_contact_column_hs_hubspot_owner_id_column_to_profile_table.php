<?php

use yii\db\Migration;

/**
 * Handles adding hs_associated_company_id_column_hs_billing_contact_column_hs_it_contact_column_hs_hubspot_owner_id to table `profile`.
 */
class m180416_104245_add_hs_associated_company_id_column_hs_billing_contact_column_hs_it_contact_column_hs_hubspot_owner_id_column_to_profile_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('profile', 'hs_associated_company_id', $this->integer()->defaultValue(NULL));
        $this->addColumn('profile', 'hs_billing_contact', $this->boolean()->defaultValue(NULL));
        $this->addColumn('profile', 'hs_it_contact', $this->boolean()->defaultValue(NULL));
        $this->addColumn('profile', 'hs_hubspot_owner_id', $this->integer()->defaultValue(NULL));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('profile', 'hs_associated_company_id');
        $this->dropColumn('profile', 'hs_billing_contact');
        $this->dropColumn('profile', 'hs_it_contact');
        $this->dropColumn('profile', 'hs_hubspot_owner_id');
    }
}

<?php

use yii\db\Migration;

/**
 * Handles adding hs_address_1_column_hs_address_2_column_hs_address_3_column_hs_city_column_hs_state_column_hs_region_column_hs_zip_column_hs_country to table `profile`.
 */
class m180416_104220_add_hs_address_1_column_hs_address_2_column_hs_address_3_column_hs_city_column_hs_state_column_hs_region_column_hs_zip_column_hs_country_column_to_profile_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('profile', 'hs_address_1', $this->string()->defaultValue(NULL));
        $this->addColumn('profile', 'hs_address_2', $this->string()->defaultValue(NULL));
        $this->addColumn('profile', 'hs_address_3', $this->string()->defaultValue(NULL));
        $this->addColumn('profile', 'hs_city', $this->string()->defaultValue(NULL));
        $this->addColumn('profile', 'hs_state', $this->string()->defaultValue(NULL));
        $this->addColumn('profile', 'hs_region', $this->string()->defaultValue(NULL));
        $this->addColumn('profile', 'hs_zip', $this->string()->defaultValue(NULL));
        $this->addColumn('profile', 'hs_country', $this->string()->defaultValue(NULL));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('profile', 'hs_address_1');
        $this->dropColumn('profile', 'hs_address_2');
        $this->dropColumn('profile', 'hs_address_3');
        $this->dropColumn('profile', 'hs_city');
        $this->dropColumn('profile', 'hs_state');
        $this->dropColumn('profile', 'hs_region');
        $this->dropColumn('profile', 'hs_zip');
        $this->dropColumn('profile', 'hs_country');
    }
}

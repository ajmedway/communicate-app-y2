<?php

use yii\db\Migration;

/**
 * Handles adding hs_contact_id to table `profile`.
 */
class m180426_110608_add_hs_contact_id_column_to_profile_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('profile', 'hs_contact_id', $this->integer()->after('bio'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('profile', 'bio');
    }
}

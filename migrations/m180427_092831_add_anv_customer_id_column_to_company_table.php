<?php

use yii\db\Migration;

/**
 * Handles adding anv_customer_id to table `company`.
 */
class m180427_092831_add_anv_customer_id_column_to_company_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('company', 'anv_customer_id', $this->integer()->comment('An Anvil customer is equivalent to a Hubspot company')->after('modified_date'));
        
        // creates index for column `anv_customer_id`
        $this->createIndex(
            'idx_anv_customer_id',
            'company',
            'anv_customer_id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('company', 'anv_customer_id');
    }
}

<?php

use yii\db\Migration;

/**
 * Handles adding billing_contact_firstname_column_billing_contact_lastname_column_billing_contact_email to table `profile`.
 */
class m181204_153122_add_billing_contact_firstname_column_billing_contact_lastname_column_billing_contact_email_column_to_profile_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('profile', 'billing_contact_firstname', $this->string()->defaultValue(NULL));
        $this->addColumn('profile', 'billing_contact_lastname', $this->string()->defaultValue(NULL));
        $this->addColumn('profile', 'billing_contact_email', $this->string()->defaultValue(NULL));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('profile', 'billing_contact_firstname');
        $this->dropColumn('profile', 'billing_contact_lastname');
        $this->dropColumn('profile', 'billing_contact_email');
    }
}

<?php

use yii\db\Migration;

/**
 * Class m180416_111822_alter_timezone_column_in_profile_table
 */
class m180416_111822_alter_timezone_column_in_profile_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('profile', 'timezone', $this->string()->defaultValue('Europe/London'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180416_111822_alter_timezone_column_in_profile_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180416_111822_alter_timezone_column_in_profile_table cannot be reverted.\n";

        return false;
    }
    */
}

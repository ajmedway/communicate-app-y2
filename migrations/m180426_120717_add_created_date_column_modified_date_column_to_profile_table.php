<?php

use yii\db\Migration;

/**
 * Handles adding created_date_column_modified_date to table `profile`.
 */
class m180426_120717_add_created_date_column_modified_date_column_to_profile_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('profile', 'created_date', $this->dateTime()->after('bio')->defaultValue(NULL));
        $this->addColumn('profile', 'modified_date', $this->dateTime()->after('created_date')->defaultValue(NULL));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('profile', 'created_date');
        $this->dropColumn('profile', 'modified_date');
    }
}

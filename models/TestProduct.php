<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * TestProduct is a model for defining test products.
 *
 * @property TestProduct|null $user This property is read-only.
 *
 */
class TestProduct extends Model
{
    public $name;
    public $price;
    public $setup_price;
}

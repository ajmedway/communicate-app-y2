<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "company".
 *
 * @property int $id
 * @property string $created_date
 * @property string $modified_date
 * @property int $anv_customer_id An Anvil customer is equivalent to a Hubspot company
 * @property int $hs_company_id
 * @property string $hs_name
 * @property string $hs_phone
 * @property string $hs_address_1
 * @property string $hs_address_2
 * @property string $hs_city
 * @property string $hs_state
 * @property string $hs_region
 * @property string $hs_zip
 * @property string $hs_country
 * @property string $hs_park_building_name
 * @property string $hs_park_building_type
 * @property string $hs_website
 * @property string $hs_domain
 * @property string $hs_industry
 * @property string $hs_description
 * @property int $hs_parent_company_id
 * @property int $hs_hubspot_owner_id
 * @property string $hs_last_modified_date
 * @property string $hs_created_date
 * @property int $CustomerTypeID
 * @property string $CompanyNumber
 * @property string $VATref
 * @property int $PrimaryContactID
 * @property int $PrimaryAddressID
 * 
 * Defined relations:
 * @property Profile[] $profiles
 */
class Company extends \yii\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hs_name', 'hs_phone', 'CustomerTypeID', 'CompanyNumber'], 'required'],
            [['created_date', 'modified_date', 'hs_last_modified_date', 'hs_created_date'], 'safe'],
            [['anv_customer_id', 'hs_company_id', 'hs_parent_company_id', 'hs_hubspot_owner_id', 'CustomerTypeID'], 'integer'],
            [['anv_customer_id', 'hs_company_id', 'hs_parent_company_id', 'hs_hubspot_owner_id', 'CustomerTypeID', 'PrimaryContactID', 'PrimaryAddressID'], 'default', 'value' => null],
            [['hs_name', 'hs_phone', 'hs_address_1', 'hs_address_2', 'hs_city', 'hs_state', 'hs_region', 'hs_zip', 'hs_country', 'hs_park_building_name', 'hs_park_building_type', 'hs_website', 'hs_domain', 'hs_industry', 'hs_description', 'CompanyNumber', 'VATref'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_date' => 'Created Date',
            'modified_date' => 'Modified Date',
            'anv_customer_id' => 'Anvil Customer ID',
            'hs_company_id' => 'Hubspot Company ID',
            'hs_name' => 'Company Name',
            'hs_phone' => 'Phone',
            'hs_address_1' => 'Address 1',
            'hs_address_2' => 'Address 2',
            'hs_city' => 'City',
            'hs_state' => 'State',
            'hs_region' => 'Region',
            'hs_zip' => 'Zip',
            'hs_country' => 'Country',
            'hs_park_building_name' => 'Park Building Name',
            'hs_park_building_type' => 'Park Building Type',
            'hs_website' => 'Website',
            'hs_domain' => 'Domain',
            'hs_industry' => 'Industry',
            'hs_description' => 'Description',
            'hs_parent_company_id' => 'Parent Company ID',
            'hs_hubspot_owner_id' => 'Hubspot Owner ID',
            'hs_last_modified_date' => 'Last Modified Date',
            'hs_created_date' => 'Created Date',
            'CustomerTypeID' => 'Customer Type ID',
            'CompanyNumber' => 'Company Number',
            'VATref' => 'VAT Reference',
            'PrimaryContactID' => 'Primary Contact ID',
            'PrimaryAddressID' => 'Primary Address ID',
        ];
    }

    /**
     * @return array Anvil Customer Type options
     */
    public static function customerTypeOptions()
    {
        return [
            1 => 'Small Business',
            2 => 'Medium Business',
            3 => 'Corporate',
            4 => 'Charity',
            5 => 'Governmental',
            6 => 'Residential',
            7 => 'Other Organisation',
            8 => 'M2M',
            9 => 'M2M Trial',
            10 => 'Wholesale',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfiles()
    {
        return $this->hasMany(Profile::className(), ['company_id' => 'id']);
    }

    /**
     * @return int|null Anvil Company Id or null if not set
     */
    public static function getAnvilCompanyId()
    {
        return isset(Yii::$app->params['anvilCompanyId']) ? Yii::$app->params['anvilCompanyId'] : null;
    }

    /**
     * {@inheritdoc}
     * @return CompanyQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CompanyQuery(get_called_class());
    }

    /**
     * Handle any nuances of data in the model before we commit to insert/update
     * 
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->modified_date = new \yii\db\Expression('NOW()');
            if (empty($this->created_date)) {
                $this->created_date = new \yii\db\Expression('NOW()');
            }
            return true;
        } else {
            return false;
        }
    }

}

<?php

namespace app\models;

use Yii;
use Da\User\Model\User as BaseUser;

/**
 * Extended User ActiveRecord model.
 *
 * @property bool $isAdmin
 * @property bool $isBlocked
 * @property bool $isConfirmed whether user account has been confirmed or not
 *
 * Database fields:
 * @property int $id
 * @property string $username
 * @property string $email
 * @property string $unconfirmed_email
 * @property string $password_hash
 * @property string $auth_key
 * @property string $auth_tf_key
 * @property int $auth_tf_enabled
 * @property int $registration_ip
 * @property int $confirmed_at
 * @property int $blocked_at
 * @property int $flags
 * @property int $created_at
 * @property int $updated_at
 * @property int $last_login_at
 *
 * Defined relations:
 * @property SocialNetworkAccount[] $socialNetworkAccounts
 * @property Profile $profile
 */
class User extends BaseUser implements \yii\web\IdentityInterface
{
    public static $ROLE_ADMIN = 'admin';
    public static $ROLE_USER = 'user';
    
    /**
     * Sends Order Complete email to customer & sales team
     * 
     * @return boolean whether the send operation was reported as successful
     */
    public function newUserSalesNotificationEmail()
    {
        $subject = "New Communicate User Signup: {$this->username}";

        $sent = Yii::$app->mailer->compose('@app/mail/user/new-user-sales-notification', [
                'title' => $subject,
                'id' => $this->id,
                'username' => $this->username,
                'email' => $this->email,
                'created_at' => $this->created_at,
                'registration_ip' => $this->registration_ip,
            ])
            ->setFrom(Yii::$app->params['salesEmail'])
            //->setTo(Yii::$app->params['salesEmail'])
            ->setTo([Yii::$app->params['alecEmail'] => 'Alec Pritchard'])
            ->setSubject($subject)
            ->send();

        return $sent;
    }
}

<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[SignnowApiToken]].
 *
 * @see SignnowApiToken
 */
class SignnowApiTokenQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * Find SignnowApiToken record by username
     * @return SignnowApiToken|null
     */
    public function byUsername($username)
    {
        return $this->andWhere(['username' => $username])->one();
    }

    /**
     * {@inheritdoc}
     * @return SignnowApiToken[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return SignnowApiToken|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "signnow_api_token".
 *
 * @property int $id
 * @property string $username
 * @property string $access_token
 * @property string $token_type
 * @property int $expires_in
 * @property string $refresh_token
 * @property string $scope
 */
class SignnowApiToken extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'signnow_api_token';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['expires_in'], 'integer'],
            [['username', 'access_token', 'token_type', 'refresh_token', 'scope'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'access_token' => 'Access Token',
            'token_type' => 'Token Type',
            'expires_in' => 'Expires In',
            'refresh_token' => 'Refresh Token',
            'scope' => 'Scope',
        ];
    }

    /**
     * {@inheritdoc}
     * @return SignnowApiTokenQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SignnowApiTokenQuery(get_called_class());
    }
}

<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\modules\quote\models\Quote;
use Da\User\Model\Profile as BaseProfile;

/**
 * Extended Profile ActiveRecord model.
 * This is the model class for table "profile".
 * 
 * Database fields:
 * @property int $user_id
 * @property string $name
 * @property string $public_email
 * @property string $gravatar_email
 * @property string $gravatar_id
 * @property string $location
 * @property string $website
 * @property string $timezone
 * @property string $bio
 * 
 * Custom Migration fields:
 * @property int $company_id
 * @property int $gdpr_email
 * @property int $gdpr_sms
 * @property int $gdpr_phone
 * @property int $gdpr_post
 * @property int $gdpr_version
 * @property string $created_date
 * @property string $modified_date
 * @property int $anv_contact_id
 * @property int $anv_customer_id An Anvil customer is equivalent to a Hubspot company
 * @property int $hs_contact_id
 * @property string $hs_salutation
 * @property string $hs_firstname
 * @property string $hs_lastname
 * @property string $hs_jobtitle
 * @property string $hs_mobile_phone
 * @property string $hs_phone
 * @property string $hs_fax
 * @property string $hs_address_1
 * @property string $hs_address_2
 * @property string $hs_address_3
 * @property string $hs_city
 * @property string $hs_state
 * @property string $hs_region
 * @property string $hs_zip
 * @property string $hs_country
 * @property string $hs_telecoms_platform_carrier
 * @property string $hs_park_building_name
 * @property string $hs_park_type
 * @property string $hs_company
 * @property string $hs_industry
 * @property int $hs_associated_company_id
 * @property int $hs_billing_contact
 * @property int $hs_it_contact
 * @property int $hs_hubspot_owner_id
 * @property string $hs_last_modified_date
 * @property string $hs_created_date
 * @property string $billing_address_1
 * @property string $billing_address_2
 * @property string $billing_address_3
 * @property string $billing_city
 * @property string $billing_state
 * @property string $billing_region
 * @property string $billing_zip
 * @property string $billing_country
 * @property int $billing_address_same
 * @property string $billing_contact_firstname
 * @property string $billing_contact_lastname
 * @property string $billing_contact_email
 * 
 * Defined relations:
 * @property Company $company
 * @property User $user
 * @property Quote[] $quotes
 */
class Profile extends BaseProfile
{

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
                'required' => [['public_email', 'hs_firstname', 'hs_lastname', 'hs_jobtitle', 'hs_mobile_phone', 'hs_address_1', 'hs_city', 'hs_state', 'hs_region', 'hs_zip', 'hs_company'], 'required'],
                'textString' => [['bio'], 'string'],
                'integer' => [['company_id', 'gdpr_email', 'gdpr_sms', 'gdpr_phone', 'gdpr_post', 'gdpr_version', 'anv_contact_id', 'anv_customer_id', 'hs_contact_id', 'hs_associated_company_id', 'hs_hubspot_owner_id', 'billing_address_same'], 'integer'],
                'safeDate' => [['created_date', 'modified_date', 'hs_last_modified_date', 'hs_created_date'], 'safe'],
                'varcharLength' => [['name', 'public_email', 'gravatar_email', 'location', 'website', 'timezone', 'hs_salutation', 'hs_firstname', 'hs_lastname', 'hs_jobtitle', 'hs_mobile_phone', 'hs_phone', 'hs_fax', 'hs_address_1', 'hs_address_2', 'hs_address_3', 'hs_city', 'hs_state', 'hs_region', 'hs_zip', 'hs_country', 'hs_telecoms_platform_carrier', 'hs_park_building_name', 'hs_park_type', 'hs_company', 'hs_industry', 'billing_address_1', 'billing_address_2', 'billing_address_3', 'billing_city', 'billing_state', 'billing_region', 'billing_zip', 'billing_country', 'billing_contact_firstname', 'billing_contact_lastname', 'billing_contact_email'], 'string', 'max' => 255],
                'gravatarLength' => [['gravatar_id'], 'string', 'max' => 32],
                'tinyintBoolean' => [['gdpr_email', 'gdpr_sms', 'gdpr_phone', 'gdpr_post', 'gdpr_version', 'hs_billing_contact', 'hs_it_contact', 'billing_address_same'], 'integer', 'max' => 1],
                'companyExist' => [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
                'userExist' => [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
                /**
                 * The below uses the "when" conditional validatator, which validates attributes only when certain conditions apply, e.g. the validation of one attribute depends on the value of another attribute.
                 * @see https://www.yiiframework.com/doc/guide/2.0/en/input-validation#conditional-validation
                 */
                'billingContactDetailsRequired' => [['billing_contact_firstname', 'billing_contact_lastname', 'billing_contact_email'], 'required', 'when' => function ($model) {
                    return (bool) $model->hs_billing_contact === false;
                }, 'whenClient' => "function (attribute, value) {
                    return $('#profile-hs_billing_contact').is(':checked') !== true;
                }"],
                'billingContactDetailsEmailPattern' => [['billing_contact_email'], 'email', 'when' => function ($model) {
                    return (bool) $model->hs_billing_contact === false;
                }, 'whenClient' => "function (attribute, value) {
                    return $('#profile-hs_billing_contact').is(':checked') !== true;
                }"],
                'billingAddressDetailsRequired' => [['billing_address_1', 'billing_city', 'billing_state', 'billing_region', 'billing_zip'], 'required', 'when' => function ($model) {
                    return (bool) $model->billing_address_same === false;
                }, 'whenClient' => "function (attribute, value) {
                    return $('#profile-billing_address_same').is(':checked') !== true;
                }"],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
                'user_id' => Yii::t('usuario', 'User ID'),
                'company_id' => Yii::t('usuario', 'Company ID'),
                'name' => Yii::t('usuario', 'Name'),
                'public_email' => Yii::t('usuario', 'Email'),
                'gravatar_email' => Yii::t('usuario', 'Gravatar Email'),
                'gravatar_id' => Yii::t('usuario', 'Gravatar ID'),
                'location' => Yii::t('usuario', 'Location'),
                'website' => Yii::t('usuario', 'Website'),
                'timezone' => Yii::t('usuario', 'Timezone'),
                'bio' => Yii::t('usuario', 'About Me'),
                'gdpr_email' => Yii::t('usuario', 'By Email'),
                'gdpr_sms' => Yii::t('usuario', 'By SMS'),
                'gdpr_phone' => Yii::t('usuario', 'By Phone'),
                'gdpr_post' => Yii::t('usuario', 'By Post'),
                'gdpr_version' => Yii::t('usuario', 'GDPR Text Version'),
                'created_date' => Yii::t('usuario', 'Created Date'),
                'modified_date' => Yii::t('usuario', 'Modified Date'),
                'anv_contact_id' => Yii::t('usuario', 'Anvil Contact ID'),
                'anv_customer_id' => Yii::t('usuario', 'Anvil Customer ID'),
                'hs_contact_id' => Yii::t('usuario', 'Contact ID'),
                'hs_salutation' => Yii::t('usuario', 'Salutation'),
                'hs_firstname' => Yii::t('usuario', 'First Name'),
                'hs_lastname' => Yii::t('usuario', 'Last Name'),
                'hs_jobtitle' => Yii::t('usuario', 'Jobtitle'),
                'hs_mobile_phone' => Yii::t('usuario', 'Mobile Phone'),
                'hs_phone' => Yii::t('usuario', 'Other Phone'),
                'hs_fax' => Yii::t('usuario', 'Fax'),
                'hs_address_1' => Yii::t('usuario', 'Address Line 1'),
                'hs_address_2' => Yii::t('usuario', 'Address Line 2'),
                'hs_address_3' => Yii::t('usuario', 'Address Line 3'),
                'hs_city' => Yii::t('usuario', 'City'),
                'hs_state' => Yii::t('usuario', 'County'),
                'hs_region' => Yii::t('usuario', 'Region'),
                'hs_zip' => Yii::t('usuario', 'Postcode'),
                'hs_country' => Yii::t('usuario', 'Country'),
                'hs_telecoms_platform_carrier' => Yii::t('usuario', 'Telecoms Platform Carrier'),
                'hs_park_building_name' => Yii::t('usuario', 'Park Building Name'),
                'hs_park_type' => Yii::t('usuario', 'Park Type'),
                'hs_company' => Yii::t('usuario', 'Company Name'),
                'hs_industry' => Yii::t('usuario', 'Industry'),
                'hs_associated_company_id' => Yii::t('usuario', 'Associated Company ID'),
                'hs_billing_contact' => Yii::t('usuario', 'Is Billing Contact?'),
                'hs_it_contact' => Yii::t('usuario', 'It Contact'),
                'hs_hubspot_owner_id' => Yii::t('usuario', 'Hubspot Owner ID'),
                'hs_last_modified_date' => Yii::t('usuario', 'Last Modified Date'),
                'hs_created_date' => Yii::t('usuario', 'Created Date'),
                'billing_address_1' => Yii::t('usuario', 'Billing Address Line 1'),
                'billing_address_2' => Yii::t('usuario', 'Billing Address Line 2'),
                'billing_address_3' => Yii::t('usuario', 'Billing Address Line 3'),
                'billing_city' => Yii::t('usuario', 'Billing City'),
                'billing_state' => Yii::t('usuario', 'Billing County'),
                'billing_region' => Yii::t('usuario', 'Billing Region'),
                'billing_zip' => Yii::t('usuario', 'Billing Postcode'),
                'billing_country' => Yii::t('usuario', 'Billing Country'),
                'billing_address_same' => Yii::t('usuario', 'Billing Address Is Same As Main?'),
                'billing_contact_firstname' => Yii::t('usuario', 'Billing Contact First Name'),
                'billing_contact_lastname' => Yii::t('usuario', 'Billing Contact Last Name'),
                'billing_contact_email' => Yii::t('usuario', 'Billing Contact Email'),
        ]);
    }

    /**
     * @param bool return values as keys
     * @return array Hubspot address region options
     */
    public static function addressRegionOptions($valuesAsKeys = false)
    {
        $data = [
            'South East',
            'South West',
            'North East',
            'North West',
            'London',
            'East of England',
            'East Midlands',
            'West Midlands',
            'Wales',
            'Yorkshire and the Humber',
            'Scotland',
            'N. Ireland',
            'Ireland',
            'Europe',
            'USA',
        ];

        return $valuesAsKeys ? array_combine($data, $data) : $data;
    }

    /**
     * Get the current GDRP text version from app config params
     * 
     * @return int|null
     */
    public function getGdprTextVersion()
    {
        return isset(Yii::$app->params['gdprTextVersion']) ? Yii::$app->params['gdprTextVersion'] : null;
    }

    /**
     * Get the current GDRP text from app config params
     * 
     * @param int $version GDPR Text Version
     * @return int|null
     */
    public function getGdprText($version = '')
    {
        if (empty($version)) {
            // default to the current GDPR text version as per app config params
            $version = $this->getGdprTextVersion();
        }
        
        return isset(Yii::$app->params['gdprText'][$this->getGdprTextVersion()]) ? Yii::$app->params['gdprText'][$this->getGdprTextVersion()] : '';
    }

    /**
     * Update the name field to be the concatenated first/last names from Hubspot
     *
     * @return bool
     */
    public function concatFirstLastNamesToName()
    {
        $this->name = implode(' ', [$this->hs_firstname, $this->hs_lastname]);
        $this->name = trim($this->name);
    }

    /**
     * Check if billing address is different and also ~valid (not strict)
     * 
     * @return bool
     */
    public function getBillingAddressPrecedence()
    {
        return !$this->billing_address_same &&
            !empty($this->billing_address_1) &&
            !empty($this->billing_city) &&
            !empty($this->billing_zip);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuotes()
    {
        return $this->hasMany(Quote::className(), ['user_id' => 'user_id']);
    }

    /**
     * Handle any nuances of data in the model before we commit to insert/update
     * 
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if ($insert) {
            // initialise gravatar email to the user email
            $this->public_email = $this->user->email;
        }

        // glue the profile email to the user email
        $this->public_email = $this->user->email;
        $this->concatFirstLastNamesToName();
        $this->modified_date = new \yii\db\Expression('NOW()');
        if (empty($this->created_date)) {
            $this->created_date = new \yii\db\Expression('NOW()');
        }

        return true;
    }

}

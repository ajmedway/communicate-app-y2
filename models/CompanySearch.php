<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Company;

/**
 * CompanySearch represents the model behind the search form of `app\models\Company`.
 */
class CompanySearch extends Company
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'anv_customer_id', 'hs_company_id', 'hs_parent_company_id', 'hs_hubspot_owner_id', 'CustomerTypeID', 'PrimaryContactID', 'PrimaryAddressID'], 'integer'],
            [['created_date', 'modified_date', 'hs_name', 'hs_phone', 'hs_address_1', 'hs_address_2', 'hs_city', 'hs_state', 'hs_region', 'hs_zip', 'hs_country', 'hs_park_building_name', 'hs_park_building_type', 'hs_website', 'hs_domain', 'hs_industry', 'hs_description', 'hs_last_modified_date', 'hs_created_date', 'CompanyNumber', 'VATref'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Company::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_date' => $this->created_date,
            'modified_date' => $this->modified_date,
            'anv_customer_id' => $this->anv_customer_id,
            'hs_company_id' => $this->hs_company_id,
            'hs_parent_company_id' => $this->hs_parent_company_id,
            'hs_hubspot_owner_id' => $this->hs_hubspot_owner_id,
            'hs_last_modified_date' => $this->hs_last_modified_date,
            'hs_created_date' => $this->hs_created_date,
            'CustomerTypeID' => $this->CustomerTypeID,
            'PrimaryContactID' => $this->PrimaryContactID,
            'PrimaryAddressID' => $this->PrimaryAddressID,
        ]);

        $query->andFilterWhere(['like', 'hs_name', $this->hs_name])
            ->andFilterWhere(['like', 'hs_phone', $this->hs_phone])
            ->andFilterWhere(['like', 'hs_address_1', $this->hs_address_1])
            ->andFilterWhere(['like', 'hs_address_2', $this->hs_address_2])
            ->andFilterWhere(['like', 'hs_city', $this->hs_city])
            ->andFilterWhere(['like', 'hs_state', $this->hs_state])
            ->andFilterWhere(['like', 'hs_region', $this->hs_region])
            ->andFilterWhere(['like', 'hs_zip', $this->hs_zip])
            ->andFilterWhere(['like', 'hs_country', $this->hs_country])
            ->andFilterWhere(['like', 'hs_park_building_name', $this->hs_park_building_name])
            ->andFilterWhere(['like', 'hs_park_building_type', $this->hs_park_building_type])
            ->andFilterWhere(['like', 'hs_website', $this->hs_website])
            ->andFilterWhere(['like', 'hs_domain', $this->hs_domain])
            ->andFilterWhere(['like', 'hs_industry', $this->hs_industry])
            ->andFilterWhere(['like', 'hs_description', $this->hs_description])
            ->andFilterWhere(['like', 'CompanyNumber', $this->CompanyNumber])
            ->andFilterWhere(['like', 'VATref', $this->VATref]);

        return $dataProvider;
    }
}

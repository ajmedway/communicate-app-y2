<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Inflector as I;

/* @var $this yii\web\View */
/* @var $title string */
/* @var $uniqueRef string */
/* @var $quoteUrl string */
/* @var $salesEmail string */
/* @var $salesPhone string */
/* @var $salesPhoneTel string */

?>
<div>
    <h1><?= $title ?></h1>
    <p>Hello, we are pleased to confirm that the sales team at Communicate have reviewed and approved your quote package with reference <strong><?= $uniqueRef ?></strong>.</p>
    <p>Please <?= Html::a('click here', $quoteUrl, ['target' => '_blank']) ?> to check your approved quote package, whereupon you can either checkout to complete the order or make further changes to your package.</p>
    <p>We look forward to receiving your completed order! If you choose to make any further changes, our sales team will need to manually review and approve your product selection once again.</p>
    <p>If you have any questions please contact the office by emailing <?= Html::mailto($salesEmail, $salesEmail) ?> or calling us on <?= Html::a($salesPhone, $salesPhoneTel) ?>.</p>
    <p>Kind Regards, Communicate Sales Team</p>
</div>

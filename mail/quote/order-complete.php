<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Inflector as I;

/* @var $this yii\web\View */
/* @var $title string */
/* @var $uniqueRef string */
/* @var $quoteUrl string */
/* @var $salesEmail string */
/* @var $salesPhone string */
/* @var $salesPhoneTel string */

?>
<div>
    <h1><?= $title ?></h1>
    <p>Hello, we are pleased to confirm that your Communicate order with reference <strong><?= $uniqueRef ?></strong> has been completed!</p>
    <p>Please <?= Html::a('click here', $quoteUrl, ['target' => '_blank']) ?> to check your finalised quote package.</p>
    <p>We will be in touch soon to progress with implementation of your order.</p>
    <p>If you have any questions please contact the office by emailing <?= Html::mailto($salesEmail, $salesEmail) ?> or calling us on <?= Html::a($salesPhone, $salesPhoneTel) ?>.</p>
    <p>Kind Regards, Communicate Sales Team</p>
</div>

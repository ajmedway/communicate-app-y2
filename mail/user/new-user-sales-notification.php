<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Inflector as I;

/* @var $this yii\web\View */
/* @var $title string */
/* @var $id int */
/* @var $username string */
/* @var $email string */
/* @var $created_at string */
/* @var $registration_ip string */

?>
<div>
    <h1><?= $title ?></h1>
    <h4>User Details</h4>
    <ul>
        <li>User ID: <?= $id ?></li>
        <li>Username: <?= $username ?></li>
        <li>Email: <?= $email ?></li>
        <li>Created At: <?= $created_at ?></li>
        <li>Registration IP: <?= $registration_ip ?></li>
    </ul>
</div>

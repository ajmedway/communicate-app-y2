<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'app\commands',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'authManager' => [
            'class' => 'Da\User\Component\AuthDbManagerComponent',
            // uncomment if you want to cache RBAC items hierarchy
            // 'cache' => 'cache',
        ],
    ],
    'params' => $params,
    'controllerMap' => [
        // Fixture generation command line.
        // e.g. php yii fixture/generate-all --count=5
        // See: https://github.com/yiisoft/yii2-faker/blob/master/docs/guide/basic-usage.md
        //      http://www.yiiframework.com/doc-2.0/guide-test-fixtures.html#auto-generating-fixtures
        'fixture' => [
            // Using \Faker\Generator to create fixture data from template files
            'class' => 'yii\faker\FixtureController',
            // Fixture classes namespace
            'namespace' => 'app\tests\unit\fixtures',
            // Source Faker fixture templates path
            'templatePath' => '@app/tests/unit/templates/fixtures',
            // Output fixture data files
            'fixtureDataPath' => '@app/tests/unit/fixtures/data',
        ],
        'migrate' => [
            'class' => \yii\console\controllers\MigrateController::class,
            'migrationPath' => [
                '@app/migrations',
                '@yii/rbac/migrations', // Just in case you forgot to run it on console (see next note)
            ],
            'migrationNamespaces' => [
                'Da\User\Migration',
                'app\modules\product\migrations',
                'app\modules\quote\migrations',
            ],
        ],
    ],
    'modules' => [
        'user' => [
            'class' => Da\User\Module::class,
            'classMap' => [
                'Profile' => app\models\Profile::class,
                'User' => app\models\User::class,
            ],
            'generatePasswords' => true,
            'switchIdentitySessionKey' => 'yuik_usuario_admin_user',
            'administratorPermissionName' => 'administrator',
        ],
    ],
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;

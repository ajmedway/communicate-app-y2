<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'communicate-app',
    'name' => 'Communicate',
    'basePath' => dirname(__DIR__),
    'bootstrap' => [
        'log',
        'product',
        'quote',
    ],
    'timeZone' => 'Europe/London',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'defaultRoute' => 'site/index',
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'j1F0pD2C5ni3DbKzb3oi75obI2eiFUz9',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.office365.com',
                'username' => 'quotes@communicateplc.com',
                'password' => 'fastGor!lla16',
                'port' => '587',
                'encryption' => 'tls',
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@runtime/logs/app.log',
                    'levels' => ['error', 'warning'],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@runtime/logs/hubspot-sync.log',
                    'levels' => ['error', 'warning', 'info'],
                    'categories' => [
                        'app\components\managers\HubspotDataManager:*',
                    ],
                ],
            ],
        ],
        'db' => $db,
        'authManager' => [
            'class' => 'Da\User\Component\AuthDbManagerComponent',
            // uncomment if you want to cache RBAC items hierarchy
            // 'cache' => 'cache',
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@Da/User/resources/views' => '@app/views/user',
                ]
            ]
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
            ],
        ],
        'i18n' => [
            'translations' => [
                'usuario*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages/usuario',
                    'sourceLanguage' => 'en-US',
                ],
            ],
        ],
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            // http://userguide.icu-project.org/formatparse
            'defaultTimeZone' => 'Europe/London', # AP: or 'UTC'? Further research required
            'timeZone' => 'Europe/London',
            'dateFormat' => 'dd-MM-yyyy',
            'datetimeFormat' => 'medium',
            'decimalSeparator' => '.',
            'thousandSeparator' => ',',
            'currencyCode' => 'GBP',
        ],
        'anvilDataManager' => [
            'class' => app\components\managers\AnvilDataManager::class,
            'baseUrl' => 'https://testapi.custservice.co/api/v1',
            'username' => 'testapi@example.com',
            'password' => 'jIlc6#V!d03w',
        ],
        'hubspotDataManager' => [
            'class' => app\components\managers\HubspotDataManager::class,
            'hubspotApiKey' => '7559c05b-c7e3-4769-8f3f-3ca6d478b935',
        ],
        'signNowManager' => [
            'class' => app\components\managers\SignNowManager::class,
            'baseUrl' => 'https://api-eval.signnow.com',
            'clientId' => '',
            'clientSecret' => '',
            'senderEmail' => '',
            'senderPassword' => '',
            'signerEmail' => '',
            'signerPassword' => '',
        ],
    ],
    'on beforeRequest' => function () {
        $user = Yii::$app->user->identity;
        if ($user && !empty($user->profile->timezone)) {
            Yii::$app->setTimeZone($user->profile->timezone);
        }
    },
    'language' => 'en-GB',
    'sourceLanguage' => 'en-US',
    'params' => $params,
    'controllerMap' => [
        'migrate' => [
            'class' => \yii\console\controllers\MigrateController::class,
            'migrationPath' => [
                '@app/migrations',
                '@yii/rbac/migrations', // Just in case you forgot to run it on console (see next note)
            ],
            'migrationNamespaces' => [
                'Da\User\Migration',
                'app\modules\product\migrations',
                'app\modules\quote\migrations',
            ],
        ],
    ],
    'modules' => [
        'product' => [
            'class' => 'app\modules\product\Product',
            'defaultRoute' => 'product/index',
        ],
        'quote' => [
            'class' => 'app\modules\quote\Quote',
            'defaultRoute' => 'quote/index',
        ],
        'user' => [
            'class' => Da\User\Module::class,
            'classMap' => [
                'Profile' => app\models\Profile::class,
                'User' => app\models\User::class,
            ],
            'controllerMap' => [
                'profile' => app\controllers\ProfileController::class,
            ],
            'enableFlashMessages' => false,
            'generatePasswords' => false,
            'enableEmailConfirmation' => true,
            'switchIdentitySessionKey' => 'yuik_usuario_admin_user_x9M&4p^=s',
            'administratorPermissionName' => 'administrator',
            'mailParams' => [
                'fromEmail' => !empty($params['salesEmail']) ? $params['salesEmail'] : 'no-reply@example.com',
            ],
            'routes' => [
                //'<id:\d+>' => 'profile/show',
                '<action:(login|logout)>' => 'security/<action>',
                '<action:(register|resend)>' => 'registration/<action>',
                'confirm/<id:\d+>/<code:[A-Za-z0-9_-]+>' => 'registration/confirm',
                'forgot' => 'recovery/request',
                'recover/<id:\d+>/<code:[A-Za-z0-9_-]+>' => 'recovery/reset',
            ],
        ],
    ],
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', '::1', '185.59.181.136'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', '::1', '185.59.181.136'],
    ];
    
    // https://www.yiiframework.com/doc/api/2.0/yii-web-assetmanager#%24forceCopy-detail
    $config['components']['assetManager']['forceCopy'] = true;
}

return $config;

<?php

use yii\base\Event;
use Da\User\Event\FormEvent;
use Da\User\Event\ProfileEvent;
use Da\User\Event\UserEvent;
use Da\User\Controller\AdminController;
use Da\User\Controller\RegistrationController;
use Da\User\Controller\SecurityController;
use Da\User\Controller\SettingsController;
use app\models\User;

// Form Events
/**
 * @see http://yii2-usuario.readthedocs.io/en/latest/events/form-events/
 * Form Events
 * The following is the list of the form events and where they happen:
 * 
 * RecoveryController
 * 
 * FormEvent::EVENT_BEFORE_REQUEST: Occurs before a password recovery request
 * FormEvent::EVENT_AFTER_REQUEST: Occurs after a password recovery request
 * 
 * 
 * RegistrationController
 * FormEvent::EVENT_BEFORE_RESEND: Occurs before a confirmation message is being sent via email
 * FormEvent::EVENT_AFTER_RESEND: Occurs after a confirmation message is being sent via email
 * FormEvent::EVENT_BEFORE_REGISTER: Occurs before user registration
 * FormEvent::EVENT_AFTER_REGISTER: Occurs after user registration
 * 
 * SecurityController
 * FormEvent::EVENT_BEFORE_LOGIN: Occurs before a user logs into the system
 * FormEvent::EVENT_AFTER_LOGIN: Occurs after a user logs into the system
 * 
 * 
 * How to Work With Form Events
 * All these events received an instance of a `Da\User\Event\FormEvent`.
 * The event receives an instance of a form depending on where its being called. The following is the list of the forms accessible via `FormEvent::getForm()`
 * 
 * FormEvent::EVENT_BEFORE_LOGIN|EVENT_AFTER_LOGIN: It will contain a `Da\User\Form\LoginForm` instance with the submitted data
 * FormEvent::EVENT_BEFORE_RESEND|EVENT_AFTER_RESEND: It will contain a `Da\User\Form\ResendForm` instance with the submitted data
 * FormEvent::EVENT_BEFORE_REQUEST|EVENT_AFTER_REQUEST: It will contain a `Da\User\Form\RecoveryForm` instance with the submitted data
 */
// User Events
/**
 * @see http://yii2-usuario.readthedocs.io/en/latest/events/user-events/
 * User Events
 * The following is the list of the user events and where they happen:
 * 
 * AdminController
 * 
 * UserEvent::EVENT_BEFORE_CREATE: Occurs before a user has been created
 * UserEvent::EVENT_AFTER_CREATE: Occurs after a user has been created
 * UserEvent::EVENT_BEFORE_PROFILE_UPDATE: Occurs before a user's profile has been updated
 * UserEvent::EVENT_AFTER_PROFILE_UPDATE: Occurs after a user's profile has been updated
 * UserEvent::EVENT_BEFORE_CONFIRMATION: Occurs before a user's email has been confirmed
 * UserEvent::EVENT_AFTER_CONFIRMATION: Occurs after a user's email has been confirmed
 * UserEvent::EVENT_BEFORE_BLOCK: Occurs before a user is being blocked (forbid access to app)
 * UserEvent::EVENT_AFTER_BLOCK: Occurs after a user is being blocked (forbid access to app)
 * UserEvent::EVENT_BEFORE_UNBLOCK: Occurs before a user is being un-blocked
 * UserEvent::EVENT_AFTER_UNBLOCK: Occurs after a user is being un-blocked
 * UserEvent::EVENT_BEFORE_SWITCH_IDENTITY: Occurs before a user is being impersonated by admin
 * UserEvent::EVENT_AFTER_SWITCH_IDENTITY: Occurs after a user his being impersonated by admin
 * 
 * 
 * RegistrationController
 * 
 * UserEvent::EVENT_BEFORE_CONFIRMATION
 * UserEvent::EVENT_AFTER_CONFIRMATION
 * 
 * 
 * SecurityController
 * 
 * UserEvent::EVENT_BEFORE_LOGOUT: Occurs before user logs out of the app
 * UserEvent::EVENT_AFTER_LOGOUT: Occurs after user logs out of the app
 * 
 * 
 * SettingsController
 * 
 * UserEvent::EVENT_BEFORE_PROFILE_UPDATE
 * UserEvent::EVENT_AFTER_PROFILE_UPDATE
 * UserEvent::EVENT_BEFORE_ACCOUNT_UPDATE: Occurs before the user account is updated
 * UserEvent::EVENT_AFTER_ACCOUNT_UPDATE: Occurs after the user account is updated
 * UserEvent::EVENT_BEFORE_DELETE: Occurs before the user account is deleted
 * UserEvent::EVENT_AFTER_DELETE: Occurs after the user account is deleted
 * 
 * 
 * User Model
 * 
 * UserEvent::EVENT_BEFORE_REGISTER
 * UserEvent::EVENT_AFTER_REGISTER
 * UserEvent::EVENT_BEFORE_CONFIRMATION
 * UserEvent::EVENT_AFTER_CONFIRMATION
 * UserEvent::EVENT_BEFORE_BLOCK
 * UserEvent::EVENT_AFTER_BLOCK
 * UserEvent::EVENT_BEFORE_UNBLOCK
 * UserEvent::EVENT_AFTER_UNBLOCK
 * 
 * How to Work With User Events
 * 
 * All these events receive an instance of `Da\User\Event\UserEvent`.
 * The Event receives an instance of a `Da\Model\User` class that you could use for whatever logic you wish to implement.
 */

// Assign basic user role on User model after create event (triggered by admin create user)
// Also update profile from Hubspot contact
// Also notify sales team of new user sign-up
Event::on(User::class, UserEvent::EVENT_AFTER_CREATE, function (UserEvent $event) {

    $user = $event->getUser();

    $auth = Yii::$app->getAuthManager();
    $auth->assign($auth->getRole(User::$ROLE_USER), $user->id);

    Yii::$app->hubspotDataManager->updateProfileFromHubspotContactByUser($user);

    $user->newUserSalesNotificationEmail();
});

// Assign basic user role on Registration controller after confirmation event (triggered by confirmation)
// Also update profile from Hubspot contact
// Also notify sales team of new user sign-up
Event::on(RegistrationController::class, UserEvent::EVENT_AFTER_CONFIRMATION, function (UserEvent $event) {

    $user = $event->getUser();

    $auth = Yii::$app->getAuthManager();
    $auth->assign($auth->getRole(User::$ROLE_USER), $user->id);

    Yii::$app->hubspotDataManager->updateProfileFromHubspotContactByUser($user);

    $user->newUserSalesNotificationEmail();
});

// Update profile from Hubspot contact on Security controller after login event
Event::on(SecurityController::class, FormEvent::EVENT_AFTER_LOGIN, function (FormEvent $event) {

    $user = $event->getForm()->getUser();

    Yii::$app->hubspotDataManager->updateProfileFromHubspotContactByUser($user);
});

// Update Hubspot contact from the updated Profile data on Settings controller after update Profile event
Event::on(SettingsController::class, UserEvent::EVENT_AFTER_PROFILE_UPDATE, function (ProfileEvent $event) {

    $profile = $event->getProfile();

    Yii::$app->hubspotDataManager->updateHubspotContactFromProfileByUser($profile->user);
});

// Update Hubspot contact from the updated Profile data on Admin controller after update Profile event
Event::on(AdminController::class, UserEvent::EVENT_AFTER_PROFILE_UPDATE, function (UserEvent $event) {

    $user = $event->getUser();

    Yii::$app->hubspotDataManager->updateHubspotContactFromProfileByUser($user);
});

<?php

return [
    'salesEmail' => 'quotes@communicateplc.com',
    'alecEmail' => 'alec@r-evolution.co.uk',
    'salesPhone' => '0800 404 8888',
    'salesPhoneTel' => 'tel:+448004048888',
    'anvilCompanyId' => 13,
    'gdprTextVersion' => 1,
    'gdprText' => [
        1 => 'I am happy to consent to receiving marketing communications via the following methods...',
    ],
];

<?php
// Uncomment below to mock the \Faker\Generator instance for hints
//$faker = \Faker\Factory::create();

/**
 * @see https://github.com/fzaninotto/Faker
 * @var $faker \Faker\Generator
 * @var $index integer
 */
return [
    'name' => $faker->firstName(),
    'price' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 10000),
    'setup_price' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 10000),
];

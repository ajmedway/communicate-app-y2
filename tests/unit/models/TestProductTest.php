<?php

namespace tests\unit\models;

use app\models\User;

class TestProductTest extends \Codeception\Test\Unit
{
    
    public function _fixtures()
    {
        return [
            'profiles' => [
                'class' => TestProductFixture::className(),
                '@app/tests/unit/fixtures/data/TestProduct.php',
            ],
        ];
    }

}

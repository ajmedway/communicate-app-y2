<?php

return [
    [
        'name' => 'Arvilla',
        'price' => 1174.22,
        'setup_price' => 8053.52,
    ],
    [
        'name' => 'Rebecca',
        'price' => 1719.36,
        'setup_price' => 7875.03,
    ],
    [
        'name' => 'Grover',
        'price' => 6885.65,
        'setup_price' => 7698.55,
    ],
    [
        'name' => 'Janelle',
        'price' => 6588.84,
        'setup_price' => 8809.03,
    ],
    [
        'name' => 'Rhianna',
        'price' => 9826.99,
        'setup_price' => 1580.26,
    ],
];

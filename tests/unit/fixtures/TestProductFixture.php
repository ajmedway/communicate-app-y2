<?php

/**
 * Copyright © 2017 r//evolution Marketing. All rights reserved.
 * 
 * @author Alec Pritchard <alec@r-evolution.co.uk>
 */

namespace app\tests\unit\fixtures;

use yii\test\ArrayFixture;

class TestProductFixture extends ArrayFixture
{
    public $modelClass = 'app\models\TestProduct';
    public $dataFile = '@app/tests/unit/fixtures/data/TestProduct.php';
}
